object Form11: TForm11
  Left = 1404
  Top = 129
  Width = 251
  Height = 344
  BorderStyle = bsSizeToolWin
  Caption = 'Verzeichnis'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object ShellTreeView1: TShellTreeView
    Left = 0
    Top = 0
    Width = 235
    Height = 274
    ObjectTypes = [otFolders]
    Root = 'rfDesktop'
    UseShellImages = True
    Align = alClient
    AutoRefresh = True
    Indent = 19
    ParentColor = False
    RightClickSelect = True
    ShowRoot = False
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 274
    Width = 235
    Height = 32
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Button1: TButton
      Left = 8
      Top = 0
      Width = 97
      Height = 32
      Caption = 'Abbrechen'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 128
      Top = 0
      Width = 99
      Height = 32
      Caption = 'OK'
      TabOrder = 1
      OnClick = Button2Click
    end
  end
end
