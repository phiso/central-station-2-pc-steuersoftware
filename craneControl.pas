unit craneControl;

interface

uses Sysutils,typehelper, traincontroller;

type TCraneControl = class(TTraincontroller)
  private
    FAdr1, FAdr2 : integer;
  public
    constructor create(adr1:integer=71; adr2:integer=72);
end;

implementation

constructor TCraneControl.create(adr1:integer=71; adr2:integer=72);
begin
  inherited create(adr1,dcMM2Prog);
  FAdr1 := adr1;//br�cke fahren
  FAdr2 := adr2;//Kran bedienen
end;

end.
