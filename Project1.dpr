program Project1;

uses
  Forms,
  main in 'main.pas' {Form1},
  RuleHandler in 'RuleHandler.pas',
  typehelper in 'typehelper.pas',
  package in 'package.pas',
  ruleeditor in 'ruleeditor.pas' {Form2},
  UDPsend in 'UDPsend.pas',
  keyboard in 'keyboard.pas' {Form3},
  digitTurntable in 'digitTurntable.pas' {Form4},
  digitCrane in 'digitCrane.pas' {Form5},
  debugger in 'debugger.pas' {Form6},
  UDPlistenThread in 'UDPlistenThread.pas',
  rcvparser in 'rcvparser.pas',
  traincontroller in 'traincontroller.pas',
  Control in 'Control.pas',
  csSystem in 'csSystem.pas',
  trainthread in 'trainthread.pas',
  newTrain in 'newTrain.pas' {Form7},
  cs2File in 'cs2File.pas',
  TraincontrolWindow in 'TraincontrolWindow.pas',
  GridImage in 'GridImage.pas',
  MenuitemExtend in 'MenuitemExtend.pas',
  newLayout in 'newLayout.pas' {Form8},
  layoutObject in 'layoutObject.pas',
  imageEx in 'imageEx.pas',
  newArticle in 'newArticle.pas' {Form9},
  turntableGraphics in 'turntableGraphics.pas',
  articlecontrol in 'articlecontrol.pas',
  turntableControl in 'turntableControl.pas',
  ControlListenThread in 'ControlListenThread.pas',
  settings in 'settings.pas' {Form10},
  directorytree in 'directorytree.pas' {Form11},
  newSystem in 'newSystem.pas' {Form12},
  turntableDrawThread in 'turntableDrawThread.pas',
  craneControl in 'craneControl.pas',
  insertLayoutObject in 'insertLayoutObject.pas' {Form13},
  helpdisplay in 'helpdisplay.pas',
  gamepad in 'gamepad.pas',
  gamepadthread in 'gamepadthread.pas',
  linking in 'linking.pas' {Form15},
  eventlistener in 'eventlistener.pas',
  shuttletrain in 'shuttletrain.pas' {Form16},
  linkedaction_obj in 'linkedaction_obj.pas',
  newlinking in 'newlinking.pas' {Form17};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm3, Form3);
  Application.CreateForm(TForm4, Form4);
  Application.CreateForm(TForm5, Form5);
  Application.CreateForm(TForm6, Form6);
  Application.CreateForm(TForm7, Form7);
  Application.CreateForm(TForm8, Form8);
  Application.CreateForm(TForm9, Form9);
  Application.CreateForm(TForm10, Form10);
  Application.CreateForm(TForm11, Form11);
  Application.CreateForm(TForm12, Form12);
  Application.CreateForm(TForm13, Form13);
  Application.CreateForm(TForm15, Form15);
  Application.CreateForm(TForm16, Form16);
  Application.CreateForm(TForm17, Form17);
  //Application.CreateForm(TtrainWindow, TrainW);
  Application.Run;
end.
