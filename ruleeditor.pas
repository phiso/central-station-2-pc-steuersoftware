unit ruleeditor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Menus, Rulehandler, typehelper;

type
  TForm2 = class(TForm)
    GroupBox1: TGroupBox;
    Splitter1: TSplitter;
    GroupBox2: TGroupBox;
    Button1: TButton;
    Button2: TButton;
    LabeledEdit1: TLabeledEdit;
    ListBox1: TListBox;
    LabeledEdit2: TLabeledEdit;
    PopupMenu1: TPopupMenu;
    EintragLschen1: TMenuItem;
    Memo1: TMemo;
    Label1: TLabel;
    CheckBox1: TCheckBox;
    procedure Splitter1Moved(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EintragLschen1Click(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    RuleHandler : TRuleHandling;
    editting : boolean;
    actRule : TRule;
    CorrStr : string;

    procedure checkbuff;
    procedure buildrule;
  public
    procedure start;
  end;

var
  Form2: TForm2;

implementation

uses main;

{$R *.dfm}

procedure Tform2.start;
var i : integer;
begin
  if not self.Showing then showmodal;
  listbox1.Clear;
  editting := false;
  corrStr := '';
  for i := 0 to length(MainRuleHandler.Rules)-1 do begin
    listbox1.Items.Add(mainRulehandler.Rules[i].Name+'->'+
                        MainRuleHandler.Rules[i].Buffer);
  end;
  listbox1.ItemIndex := -1;
  labelededit1.Clear;
  labelededit2.Clear;
  memo1.clear;
  Splitter1Moved(self);
end;

procedure TForm2.Splitter1Moved(Sender: TObject);
begin
  labelededit1.Width := groupbox2.ClientWidth-20;
  labelededit1.Left := 10;
  labelededit2.Width := groupbox2.ClientWidth-20;
  labelededit2.Left := 10;
  memo1.Width := groupbox2.ClientWidth-20;
  memo1.Left := 10;
  button1.Top := groupbox2.ClientHeight-button1.Height-10;
  button1.Left := groupbox2.ClientWidth-button1.Width-10;
  button2.Top := groupbox2.ClientHeight-button2.Height-10;
  button2.Left := 10;
  checkbox1.Left := 10;
end;

procedure TForm2.FormShow(Sender: TObject);
begin
  start;
end;

procedure TForm2.EintragLschen1Click(Sender: TObject);
begin
  mainRuleHandler.DeleteRule(listbox1.ItemIndex);
  start;
end;

procedure Tform2.checkbuff;
var temp : TstrArray;
    tempstr : string;
    p,i,n : integer;
    corr : boolean;
begin
  corr := false;
  tempstr := labelededit2.Text;
  //Doppelte Leerzeichen checken
  p := ansipos('  ',tempstr);
  if p <> 0 then begin
    delete(tempstr,p,1);
    corr := true;
  end;

  n := explode(temp,' ',tempstr);

  SetLength(temp,13);
  if n < 13 then begin
    corr := true;
    for i := n to 12 do begin
      temp[i] := '0'; //bei zu kurzen buffer mit nullen auff�llen
    end;
  end;

  //Bytegr��e und L�nge des Buffers erzwingen
  for i := 0 to 12 do begin
    if strtoint(temp[i])>255 then begin
      temp[i] := '255';
      corr := true;
    end else
      if strtoint(temp[i])<0 then begin
        temp[i] := '0';
        corr := true;
      end;
  end;
  CorrStr := implode(' ',temp);
  if corr then begin
    labelededit2.Text := Corrstr;
    labelededit2.Font.Color := clred;
    Showmessage('Fehler im Buffer wurden behoben, bitte nochmals pr�fen!');
  end;
end;

procedure Tform2.buildrule;
begin
  if labelededit1.Text <> '' then begin
    actrule.Name := labelededit1.Text;
    actrule.Buffer := labelededit2.Text;
    actrule.IsDynamic := checkbox1.Checked;
  end else begin
    showmessage('Name kann so nicht verwendet werden!');
  end;
end;

procedure TForm2.ListBox1Click(Sender: TObject);
begin
  editting := true;
  button2.Caption := '�ndern';
  actrule := MainruleHandler.GetRule(listbox1.ItemIndex);
  labelededit1.Text := actrule.Name;
  labelededit2.Text := actrule.Buffer;
  checkbox1.Checked := actrule.IsDynamic;
  memo1.Clear;
  memo1.Text := Mainrulehandler.GetDescription(actrule.Name);
end;

procedure TForm2.Button2Click(Sender: TObject);
begin
  checkbuff;
  if corrStr <> '' then buildrule;
  
  if not editting then begin
    mainRuleHandler.newRule(actrule.Name,actrule.Buffer,actrule.IsDynamic);
  end else begin
    mainRulehandler.EditRule(listbox1.ItemIndex,actrule.Name,actrule.Buffer,
                                actrule.IsDynamic);
    start;
    button2.Caption := 'Hinzuf�gen';
  end;
  if memo1.Text<>'' then MainRuleHandler.SetDescription(actrule.Name,memo1.Text);
  start;
end;

procedure TForm2.Button1Click(Sender: TObject);
begin
  close;
end;

end.
