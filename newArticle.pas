unit newArticle;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, ComCtrls, StdCtrls, ExtCtrls, Spin, inifiles, layoutobject,
  typehelper, gridimage;

type
  TForm9 = class(TForm)
    GroupBox1: TGroupBox;
    ComboBox1: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    ComboBoxEx1: TComboBoxEx;
    ImageList1: TImageList;
    LabeledEdit1: TLabeledEdit;
    SpinEdit1: TSpinEdit;
    Label3: TLabel;
    Label4: TLabel;
    SpinEdit2: TSpinEdit;
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ComboBoxEx1Change(Sender: TObject);
  private
    Article : TArticle;
    Starter : Tobject;
    Layout : TlayoutObject;
    FType : string;
    secs : Tstringlist;

    procedure loadarticles(src : string);
  public
    procedure Start(Sender : Tobject; lob : TlayoutObject);
    procedure New(Sender : Tobject);
  end;

var
  Form9: TForm9;

implementation

uses main, keyboard;

{$R *.dfm}

procedure TForm9.FormCreate(Sender: TObject);
begin
  button1.Align := alRight;
  button2.Align := alLeft;
//  SendTo := 0;
//  combobox2.OnDrawItem := combobox2DrawItem;
end;


procedure Tform9.loadarticles(src : string);
var ini : Tinifile;
    i : integer;
    temp : TIcon;
    f : string;
begin
  ini := Tinifile.Create(src);
  secs := Tstringlist.Create;
  ini.ReadSections(secs);
  temp := TIcon.Create;
  for i := 0 to secs.Count-1 do begin
    f := ini.ReadString(secs[i],'icon','err');
    temp.LoadFromFile(form1.Default_SystemPath+'icons\articles\'+f+'.ico');
    imagelist1.AddIcon(temp);
    f := ini.ReadString(secs[i],'caption','err');
    comboboxex1.ItemsEx[i].Caption := f;
    comboboxex1.ItemsEx[i].ImageIndex := i;
  end;
end;

procedure Tform9.start(Sender : Tobject; lob : TlayoutObject);
var i : integer;
begin
  Starter := Sender;
  labelededit1.Text := lob.typ;
//  Layout := TlayoutObject.create(lob.Typ,lob.position,lob.state,lob.Turned);
  loadarticles(form1.Default_SystemPath+'icons\PicMapping.ini');
  comboboxex1.ItemIndex := 0;
  showmodal;
end;

procedure TForm9.Button2Click(Sender: TObject);
begin
  if Starter is TgridImage then
    (Starter as Tgridimage).ClearPos(Layout.position.X,layout.position.Y);
  close;
end;

procedure Tform9.New(Sender : Tobject);
begin
  Starter := Sender;
  if Sender is Tform then begin
        
  end;
  loadarticles(form1.Default_SystemPath+'icons\PicMapping.ini');
  comboboxex1.ItemIndex := 0;
  showmodal;
end;

procedure TForm9.Button1Click(Sender: TObject);
begin
  Article.Adr := spinedit2.Value;
  Article.State := 0;
  Article.shiftTime := spinedit1.Value;
  Article.dectype := strTodec(combobox1.Text);
  Article.Typ := FType;
  Article.name := labelededit1.Text;
  form3.NewArticle(Article);
  close;
end;

procedure TForm9.ComboBoxEx1Change(Sender: TObject);
begin
  Ftype := secs[comboboxex1.itemindex];
end;

end.
