object Trainwindow: TTrainwindow
  Left = 507
  Top = 177
  Width = 384
  Height = 432
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSizeToolWin
  Caption = 'Zug'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 137
    Width = 368
    Height = 2
    Cursor = crVSplit
    Align = alTop
  end
  object Splitter2: TSplitter
    Left = 214
    Top = 219
    Width = 2
    Height = 175
    Align = alRight
  end
  object Splitter3: TSplitter
    Left = 0
    Top = 217
    Width = 368
    Height = 2
    Cursor = crVSplit
    Align = alTop
  end
  object Panel1: TPanel
    Left = 216
    Top = 219
    Width = 152
    Height = 175
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 0
    object ScrollBox1: TScrollBox
      Left = 0
      Top = 0
      Width = 152
      Height = 175
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 368
    Height = 137
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Image1: TImage
      Left = 0
      Top = 0
      Width = 368
      Height = 137
      Align = alClient
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 219
    Width = 214
    Height = 175
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 214
      Height = 175
      Align = alClient
      Caption = 'Geschwindigkeit'
      TabOrder = 0
      object Image2: TImage
        Left = 2
        Top = 15
        Width = 210
        Height = 82
        Align = alTop
      end
      object ProgressBar1: TProgressBar
        Left = 0
        Top = 136
        Width = 177
        Height = 25
        Position = 5
        TabOrder = 0
      end
      object SpinButton1: TSpinButton
        Left = 184
        Top = 136
        Width = 20
        Height = 25
        DownGlyph.Data = {
          0E010000424D0E01000000000000360000002800000009000000060000000100
          200000000000D800000000000000000000000000000000000000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000008080000080800000808000000000000080800000808000008080000080
          8000008080000080800000808000000000000000000000000000008080000080
          8000008080000080800000808000000000000000000000000000000000000000
          0000008080000080800000808000000000000000000000000000000000000000
          0000000000000000000000808000008080000080800000808000008080000080
          800000808000008080000080800000808000}
        TabOrder = 1
        UpGlyph.Data = {
          0E010000424D0E01000000000000360000002800000009000000060000000100
          200000000000D800000000000000000000000000000000000000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000000000000000000000000000000000000000000000000000000000000080
          8000008080000080800000000000000000000000000000000000000000000080
          8000008080000080800000808000008080000000000000000000000000000080
          8000008080000080800000808000008080000080800000808000000000000080
          8000008080000080800000808000008080000080800000808000008080000080
          800000808000008080000080800000808000}
      end
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 139
    Width = 368
    Height = 78
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
  end
end
