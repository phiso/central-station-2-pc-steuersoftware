unit turntableControl;

interface

uses typehelper, Sysutils, Classes, RuleHandler, package, control, csSystem,
     turntablegraphics, Turntabledrawthread, inifiles;

type TTurntableControl = class(TControl)
  private
    FSystem : TcsSystem;
    FStartAdress, FPos : integer;
    FGraphics : TTurntabledraw;
    FexternalCall : boolean;
    StateIni : Tinifile;

    procedure incPos(n : integer);
    procedure decPos(n : integer);
  public
    property Position : integer read FPos;
    property ExternalCall : boolean read FExternalCall;

    constructor create(surface : TComponent; f: string; adr:integer=225);

    procedure StartProgramming;
    procedure close;
    procedure input;
    procedure ending;
    procedure clear;
    procedure turn;
    procedure setDirection(d : integer);
    procedure autoMapping(occu : TintArray=0);
    procedure Mapping(posi : TintArray);
    procedure StepForward;
    procedure StepBackward;
    procedure TurnTo(adr : integer);//1..24
    procedure mark(i : integer);
    procedure MarkCirclePos(c : integer);
    procedure SetAnimSpeed(s : integer);
    procedure StartExtCall;
    procedure StopExtCall;
end;

implementation

uses main, digitTurntable;

constructor TTurntableControl.create(surface : TComponent; f : string; adr:integer=225);
begin
  inherited create(adr,dcMM2Shift);
  FstartAdress := FAdress;
  Fgraphics := TTurntabledraw.create(surface,0);
  FPos := 0;
  FExternalCall := false;
  stateIni := Tinifile.Create(f);
end;

procedure TTurntableControl.input;
begin
  Fadress := FStartAdress;
  ShiftSwitch(1);
end;

procedure TTurntableControl.ending;
begin
  Fadress := FStartAdress;
  shiftswitch(0);
end;

procedure TTurntableControl.clear;
begin
  Fadress := FStartadress + 1;
  shiftswitch(0);
end;

procedure TTurnTableControl.setDirection(d : integer);
begin
  Fadress := FStartAdress +3;
  if not FExternalCall then Shiftswitch(d);
end;

procedure TTurntableControl.turn;
var temp : integer;
begin
  Fadress := FStartadress + 1; //std:226
  shiftswitch(1);
  Fgraphics.TurnTo(-180,true);
  temp := FPos;
  dec(temp,24);
  if temp < 0 then temp := 48-(24-Fpos);
end;

procedure TTurntableControl.StepForward;
begin
  FAdress := FStartAdress +2;  //std:227
  if not FExternalCall then shiftswitch(0);
  FGraphics.TurnTo(7.5,false);
  inc(FPos);
  if Fpos > 48 then Fpos := FPos - 48;
  form4.StatusBar1.Panels[2].Text := inttostr(FPos);
end;

procedure TTurntableControl.StepBackward;
begin
  FAdress := FStartAdress +2;
  if not FExternalCall then shiftswitch(1);
  FGraphics.turnTo(-7.5,false);
  dec(Fpos);
  if Fpos < 0 then Fpos := 48;
  form4.StatusBar1.Panels[2].Text := inttostr(FPos);
end;

procedure TTurntableControl.TurnTo(adr : integer);
var adress,temp : integer;
begin
  dec(adr);
  if adr > Fpos then setDirection(0)
  else setDirection(1);
  pause(100);
  inc(adr);
  case adr of
    1,2: adress := 229;
    3,4: adress := 230;
    5,6: adress := 231;
    7,8: adress := 232;
    9,10: adress := 233;
    11,12: adress := 234;
    13,14: adress := 235;
    15,16: adress := 236;
    17,18: adress := 237;
    19,20: adress := 238;
    21,22: adress := 239;
    23,24: adress := 240;
  end;
  dec(adr);
  FAdress := adress;
  if not FExternalCall then begin
    if adr mod 2 = 0 then
      shiftswitch(0)
    else shiftswitch(1);
  end;
  if adr < Fpos then FGRaphics.TurnTo(-((Fpos-adr)*7.5),true)
  else FGRaphics.TurnTo((adr-Fpos)*7.5,true);

  {temp := 180;
  if adr < Fpos then begin
    FGRaphics.TurnTo(-((Fpos-adr)*7.5),true);
    Fpos := adr;
  end else begin
    FGraphics.TurnTo(-(temp-(adr-Fpos)*7.5),true);
    Fpos := adr;
  end; }
  Fpos := adr;
  form4.StatusBar1.Panels[2].Text := inttostr(FPos);
  form4.StatusBar1.Panels[0].Text := 'Destination reached';
end;

procedure TTurntableControl.StartProgramming;
var i : integer;
begin
  form1.System.SystemStop;
  for i := 1 to 12 do begin
    pause(1000);
    form4.StatusBar1.Panels[0].Text := 'Initialisiere Programmierung '+inttostr(12-i)+'sek';
  end;
  form1.System.SystemGo;
  pause(500);
  input;
  form4.StatusBar1.Panels[0].Text := 'Starte Programmierung...';
  pause(5000);
  clear;
  mark(1);
  mark(24);
end;

procedure TTurntableControl.autoMapping(occu : TintArray=0);
var i,j : integer;
begin
  form4.StatusBar1.Panels[0].Text := 'Initialisiere Programmierung';
  startprogramming;
  pause(1000);
  for i := 0 to 23 do begin
    form4.StatusBar1.Panels[0].Text := 'programiere Gleis: '+inttostr(i+1);
    stepforward;
    sleep(2500);
    input;
    sleep(1000);
    if occu <> nil then begin
      for j := 0 to length(occu)-1 do begin
        if i = occu[j]-1 then begin
          mark(i);
          mark(i+24);
        end;
      end;
    end else begin
      mark(i);
      mark(i+24);
    end;
  end;
  ending;
  form4.StatusBar1.Panels[0].Text := 'Programmierung wird abgeschlossen...';
  pause(3000);
  form4.StatusBar1.Panels[0].Text := '';
end;

procedure TTurnTableControl.incPos(n : integer);
var temp : integer;
begin

end;

procedure TTurnTableControl.decPos(n : integer);
var temp : integer;
begin

end;

procedure TTurntableControl.mark(i : integer);
begin
  FGraphics.RailPosition(i);
  Fgraphics.Graph.Refresh;
end;

procedure TTurntableControl.Mapping(posi : TintArray);
var i,act,n : integer;
begin
  form4.StatusBar1.Panels[0].Text := 'Initialisiere Programmierung';
  act := 0;
  startprogramming;
  pause(1000);
  for i := 0 to length(posi)-1 do begin
    form4.StatusBar1.Panels[0].Text := 'programiere Gleis: '+inttostr(posi[i]);
    for n := 0 to (posi[i]-act) do begin
      stepforward;
      inc(act);
      sleep(1500);
    end;
    mark(posi[i]);
    mark(posi[i]+24);
    input;
    sleep(1000);
  end;
  ending;
  form4.StatusBar1.Panels[0].Text := 'Programmierung wird abgeschlossen...';
  Fgraphics.TurnTo(-(act*7.5),true);
  pause(3000);
  form4.StatusBar1.Panels[0].Text := '';
end;

procedure TTurntablecontrol.close;
begin
//  Fgraphics.Free;
end;

procedure TTurntableControl.SetAnimSpeed(s : integer);
begin
  FGraphics.AnimationSpeed := s;
end;

procedure TTurntablecontrol.MarkCirclePos(c : integer);
begin
  FGraphics.MarkPosition(c);
  FGraphics.Graph.Refresh;
end;

procedure TTurnTableControl.StartExtCall;
begin
  FExternalCall := true;
end;

procedure TTurnTableControl.StopExtCall;
begin
  FExternalCall := false;
end;

end.
