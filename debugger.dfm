object Form6: TForm6
  Left = 458
  Top = 292
  Width = 550
  Height = 397
  Caption = 'Debug'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 209
    Width = 534
    Height = 2
    Cursor = crVSplit
    Align = alTop
  end
  object Button2: TButton
    Left = 352
    Top = 264
    Width = 75
    Height = 25
    Caption = 'Stop Logger'
    TabOrder = 0
    OnClick = Button2Click
  end
  object RichEdit1: TRichEdit
    Left = 0
    Top = 0
    Width = 534
    Height = 209
    Align = alTop
    Lines.Strings = (
      'RichEdit1')
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
    OnChange = RichEdit1Change
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 211
    Width = 534
    Height = 148
    Align = alClient
    Caption = 'Verf'#252'gbare Befehle'
    TabOrder = 2
    object ListBox1: TListBox
      Left = 2
      Top = 15
      Width = 217
      Height = 131
      Align = alLeft
      ItemHeight = 13
      TabOrder = 0
    end
    object Button1: TButton
      Left = 224
      Top = 16
      Width = 105
      Height = 33
      Caption = 'Befehl hinzuf'#252'gen'
      TabOrder = 1
    end
    object Button3: TButton
      Left = 224
      Top = 56
      Width = 105
      Height = 33
      Caption = 'Befehl '#228'ndern'
      TabOrder = 2
    end
    object Button4: TButton
      Left = 224
      Top = 96
      Width = 105
      Height = 33
      Caption = 'Befehl senden'
      TabOrder = 3
    end
  end
  object CheckBox1: TCheckBox
    Left = 56
    Top = 160
    Width = 97
    Height = 17
    Caption = 'Logging'
    Checked = True
    State = cbChecked
    TabOrder = 3
    OnClick = CheckBox1Click
  end
end
