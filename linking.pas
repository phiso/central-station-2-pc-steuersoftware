unit linking;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Menus, Spin;

type TLink = record
  adr,param1,param2 : integer;
end;
type
  TForm15 = class(TForm)
    GroupBox1: TGroupBox;
    MainMenu1: TMainMenu;
    Fahrstrae1: TMenuItem;
    ComboBox1: TComboBox;
    GroupBox2: TGroupBox;
    Panel1: TPanel;
    Button1: TButton;
    Label2: TLabel;
    SpinEdit1: TSpinEdit;
    Label3: TLabel;
    ComboBox3: TComboBox;
    ListBox1: TListBox;
    Panel2: TPanel;
    Button2: TButton;
    Button3: TButton;
    LabeledEdit1: TLabeledEdit;
    ComboBox2: TComboBox;
    ComboBox4: TComboBox;
    SpinEdit2: TSpinEdit;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
  private
    procedure newlink(link : TLink);
    procedure hidefields;
  public
    { Public declarations }
  end;

var
  Form15: TForm15;

implementation

uses newlinking;

{$R *.dfm}

procedure TForm15.Button1Click(Sender: TObject);
begin
  form17.ShowModal;
end;

procedure TForm15.newlink(link : TLink);
begin

end;

procedure TForm15.Button2Click(Sender: TObject);
begin
  close;
end;

procedure TForm15.ComboBox1Change(Sender: TObject);
begin
  hidefields;
  case combobox1.ItemIndex of
    0:begin //S88 Kontakt
        label3.Caption := 'S88 - Kontakt Nr.';
        label3.Visible := true;
        spinedit1.Visible := true;
      end;
    1:begin //Magnetartikel
      end;
    2:begin //Zug
      end;
    3:begin //Manuell
      end;
  end;
end;

procedure Tform15.hidefields;
begin
  label3.Visible := false;
  spinedit1.Visible := false;
  label2.Visible := false;
  combobox2.Visible := false;
  combobox3.Visible := false;
  labelededit1.Visible := false;
  combobox4.Visible := false;
  spinedit2.Visible := false;
end;

end.
