unit TraincontrolWindow;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Gauges, ExtCtrls, ImgList, Spin, ComCtrls,
  typehelper, jpg;

type TChangeEvent = procedure(Sender : Tobject; value,adr : integer) of Object;
type
  TTrainControlpanel = class(Tform)
    GroupBox1: TGroupBox;
    Groupbox2: TGroupbox;
    Groupbox3: TGroupbox;
    Image1: TImage;
    Splitter1: TSplitter;
    Panel1: TPanel;
    Label1: TLabel;
    Gauge1: TGauge;
    Panel2: TPanel;
    Image2: TImage;
    TrackBar1: TTrackBar;
    procedure Image2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure TrackbarChange(Sender : TObject);
    procedure image2Click(Sender : TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
  private
    FTrain : TTrain;
    FChangespeed,FChangeDirection : TChangeEvent;
    Fpath,speedStr,adress,name : string;
  protected
    procedure SpeedChange(Sender : Tobject; speed,adr : integer);
    procedure DirectionChange(Sender : Tobject; d,adr : integer);
  public
    Response,externalCall : Boolean;
    FForward : boolean;
    Nr : integer;
    property OnSpeedChange : TchangeEvent read FChangeSpeed write FchangeSpeed;
    property OnDirectionChange : TchangeEvent read FChangeDirection write FchangeDirection;
    property path : string read Fpath;

    constructor create(Owner : Tcomponent; Train : TTrain);

    procedure updateLabel;
    procedure ChangeSpeed(speed,adr : integer);
    procedure ControlSpeed(speed : integer);
  end;

var
  TrainControlpanel : TTrainControlpanel;

implementation

uses main;

{$R *.dfm}

procedure TTrainControlPanel.SpeedChange(Sender : Tobject; speed,adr : integer);
begin
  if Assigned(FChangeSpeed) then
    FChangeSpeed(Sender,speed,adr);
end;

procedure TTrainControlPanel.DirectionChange(Sender : Tobject; d,adr : integer);
begin
  if Assigned(FChangeDirection) then
    FChangeDirection(Sender,d,adr);
end;

procedure TTRainControlPanel.TrackbarChange(Sender : TObject);
begin
  if not externalCall then begin
    SpeedChange(self,trackbar1.Position,Ftrain.Adress);
    MainUDPlistener.stopListener(ciTrainSpeed);
  end else begin
    controlspeed(trackbar1.Position);
  end;
end;

procedure TTRainControlpanel.ControlSpeed(speed : integer);
begin
  trackbar1.SelEnd := speed;
  gauge1.Progress := speed;
  Speedstr := inttostr(speed);
  updatelabel;
end;

procedure TTRainControlPanel.ChangeSpeed(speed,adr : integer);
begin
  SpeedChange(self,speed,adr);
end;

constructor TTrainControlPanel.create(Owner : Tcomponent; Train : TTrain);
begin
  inherited create(Owner);
  FTrain := train;
  trackbar1.OnChange := Trackbarchange;
  image2.OnClick := image2click;
  trackbar1.Position := 0;
  trackbar1.SelStart := 0;
  FForward := true;
  Fpath := form1.Default_SystemPath;
  image2.Picture.bitmap.LoadFromFile(path+'icons\arrow_up.bmp');
  speedStr := inttostr(trackbar1.Position);
  adress := inttostr(train.Adress);
  name := train.Name;
  image1.Picture.LoadFromFile(train.pic);
  trackbar1.Max := 997;
  gauge1.MaxValue := 997;
  Response := false;
  externalCall := false;
  updatelabel;
  MainUDPListener.addListener(ciTrainDirection,self);
  MainUDPListener.addListener(ciTrainSpeed,self);
  show;
end;

procedure TTrainControlPanel.Image2MouseDown(Sender: TObject; Button: TMouseButton;
    Shift: TShiftState; X, Y: Integer);
begin
  panel2.BevelOuter := bvLowered;
end;

procedure TTrainControlPanel.Image2MouseUp(Sender: TObject; Button: TMouseButton;
    Shift: TShiftState; X, Y: Integer);
begin
  panel2.BevelOuter := bvRaised;
end;

procedure TTRainControlPanel.image2Click(Sender : TObject);
begin
  if FForward then begin
    DirectionChange(self,0,Ftrain.Adress);
   // sleep(10);
    if response then begin
      FForward := false;
      image2.Picture.LoadFromFile(path+'icons\arrow_down.bmp');
      image2.Refresh;
    end;
  end else begin
    DirectionChange(self,1,Ftrain.Adress);
  //  sleep(10);
    if response then begin
      FForward := true;
      image2.Picture.LoadFromFile(path+'icons\arrow_up.bmp');
      image2.Refresh;
    end;
  end;
  trackbar1.Position := 0;
  response := false;
end;

procedure TTrainControlPanel.updateLabel;
begin
  label1.Caption := name+'    Adr:'+adress+'         Geschwidigkeit:'+speedStr;
end;

procedure TTrainControlpanel.FormActivate(Sender: TObject);
begin
  form1.activetrain := self.nr;
  form1.StatusBar1.Panels[3].Text := inttostr(self.nr);
end;

procedure TTrainControlpanel.FormDeactivate(Sender: TObject);
begin
  form1.activetrain := -1;
  form1.StatusBar1.Panels[3].Text := ''
end;

end.
 