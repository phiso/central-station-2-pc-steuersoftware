unit turntableDrawThread;

interface

uses
  Classes, turntableGraphics, stdCtrls;

type
  TTurnTableDraw = class(TThread)
  private
    FGraph : TTurntableGraph;
    FParent : Tcomponent;
    FanimSpeed : integer;

    procedure SetAnimSpeed(s : integer);
    procedure circleClick(Sender : Tobject; x,y : integer; deg : double);
    procedure circleMove(Sender : Tobject; x,y : integer; deg : double);
  protected
    procedure Execute; override;
  public
    property Graph : TTurnTableGraph read FGraph;
    property AnimationSpeed : integer read FAnimSpeed write SetAnimSpeed;

    constructor create(Owner : TComponent; animspeed : integer=100);

    procedure Turn(add : real);
    procedure resetPosition;
    procedure RailPosition(c : integer);
    procedure MarkPosition(c : integer);
    procedure TurnTo(angle : real; absolut : boolean);
  end;

implementation

uses digitTurntable;

{ Important: Methods and properties of objects in visual components can only be
  used in a method called using Synchronize, for example,

      Synchronize(UpdateCaption);

  and UpdateCaption could look like,

    procedure TTurnTableDraw.UpdateCaption;
    begin
      Form1.Caption := 'Updated in a thread';
    end; }

{ TTurnTableDraw }

constructor TTurntableDraw.create(Owner : TComponent; animspeed : integer=100);
begin
  inherited create(false);
  FGraph := TTurnTableGraph.create(Owner,animspeed);
  FGraph.OnCircleClick := circleClick;
  FGraph.OnCircleMove := circleMove;
  FAnimspeed := animspeed;
  FParent := Owner;
end;

procedure TTurnTableDraw.Execute;
begin
  { Place thread code here }
end;

procedure TTurnTableDraw.Turn(add : real);
begin
  FGraph.turn(add);
end;

procedure TTurnTableDraw.TurnTo(angle : real; absolut : boolean);
begin
  if absolut then FGraph.TurnToPos(angle,true)
  else FGraph.TurnTo(angle, true);
end;

procedure TTurnTableDraw.resetPosition;
begin
  FGraph.resetPosition;
end;

procedure TTurnTableDraw.RailPosition(c : integer);
begin
  FGraph.RailPosition(c);
end;

procedure TTurnTableDraw.SetAnimSpeed(s : integer);
begin
  FAnimspeed := s;
  FGraph.AnimationSpeed := FAnimSpeed;
end;

procedure TTurnTableDraw.circleClick(Sender : Tobject; x,y : integer; deg : double);
begin
  form4.MouseOnCircle(deg,true);
end;

procedure TTurnTableDraw.circleMove(Sender : Tobject; x,y : integer; deg : double);
begin
  form4.MouseOnCircle(deg,false);
end;

procedure TTurnTableDraw.MarkPosition(c : integer);
begin
  FGraph.MarkPosition(c);
end;

end.
