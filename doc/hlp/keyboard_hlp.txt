###################################Keyboard-Steuerung######################################################################

Mit dem Keyboard k�nnen s�mtliche Magnetartikel auf der Anlage Gesteuert und verwaltet werden.
Im Hauptfenster werden alle durch Neuanlage bzw. dem automatischen Import von Magnetartikeln erstellten Magnetartikel
in separaten Fenster angezeigt.
Werden Magnetartikel mit der Central-Station 2 geschaltet, so wird die Anzeige des entsprechenden Magnetartikels im Keyboard
automatisch aktualisiert.
Beim schalten der Magnetartikel mit der Software ist zu beachten, dass im Gegensatz zur Schaltung von Magnetartikeln in der 
Central-Station 2 alle Stati des Artikels nacheinander durch klicken durchgeschaltet werden.
Soll ein Zustand direkt ausgew�hlt werden, so kann dies �ber das Rechtsklickmen� des jeweiligen Magnetartikels erreicht werden.

�ber die Oberfl�che des Keyboards ist es direkt m�glich einzeln neue Magnetartikel anzulegen oder Magnetartikel aus einer
.cs2 - Datei der Central-Station 2 zu laden.
Dabei ist zu beachten das zum importieren die entsprechenden .cs2 - Datei aus der Central-Staion 2 exportiert werden muss
(exportieren: siehe newProject_hlp.txt) und sich f�r den Import auf dem lokalen Rechner befinden muss.

Ebenfalls ist die direkte Ansteuerung eines nicht Erstellten Magnetartikels m�glich, �ber die Adresse des Jeweiligen Artikels.