unit mainthread;

interface

uses
  Classes, typehelper, forms;

type
  TMainThread = class(TThread)
  private
    { Private declarations }
  protected
    procedure Execute; override;
  public
    constructor create;
  end;

implementation

uses main;

{ Important: Methods and properties of objects in visual components can only be
  used in a method called using Synchronize, for example,

      Synchronize(UpdateCaption);

  and UpdateCaption could look like,

    procedure TMainThread.UpdateCaption;
    begin
      Form1.Caption := 'Updated in a thread';
    end; }

{ TMainThread }

procedure TMainThread.Execute;
begin
  while true do begin
    application.ProcessMessages;
    if MainUDPListener.Stopped then begin
      form1.Stopped := true;
      form1.Button1.Caption := 'GO';
    end else begin
      form1.Stopped := false;
      form1.Button1.Caption := 'STOP';
    end;
  end;
end;

constructor TMainThread.create;
begin
  inherited create(false);
end;

end.
