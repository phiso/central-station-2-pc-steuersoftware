unit linkedaction;

interface

uses Sysutils, typehelper, Classes, package, Controllistenthread, eventlistener;

type TLinkedAction = class
  private
    FStartWith : TBuff;
    Factions : TbuffArray;
    Fpackage : Tpackage;
    FActive : boolean;
    FEvtListener : TEventListener;
    FNr : integer;

    procedure setStart(buff : TBuff);
    function getAction(index : integer):TBuff;
  public
    property StartWith : TBuff read FstartWith write setStart;
    property Action[index:integer] : Tbuff read getAction;

    constructor create(nr : integer);overload;
    constructor create(start : Tbuff; nr : integer);overload;
    constructor create(start : TBuff; actions : TbuffArray; nr : integer);overload;

    procedure AddAction(buff : TBuff);
    procedure StartListening;
    procedure StopListening;
end;

implementation

uses main;

constructor TLinkedAction.create(nr : integer);
begin
  inherited create;
  Fpackage := Tpackage.create(pmcreate);
  FActive := false;
  Fnr := nr;
end;

constructor TlinkedAction.create(start : Tbuff; nr : integer);
var i : integer;
begin
  create(nr);
  for i := 0 to 12 do begin
    FstartWith[i] := start[i];
  end;
  FEvtListener := TeventListener.create(FstartWith,Fnr);
  FEvtListener.StopListening;
end;

constructor TlinkedAction.create(start : TBuff; actions : TbuffArray; nr : integer);
var i,j : integer;
begin
  create(start,nr);
  setlength(FActions,length(actions));
  for i := 0 to length(actions)-1 do begin
    for j := 0 to 12 do begin
      Factions[i][j] := actions[i][j];
    end;
  end;
end;

procedure TlinkedAction.setStart(buff : TBuff);
var i : integer;
begin
  for i := 0 to 12 do begin
    FstartWith[i] := buff[i];
  end;
end;

function TlinkedAction.getAction(index : integer):TBuff;
begin
  result := FActions[index];
end;

procedure TlinkedAction.AddAction(buff : TBuff);
var i : integer;
begin
  setlength(FActions,length(FActions)+1);
  for i := 0 to 12 do begin
    Factions[length(Factions)-1][i] := buff[i];
  end;
end;

procedure TlinkedAction.StartListening;
begin
  FEvtListener.StartListening;
end;

procedure TlinkedAction.StopListening;
begin
  FevtListener.StopListening;
end;

end.
