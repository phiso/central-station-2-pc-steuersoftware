unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, typehelper, Rulehandler, IdBaseComponent, IdComponent,
  IdUDPBase, IdUDPClient, IdUDPServer, Menus, StdCtrls, UDPSend, trainControlwindow,
  IdSocketHandle, ImgList, ToolWin, ComCtrls, ExtCtrls, UDPListenThread,
  package, control, traincontroller, csSystem, trainthread, inifiles, Gridimage,
  math, cs2File, menuitemextend, newLayout, newSystem, Gauges, helpdisplay,
  DxJoystick, gamepadthread, TrayIcon, shellapi, linking, linkedaction_obj;

const CSPORTSEND = 15731;
      CSPORTRCV = 15730;
      ID = $FF;
      HKDELAY = 50;

type
  TForm1 = class(TForm)
    IdUDPClient1: TIdUDPClient;
    IdUDPServer1: TIdUDPServer;
    MainMenu1: TMainMenu;
    Datei1: TMenuItem;
    Einstellungen1: TMenuItem;
    Bearbeiten1: TMenuItem;
    Befehle1: TMenuItem;
    Befehlseditor1: TMenuItem;
    GroupBox1: TGroupBox;
    Edit1: TEdit;
    CentralStation1: TMenuItem;
    ToolBar1: TToolBar;
    ImageList1: TImageList;
    Panel2: TPanel;
    DebugModis1: TMenuItem;
    Button1: TButton;
    Label1: TLabel;
    KeyBoard1: TMenuItem;
    Zge1: TMenuItem;
    NeueLokanlegen1: TMenuItem;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    Neu1: TMenuItem;
    Speichern1: TMenuItem;
    Beenden1: TMenuItem;
    Loks1: TMenuItem;
    ZgeausDatei1: TMenuItem;
    Alleffnen1: TMenuItem;
    Alleschlieen1: TMenuItem;
    ZeigeLok1: TMenuItem;
    Loks2: TMenuItem;
    Gleisbild1: TMenuItem;
    Neu2: TMenuItem;
    ffnen1: TMenuItem;
    PopupMenu1: TPopupMenu;
    Gitter1: TMenuItem;
    Magnetartikel1: TMenuItem;
    ausDatei1: TMenuItem;
    Neu3: TMenuItem;
    ffnenundanzeigen1: TMenuItem;
    Spezial1: TMenuItem;
    Drehscheibe1: TMenuItem;
    Voreinstellungen1: TMenuItem;
    Timer1: TTimer;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ffnen2: TMenuItem;
    ToolButton4: TToolButton;
    Panel3: TPanel;
    StatusBar1: TStatusBar;
    StatusBar2: TStatusBar;
    ProgressBar1: TProgressBar;
    PopupMenu2: TPopupMenu;
    Ausblenden1: TMenuItem;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    Lokfensterverkleinern1: TMenuItem;
    Max1: TMenuItem;
    Red1: TMenuItem;
    minimal1: TMenuItem;
    Portalkran1: TMenuItem;
    DxJoystick1: TDxJoystick;
    TrayIcon1: TTrayIcon;
    PopupMenu3: TPopupMenu;
    Beenden2: TMenuItem;
    Extras1: TMenuItem;
    Hilfe1: TMenuItem;
    DirectSendzurcksetzen1: TMenuItem;
    ToolButton11: TToolButton;
    Fahrstraen1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox2: TGroupBox;
    ScrollBox1: TScrollBox;
    Image1: TImage;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    GroupBox4: TGroupBox;
    PopupMenu4: TPopupMenu;
    Neu4: TMenuItem;
    lschen1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure Befehlseditor1Click(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CentralStation1Click(Sender: TObject);
    procedure DebugModis1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure KeyBoard1Click(Sender: TObject);
    procedure NeueLokanlegen1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Beenden1Click(Sender: TObject);
    procedure ZgeausDatei1Click(Sender: TObject);
    procedure Alleffnen1Click(Sender: TObject);
    procedure Alleschlieen1Click(Sender: TObject);
    procedure Neu2Click(Sender: TObject);
    procedure Gitter1Click(Sender: TObject);
    procedure ausDatei1Click(Sender: TObject);
    procedure ffnen1Click(Sender: TObject);
    procedure ffnenundanzeigen1Click(Sender: TObject);
    procedure Drehscheibe1Click(Sender: TObject);
    procedure Speichern1Click(Sender: TObject);
    procedure Voreinstellungen1Click(Sender: TObject);
    procedure Neu1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Ausblenden1Click(Sender: TObject);
    procedure ffnen2Click(Sender: TObject);
    procedure Red1Click(Sender: TObject);
    procedure Max1Click(Sender: TObject);
    procedure minimal1Click(Sender: TObject);
    procedure Portalkran1Click(Sender: TObject);
    procedure Beenden2Click(Sender: TObject);
    procedure TrayIcon1Click(Sender: TObject);
    procedure Edit1Exit(Sender: TObject);
    procedure DirectSendzurcksetzen1Click(Sender: TObject);
    procedure Hilfe1Click(Sender: TObject);
    procedure GroupBox1Click(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure Fahrstraen1Click(Sender: TObject);
    procedure ToolButton9Click(Sender: TObject);
  private
    RuleFile,TrainFile : string;
    Grid : TGridImage;
    LayoutGrid : array of TGridImage;
    manIp,FHelpCursor : boolean;

    allowDebugg,logging,Fautoip,BoostKey,isUsingPad,
    POVMode, digitalSteer, KeyCraneControl : boolean;

    keyboardWidth, FJoyStickPosX, FJoyStickPosY : integer;

    craneadrs : array[0..1] of integer;

    FDefault_SystemPath, FDefault_RuleFile,
    FDefault_configPath,FDefault_IconPath,
    lokfile,articlefile,FDefaultSave : string;

    FLastDirectCmds : Tstringlist;
    FlastCmdCounter : integer;

    layoutfiles : Tstringlist;
    StatusBarIds : TIntArray;
    FcsIP, FsysName : string;
    HelpHandler : THelpDisplay;
    FJoyStick : TGamePadThread;

    Layouts : Tstringlist;
    LayoutObjects : array of Timage;

    MenuTrains : array of TmenuItemEx;
    TrainItems : array of TmenuItem;
    
    FLinkedObj : array of TLinkedActionObj;

    DebugMode,LayoutMode: boolean;

    Mainpackage : Tpackage;
    MainTrainController : TTraincontroller;
    MainController : TControl;
    MainSystem : TcsSystem;

    procedure InitVars;
    procedure GridOver(Sender : TObject; x,y : integer);
    procedure SaveSystem(path : string);
    procedure LoadSystem(path : string);
    procedure createLoksFromcs2File(f : Tcs2File; nr : integer);
    procedure ClickLok(Sender : TObject; nr : integer);
    procedure loadProjectData;
    procedure SetIp(ip : string);
    procedure ReduceTrainWindowElem(lvl : integer);
    procedure WMHotKey(var Msg : TWMHotKey);message WM_HOTKEY;
    procedure JoyStickMove(Sender : Tobject; axis : string; value : integer);
    procedure JoyStickButtonpress(Sender : Tobject; button : integer; state : boolean);
    procedure JoyStickPovMove(Sender : Tobject; pov : integer);
    procedure LinkedObjClick(Sender : TObject; id : integer);
  public
    Stopped : boolean;

    Trains : array of Ttrain;
    TrainThread : array of TTrainThread;
    activetrain : integer;

    property autoip : boolean read FAutoip;
    property Rules : string read RuleFile;
    property System : TcsSystem read MainSystem;
    property Default_SystemPath : string read FDefault_SystemPath;
    property Default_RuleFile : string read FDefault_RuleFile;
    property Default_configPath : string read FDefault_configPath;
    property Default_IconPath : string read FDefault_IconPath;
    property SysName : string read FSysName;
    property CsIp : string read FCsIp write SetIP;
    property DefaultSave : string read FDefaultSave;
    property isIPManual : boolean read manIP write manIP;
    property HelpCursor : boolean read FHelpCursor write FhelpCursor;
    property isDebug : boolean read Debugmode write debugmode;

    procedure newTrain(data : TTrain; cv:TIntArray=(0));
    procedure applySettings;
    procedure applyKeys(param : integer);
    procedure unregKeys(param : integer);
    procedure newProject(src : string);
    function adduserNotification(str : string):integer;
    function GetStartParam(param : string):string;overload;
    function GetStartParamIndex(param : string):integer;
  end;

var
  Form1: TForm1;
  MainRuleHandler : TRulehandling;
  MainUDPListener : TUDPListener;
  MainUDPSender : TUDPSender;

implementation

uses ruleeditor, debugger, keyboard, newTrain, digitTurntable,
  directorytree, settings, newArticle, digitCrane, hotkeyconfig;

{$R *.dfm}

procedure Tform1.WMHotKey(var Msg : TWMHotKey);
var temp : integer;
begin
  if KeyCraneControl then begin
    with form5 do begin
      case Msg.idHotKey of
        ID:begin
             if form5.Active then begin
               button1MouseDown(self,mbleft,[],0,0);
               pause(HKDELAY);
               button1MouseUp(self,mbleft,[],0,0);
             end;
           end;
        ID+1:begin
               if form5.Active then begin
                 button2MouseDown(self,mbleft,[],0,0);
                 pause(HKDELAY);
                 button2MouseUp(self,mbleft,[],0,0);
               end;
             end;
        ID+2:begin
               if form5.Active then begin
                 button7MouseDown(self,mbleft,[],0,0);
                 pause(HKDELAY);
                 button7MouseUp(self,mbleft,[],0,0);
               end;
             end;
        ID+3:begin
               if form5.Active then begin
                 button6MouseDown(self,mbleft,[],0,0);
                 pause(HKDELAY);
                 button6MouseUp(self,mbleft,[],0,0);
               end;
             end;
        ID+4:begin
               if form5.Active then begin
                 bitbtn2MouseDown(self,mbleft,[],0,0);
                 pause(HKDELAY);
                 bitbtn2MouseUp(self,mbleft,[],0,0);
               end;
             end;
        ID+5:begin
               if form5.Active then begin
                 bitbtn1MouseDown(self,mbleft,[],0,0);
                 pause(100);
                 bitbtn1MouseUp(self,mbleft,[],0,0);
               end;
             end;
        ID+6:begin
               if form5.Active then begin
                 bitbtn3MouseDown(self,mbleft,[],0,0);
                 pause(HKDELAY);
                 bitbtn3MouseUp(self,mbleft,[],0,0);
               end;
             end;
        ID+7:begin
               if form5.Active then begin
                 bitbtn4MouseDown(self,mbleft,[],0,0);
                 pause(HKDELAY);
                 bitbtn4MouseUp(self,mbleft,[],0,0);
               end;
             end;
        ID+8:begin
               if form5.Active then begin
                 if checkbox1.Checked then checkbox1.Checked := false
                 else checkbox1.Checked := true;
               end;
             end;
        ID+9:begin
               if form5.Active then begin
                 if checkbox2.Checked then button9.Click
                 else begin
                   button9MouseDown(self,mbleft,[],0,0);
                   pause(HKDELAY);
                   button9MouseUp(self,mbleft,[],0,0);
                 end;
               end;
             end;
        ID+10:begin
                if (form5.Active) and (BoostKey) then begin
                  BoostControl(200,1);
                end;
              end;
        ID+11:begin
                if (form5.Active) and (BoostKey) then begin
                  BoostControl(200,2);
                end;
              end;
        ID+12:begin
                if (form5.Active) and (BoostKey) then begin
                  BoostControl(200,3);
                end;
              end;
        ID+13:begin
                if (form5.Active) and (BoostKey) then begin
                  BoostControl(200,4);
                end;
              end;
        ID+14:begin
                if (form5.Active) and (BoostKey) then begin
                  BoostControl(200,5);
                end;
              end;
        ID+15:begin
                if (form5.Active) and (BoostKey) then begin
                  BoostControl(200,6);
                end;
              end;
        ID+16:begin
                if (form5.Active) and (BoostKey) then begin
                  BoostControl(200,7);
                end;
              end;
        ID+17:begin
                if (form5.Active) and (BoostKey) then begin
                  BoostControl(200,8);
                end;
              end;
        ID+18:begin
                if form4.Active then begin
                  form4.Button4.Click;
                end;
              end;
        ID+19:begin
                if form4.Active then begin
                  form4.Button3.Click;
                end;
              end;
        ID+20:begin
                if form4.Active then begin
                  form4.Button5.Click;
                end;
              end;
        ID+21:begin
                if form4.Active then begin
                  temp := strtoint(inputbox('Anzusteuernder Anschluss','Nummer des anzusteuernden Anschlusses eingeben',''));
                  if (temp > 0) and (temp < 24) then begin
                    form4.SpinEdit1.Value := temp;
                    form4.Button9.Click;
                  end;
                end;
              end;
      end;
    end;
  end;
end;

procedure Tform1.applySettings;
begin
  groupbox1.Visible := form10.CheckBox5.Checked;
  allowdebugg := form10.CheckBox3.Checked;
  Befehlseditor1.Enabled := form10.CheckBox6.Checked;
  logging := form10.CheckBox4.Checked;
  keyboardWidth := form10.SpinEdit1.Value;
  FDefaultSave := form10.LabeledEdit3.Text;
  FAutoip := form10.CheckBox2.Checked;
  if not Fautoip then Fcsip := form10.LabeledEdit1.Text;
  craneadrs[0] := form10.SpinEdit3.Value;
  craneadrs[1] := form10.SpinEdit4.Value;
  BoostKey := form10.CheckBox11.Checked;
  isUsingPad := form10.CheckBox12.Checked;
  KeyCraneControl := form10.CheckBox14.Checked;
end;

procedure Tform1.applyKeys(param : integer);
begin
  case param of
    0:begin
        RegisterHotKey(handle,ID,0,ord(form10.bridgeKey_f));
        RegisterHotKey(handle,ID+1,0,ord(form10.bridgeKey_b));
        RegisterHotKey(handle,ID+2,0,ord(form10.CraneKey_f));
        RegisterHotKey(handle,ID+3,0,ord(form10.CraneKey_b));
        RegisterHotKey(handle,ID+4,0,ord(form10.TurnKey_f));
        RegisterHotKey(handle,ID+5,0,ord(form10.TurnKey_b));
        RegisterHotKey(handle,ID+6,0,ord(form10.HookKey_f));
        RegisterHotKey(handle,ID+7,0,ord(form10.HookKey_b));
        RegisterHotKey(handle,ID+8,0,ord(form10.LightKey));
        RegisterHotKey(handle,ID+9,0,ord(form10.FunctionKey));
        if BoostKey then begin
          RegisterHotKey(handle,ID+10,MOD_CONTROL,ord(form10.bridgeKey_f));
          RegisterHotKey(handle,ID+11,MOD_CONTROL,ord(form10.bridgeKey_b));
          RegisterHotKey(handle,ID+12,MOD_CONTROL,ord(form10.CraneKey_f));
          RegisterHotKey(handle,ID+13,MOD_CONTROL,ord(form10.CraneKey_b));
          RegisterHotKey(handle,ID+14,MOD_CONTROL,ord(form10.TurnKey_f));
          RegisterHotKey(handle,ID+15,MOD_CONTROL,ord(form10.TurnKey_b));
          RegisterHotKey(handle,ID+16,MOD_CONTROL,ord(form10.HookKey_f));
          RegisterHotKey(handle,ID+17,MOD_CONTROL,ord(form10.HookKey_b));
        end;
      end;
    1:begin
        RegisterHotKey(handle,ID+18,0,ord(form10.TurntableTurn_l));
        RegisterHotKey(handle,ID+19,0,ord(form10.TurntableTurn_r));
        RegisterHotKey(handle,ID+20,0,ord(form10.Turn180));
        RegisterHotKey(handle,ID+21,0,ord(form10.TurnToTarget));
      end;
  end
end;

procedure Tform1.unregKeys(param : integer);
var i : integer;
begin
  case param of
    0:begin
        for i := 0 to 17 do begin
          unregisterHotKey(handle,ID+i);
          GlobalDeleteAtom(ID+i);
        end;
      end;
    1:begin
        for i := 18 to 21 do begin
          unregisterHotKey(handle,ID+i);
          GlobalDeleteAtom(ID+i);
        end;
      end;
  end;
end;


procedure Tform1.InitVars;
begin
  self.DoubleBuffered := true;
  panel2.DoubleBuffered := true;
  groupbox2.DoubleBuffered := true;
  panel3.DoubleBuffered := true;

  FDefault_SystemPath := extractfilepath(application.ExeName);
  FDefault_RuleFile := Default_SystemPath+'data\Rules.ini';
  FDefault_configPath := Default_SystemPath+'data\config';
  FDefault_IconPath := Default_SystemPath+'icons\layout';
  RuleFile := FDefault_RuleFile;
  FCsIp := '0.0.0.0'; //Default Correct Value from file or settings or as parameter
  TrainFile := '';
  ArticleFile := '';
  FDefaultsave := '';
  FsysName := 'none';
  Layouts := Tstringlist.Create;
  FlastDirectCmds := Tstringlist.Create;
  FlastCmdCounter := 0;
  button1.Align := alBottom;
  button1.Height := 50;
  Stopped := false;
  LayoutMode := false;
  allowDebugg := false;
  keyboardwidth := 721;
  logging := true;
  Fautoip := true;
  button1.Align := alBottom;
  edit1.Align := alclient;
  image1.Align := alCLient;
  edit1.Clear;
  progressbar1.Max := 100;
  BoostKey := false;
  activetrain := -1;
  isUsingPad := true;
  POVMode := false;
  digitalSteer := true;
  manIP := false;
  FhelpCursor := false;
  pagecontrol1.ActivePageIndex := 0;
end;

procedure TForm1.FormCreate(Sender: TObject);
var loadproject,startsend,startIP : string;
    pack : Tpackage;
begin
  initvars;
  MainRuleHandler := Trulehandling.create(RuleFile);
  MainUDPSender := TUDPSender.create(idUDPClient1,CsIp,CSPORTSEND);
  MainUDPListener := TUDPListener.create(CSPORTRCV,true,500,self);
  mainPackage := Tpackage.create(pmCreate);
  MainTRainController := TTrainController.create(55,dcMM2Shift);
  MainController := TControl.create(17,dcDCC);
  MainSystem := TcsSystem.create;
  Grid := TGridImage.create(image1);
  Grid.OnGridMouseOver := GridOver;
  MainUDPlistener.addListener(ciSystem,self);
  MainUDPListener.addListener(ciTrainDirection,self);
  MainUDPListener.addListener(ciTrainSpeed,self);
  if isusingPad then begin
    FJoyStick := Tgamepadthread.create;
    FjoyStick.OnStickMove := JoyStickMove;
    FjoyStick.OnButtonPress := JoyStickButtonPress;
    Fjoystick.OnPovMove := JoyStickPovMove;
  end;
  HelpHandler := THelpDisplay.create;

  statusbar2.Top := 0;
  statusbar2.Left := 0;
  statusbar2.Width := panel3.Width-300;
  progressbar1.Width := 300;
  progressbar1.Top := 0;
  progressbar1.Left := statusbar1.Width;

  loadproject := getStartparam('-project');
  startsend := getStartParam('-send');
  startIP := getStartParam('-ip');
  if StartIP <> '' then begin
    Csip := StartIP;
    MainUDPSender.changeHost(csIP);
  end;
  if loadproject <> '' then begin
    Loadsystem(loadproject);
  end;
  if startsend <> '' then begin
    pack := Tpackage.create(pmCreate);
    pack.ParseUserCmd(startsend+' '+paramstr(GetStartParamIndex('-send')+2));
    pack.Free;
  end;
end;

procedure TForm1.Befehlseditor1Click(Sender: TObject);
begin
  Form2.start;
end;

procedure TForm1.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var pack : Tpackage;
begin
  pack := Tpackage.create(pmCreate);
  if (key = VK_RETURN) and (edit1.Text <> '') then begin
    if edit1.Text[1] = '\' then begin
      if pack.ParseUserCmd(edit1.Text) then begin
        FlastDirectCmds.Add(edit1.Text);
        edit1.Clear;
      end;
    end else begin
      MainUDPSender.Send(edit1.Text);
      FlastDirectCmds.Add(edit1.Text);
      edit1.Clear;
    end;
  end else if key = VK_UP then begin
    if FlastCmdCounter < FlastDirectCmds.Count then inc(FlastCmdCounter);
    if FlastDirectCmds.Count > 0 then begin
      edit1.Clear;
      edit1.Text := FlastDirectCmds[FlastDirectCmds.count-FlastCmdCounter];
      edit1.SelStart := length(edit1.Text);
    end;
  end else if key = VK_DOWN then begin
    if FlastCmdCounter > 0 then dec(FlastCmdCounter);
    if FlastDirectCmds.Count > 0 then begin
      edit1.Clear;
      edit1.Text := FlastDirectCmds[FlastDirectCmds.count-(FlastCmdCounter+1)];
      edit1.SelStart := length(edit1.Text);
    end;
  end;
end;

procedure TForm1.CentralStation1Click(Sender: TObject);
begin
  FCsIp := inputbox('Central Station 2 IP-Adresse','IP-Adresse der Central'+
                   'Station 2 eingeben.(Port 15731)',CsIp);
  manIP := true;
  MainUDPSender.changeHost(CsIp);
end;

procedure TForm1.DebugModis1Click(Sender: TObject);
begin
  if DebugModis1.Checked then begin
    DebugMode := true;
    form6.show;
    groupbox1.Show;
  end else begin
    DebugMode := false;
    form6.hide;
    groupbox1.hide;
  end;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  if not stopped then begin
    if MainSystem.SystemStop then begin
      Button1.Caption := 'GO';
      stopped := true;
    end;
  end else begin
    if MainSystem.SystemGo then begin
      stopped := false;
      Button1.Caption := 'STOP';
    end;
  end;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  form12.Close;
  form11.Close;
  form10.Close;
  form9.Close;
  form8.Close;
  form7.Close;
  form6.Close;
  form5.close;
  form4.Close;
  form3.Close;
  form2.Close;
  Trayicon1.Free;
  application.Terminate;
end;

procedure TForm1.KeyBoard1Click(Sender: TObject);
begin
  form3.show;
end;

procedure TForm1.NeueLokanlegen1Click(Sender: TObject);
begin
  form7.showmodal;
  loks1.Enabled := true;
end;

procedure Tform1.newTrain(data : TTrain; cv:TIntArray=(0));
begin
  setlength(trainthread,length(trainthread)+1);
  setlength(trains,length(trains)+1);
  setlength(MenuTrains,length(MenuTrains)+1);

  trains[length(trains)-1] := data;
  Trainthread[length(trainthread)-1] := TTrainThread.create(trains[length(trains)-1],length(trains));

  MenuTRains[length(MenuTrains)-1] := TmenuItemEx.Create(zeigelok1,length(trains));
  MenuTRains[length(MenuTrains)-1].Caption := data.Name;
  MenuTRains[length(MenuTrains)-1].doClick := ClickLok;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  Grid.ToggleGrid();
end;

procedure Tform1.GridOver(Sender : TObject; x,y : integer);
begin
  statusbar1.Panels[0].Text := pointtostr(point(x,y));
end;

procedure Tform1.SaveSystem(path : string);
var ini : Tinifile;
    i : integer;
    name : string;
begin
  if inputQuery('Name','Geben Sie einen Namen f�r Ihr System ein.',name) then
    FsysName := name
  else begin
    Fsysname := extractfilename(path);
    name := Fsysname;
  end;

  if (name = '') or (lowercase(name) = 'default') or (lowercase(name) = 'empty')
  then
    name := extractfilename(path);

  ini := Tinifile.Create(path);
  try
    ini.WriteString('System','cs2IP',csIP);
    ini.WriteString('System','name',name);

    ini.WriteString('Loks','file',TrainFile); //Vollst�ndiger pfad in TRainFile und ArticleFile
    ini.WriteString('Articles','file',ArticleFile);

    for i := 0 to Layouts.Count-1 do begin
      ini.WriteString('Layouts','Layout_'+inttostr(i),Layouts[i]);
    end;
  finally
    ini.Free;
  end;
  showmessage('System erfolgreich gespeichert.');
end;

procedure Tform1.LoadSystem(path : string);
var ini : Tinifile;
    i : integer;
begin
  ini := Tinifile.Create(path);
  layoutfiles := Tstringlist.Create;
  try
    Fsysname := ini.ReadString('INFO','name',extractfilename(path));
    lokfile := ini.ReadString('LOKS','file','');
    articlefile := ini.ReadString('ARTICLES','file','');

    if  ini.ReadInteger('LAYOUT','count',0) > 0 then begin
      for i := 0 to ini.ReadInteger('LAYOUT','count',0)-1 do begin
        layoutfiles.Add(ini.ReadString('LAYOUT','layout_'+inttostr(i),''));
      end;
    end;
    
  finally
    ini.Free;
  end;
  loadprojectdata;
end;

procedure TForm1.Beenden1Click(Sender: TObject);
begin
  application.Terminate;
end;

procedure Tform1.createLoksFromcs2File(f : Tcs2File; nr : integer);
var tempTrain : Ttrain;
begin
  temptrain := f.GetTrain(nr);
  temptrain.pic := Default_SystemPath+'icons\loks\'+temptrain.pic;
  newtrain(temptrain);
end;

procedure TForm1.ZgeausDatei1Click(Sender: TObject);
var tempcs : Tcs2File;
    i : integer;
begin
  opendialog1.InitialDir := Default_SystemPath+'data';
  if opendialog1.Execute then begin
    tempcs := Tcs2File.create(opendialog1.FileName,cfpLoks,cfsOpenRead);
    TrainFile := opendialog1.FileName;
    for i := 0 to tempcs.entries-1 do begin
      createLoksfromcs2File(tempcs,i);
    end;
  end;
  loks1.Enabled := true;
end;

procedure TForm1.Alleffnen1Click(Sender: TObject);
var i : integer;
begin
  for i := 0 to length(TrainThread)-1 do TrainThread[i].Show;
end;

procedure TForm1.Alleschlieen1Click(Sender: TObject);
var i : integer;
begin
  for i := 0 to length(TrainThread)-1 do TrainThread[i].closePanel;
end;

procedure Tform1.ClickLok(Sender : TObject; nr : integer);
begin
  TrainThread[nr-1].Show;
end;

procedure TForm1.Neu2Click(Sender: TObject);
begin
  form8.StartNew(FDefault_IconPath+'\Articles.ini');
end;

procedure TForm1.Gitter1Click(Sender: TObject);
begin
  grid.ToggleGrid();
end;

procedure TForm1.ausDatei1Click(Sender: TObject);
var tempfile : Tcs2File;
begin
  opendialog1.InitialDir := Default_SystemPath+'data';
  if opendialog1.Execute then begin
    tempfile := Tcs2File.create(opendialog1.FileName,cfpArticles,cfsOpenRead);
//    form3.LoadArticlesFromFile(tempfile);
    form3.openNewKeyboard(opendialog1.FileName);
  end;
end;

procedure TForm1.ffnen1Click(Sender: TObject);
begin
  opendialog1.InitialDir := default_SystemPath+'data\save';
  opendialog1.FilterIndex := 2;
  if opendialog1.Execute then begin
    form8.StartExisting(opendialog1.FileName);
  end;
end;

procedure TForm1.ffnenundanzeigen1Click(Sender: TObject);
begin
  opendialog1.InitialDir := Default_SystemPath+'data\save';
  opendialog1.FilterIndex := 2;
  if opendialog1.Execute then begin
    grid.Load(opendialog1.FileName,false);
  end;
end;

procedure TForm1.Drehscheibe1Click(Sender: TObject);
begin
  form4.show
end;

procedure TForm1.Speichern1Click(Sender: TObject);
begin
  savedialog1.InitialDir := default_Systempath+'data\';
  if savedialog1.Execute then begin
    savesystem(savedialog1.FileName);
  end;
end;

procedure TForm1.Voreinstellungen1Click(Sender: TObject);
begin
  form10.showmodal;
end;

procedure Tform1.newProject(src : string);
begin
  LoadSystem(src);
  show;
end;

procedure TForm1.Neu1Click(Sender: TObject);
begin
  form12.Showmodal;
end;

procedure Tform1.loadProjectData;
var tempcs : Tcs2File;
    i : integer;
begin
  tempcs := Tcs2File.create(lokfile,cfpLoks,cfsOpenRead);
  TrainFile := lokfile;
  progressbar1.Max := tempcs.entries;
  for i := 0 to tempcs.entries-1 do begin
    createLoksfromcs2File(tempcs,i);
    progressbar1.Position := i;
  end;
  progressbar1.Position := 0;
  loks1.Enabled := true;
  form3.openNewKeyboard(articlefile);
  setlength(LayoutGrid,layoutfiles.Count);
  if layoutfiles.Count > 0 then Grid.Load(layoutfiles[0],false)
  else addUserNotification('keine Layoutdateien!');
  progressbar1.Position := 0;
end;

function Tform1.adduserNotification(str : string):integer;
begin
  statusbar1.Panels.Add;
  statusbar1.Panels[statusbar1.Panels.Count-1].Width := 100;
  statusbar1.Panels[statusbar1.Panels.Count-1].Text := str;
  result := statusbar1.Panels.Count-1;
  setlength(StatusBarIds,length(StatusBarIds)+1);
  StatusBarIds[length(StatusBarIds)-1] := result;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin //Status�berpr�fungen
  if (Fcsip = '0.0.0.0') or (Fcsip = '') then statusbar1.Panels[1].Text := 'Keine Central Station gefunden!'
  else if not manIP then statusbar1.Panels[1].Text := 'OK'
       else statusbar1.Panels[1].Text := 'CS-IP set by User';

  if FsysName <> 'none' then statusbar1.Panels[2].Text := Fsysname
  else statusbar1.Panels[2].Text := 'Kein Projekt geladen';

  if FHelpCursor then screen.Cursor := crHelp
  else screen.Cursor := crDefault;

  if form10.isStanAlone then begin
    hide;
    form10.Show;
  end;
  if form6.standalone then begin
    hide;
    form6.Show;
  end;
end;

procedure Tform1.SetIp(ip : string);
begin
  FCsIp := ip;
end;

procedure TForm1.FormResize(Sender: TObject);
begin
  statusbar2.Top := 0;
  statusbar2.Left := 0;
  statusbar2.Width := panel3.Width-300;
  progressbar1.Width := 300;
  progressbar1.Top := 0;
  progressbar1.Left := statusbar1.Width;
end;

procedure TForm1.Ausblenden1Click(Sender: TObject);
begin
  groupbox1.Visible := false;
  form10.CheckBox5.Checked := false;
end;

procedure TForm1.ffnen2Click(Sender: TObject);
begin
  if FDefaultSave = '' then opendialog1.InitialDir := Default_SystemPath+'data\save'
  else opendialog1.InitialDir := FDefaultSave;
  if opendialog1.Execute then loadsystem(opendialog1.FileName);
end;

procedure TForm1.Red1Click(Sender: TObject);
begin
  if Red1.Checked then begin
    max1.Checked := false;
    minimal1.Checked := false;
  end else red1.Checked := true;
  ReduceTrainWindowElem(1);
end;

procedure TForm1.Max1Click(Sender: TObject);
begin
  if max1.Checked then begin
    red1.Checked := false;
    minimal1.Checked := false;
  end else max1.Checked := true;
  ReduceTrainWindowElem(0);
end;

procedure TForm1.minimal1Click(Sender: TObject);
begin
  if minimal1.Checked then begin
    max1.Checked := false;
    red1.Checked := false;
  end else minimal1.Checked := true;
  ReduceTrainWindowElem(2);
end;

procedure Tform1.ReduceTrainWindowElem(lvl : integer);
var i : integer;
begin
  for i := 0 to length(TrainThread)-1 do begin
    TrainThread[i].ReducePanelElements(lvl);
  end;
end;

procedure TForm1.Portalkran1Click(Sender: TObject);
begin
  form5.Show;
end;

procedure Tform1.JoyStickMove(Sender : Tobject; axis : string; value : integer);
var temp : integer;
begin
  if assigned(form10) then begin
    if (form10.isChangingJoystick >= 0) and (form10.isChangingJoystick <=3) then
      form10.changeJoystickAxis(form10.isChangingJoystick,axis);
  end;

  if (axis = 'X') or (axis = 'Y') or (axis = 'Z') then statusbar2.Panels[0].Text := axis+': '+inttostr(Value)
  else if (axis = 'RX') or (axis = 'RY') or (axis = 'RZ') then statusbar2.Panels[1].Text := axis+': '+inttostr(Value);
  if (activetrain <> -1) and (axis = 'Y') then begin
    if value < 32570 then
      Trainthread[activetrain-1].FtrainControlPanel.TrackBar1.Position := floor((abs(value-32567)/1000)*33)
    else Trainthread[activetrain-1].FtrainControlPanel.TrackBar1.Position := 0;
  end;
  if (assigned(form5)) and (form5.Active) then begin
    temp := floor((value-32767)/1000)*33;
    with form5 do begin
        if (axis = 'Y') then trackbar1.Position := temp
        else if (axis = 'RY') then trackbar3.Position := temp*(-1)
        else if (axis = 'RX') then trackbar2.Position := temp
        else if (axis = 'Z') then trackbar4.Position := temp;
        if (temp < 20) and (temp > -20) then begin
          trackbar1.position := 0;
          trackbar2.position := 0;
          trackbar3.position := 0;
          trackbar4.position := 0;
        end;
    end;
  end;
end;

procedure Tform1.JoyStickButtonpress(Sender : Tobject; button : integer; state : boolean);
begin
  if state then statusbar2.Panels[2].Text := 'Button '+inttostr(button)
  else statusbar2.Panels[2].Text := 'Button';
  
  if (activetrain <> -1) and (button = 7) and (state = true) then begin
    Trainthread[activetrain-1].FtrainControlPanel.Image2Click(self);
  end;

  if assigned(form10) then begin
    if (form10.isChangingJoystick >= 4) and (form10.isChangingJoystick <= 6) then
      form10.changeJoystickButton(form10.isChangingJoystick,button);
  end;

  if (assigned(form5)) and (form5.Active) then begin
    if (button = 0) and (state = true) then form5.Button9.Click
    else if (button = 2) and (state = true) then begin
      if form5.CheckBox1.Checked = true then form5.CheckBox1.Checked := false
      else form5.CheckBox1.Checked := true;
    end else if (button = 3) and (state = true) then begin
      with form5 do begin
        trackbar1.position := 0;
        trackbar2.position := 0;
        trackbar3.position := 0;
        trackbar4.position := 0;
      end;
    end else if (button = 7) and state then begin
      if POVMode then begin
        POVmode := false;
        statusbar1.Panels[1].Text := 'POV-Modus: Br�cke';
      end else begin
        POVMode := true;
        statusbar1.Panels[1].Text := 'POV-Modus: Kran';
      end;
    end;
  end;
end;

procedure Tform1.JoyStickPovMove(Sender : Tobject; pov : integer);
begin
  statusbar2.Panels[3].Text := 'POV: '+inttostr(pov);
  if assigned(form5) and (form5.Active) then begin
    with form5 do begin
      if digitalSteer then begin
        if POVMode then begin
          case pov of
            -1:begin
                 trackbar2.position := 0;
                 trackbar4.position := 0;
               end;
            0:trackbar4.Position := -500;
            9000:trackbar2.Position := 200;
            18000:trackbar4.Position := 500;
            27000:trackbar2.Position := -200;
          end;
        end else begin
          case pov of
            -1:begin
                 trackbar3.position := 0;
                 trackbar1.position := 0;
               end;
            0:trackbar1.Position := -500;
            9000:trackbar3.Position := 500;
            18000:trackbar1.Position := 500;
            27000:trackbar3.Position := -500;
          end;
        end;
      end;
    end;
  end;
end;

procedure TForm1.Beenden2Click(Sender: TObject);
begin
  application.Terminate;
end;

procedure TForm1.TrayIcon1Click(Sender: TObject);
begin
  popupmenu3.Popup(Mouse.CursorPos.X,Mouse.CursorPos.Y);
end;

procedure TForm1.Edit1Exit(Sender: TObject);
begin
  FlastCmdCounter := 0;
end;

procedure TForm1.DirectSendzurcksetzen1Click(Sender: TObject);
begin
  FlastDirectCmds.Clear;
  FlastCmdCounter := 0;
end;

procedure TForm1.Hilfe1Click(Sender: TObject);
begin
  if not FHelpCursor then FHelpCursor := true
  else FHelpCursor := false;
end;

procedure TForm1.GroupBox1Click(Sender: TObject);
begin
  if FhelpCursor then begin
    Helphandler.DisplayFile('Console_hlp.txt',false);
    FhelpCursor := false;
  end;
end;

function Tform1.GetStartParam(param : string):string;
var i : integer;
begin
  result := '';
  for i := 0 to paramcount do begin
    if paramstr(i) = param then begin
      result := paramstr(i+1);
      exit;
    end;
  end;
end;

function Tform1.GetStartParamIndex(param : string):integer;
var i : integer;
begin
  result := -1;
  for i := 0 to paramcount do begin
    if paramstr(i) = param then begin
      result := i;
      exit;
    end;
  end;
end;

procedure TForm1.ToolButton10Click(Sender: TObject);
begin
  shellexecute(0,'open',Pchar('cs2Shell.exe'),Pchar('-ip '+csip),nil,SW_SHOWNORMAL);
end;

procedure TForm1.FormHide(Sender: TObject);
begin
  self.Hide;
end;

procedure TForm1.Fahrstraen1Click(Sender: TObject);
begin
  form15.Show;
end;

procedure TForm1.ToolButton9Click(Sender: TObject);
begin
  setlength(FLinkedObj,length(FlinkedObj)+1);
  FlinkedObj[length(FLinkedObj)-1] := TLinkedActionObj.create('test',length(FlinkedObj),groupbox4);
  FlinkedObj[length(FLinkedObj)-1].OnActivate := LinkedObjClick;
end;

procedure Tform1.LinkedObjClick(Sender : TObject; id : integer);
begin
  showmessage(inttostr(id)+' Clicked');
end;

end.
