unit directorytree;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ShellCtrls, StdCtrls, ExtCtrls;

type
  TForm11 = class(TForm)
    ShellTreeView1: TShellTreeView;
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    Opener : TObject;
    SrcID : integer;
  public
    procedure OpenTree(Sender : TObject; path : string; SourceID : integer);
  end;

var
  Form11: TForm11;

implementation

uses settings;

{$R *.dfm}

procedure TForm11.Button1Click(Sender: TObject);
begin
  close;
end;

procedure Tform11.OpenTree(Sender : TObject; path : string; SourceID : integer);
begin
  Opener := Sender;
  SrcID := SourceID;
  ShellTreeView1.Path := path;
  showmodal;
end;

procedure TForm11.Button2Click(Sender: TObject);
begin
  if Opener is Tform10 then begin
    case SrcID of
      0:(Opener as Tform10).LabeledEdit2.Text := ShelltreeView1.Path;
      1:(Opener as TForm10).labelededit3.Text := ShellTreeView1.Path;
      2:(Opener as TForm10).labelededit4.Text := ShellTreeView1.Path;
    end;
    form10.SettingsChanged;
    close;
  end else begin
    Showmessage('Called from a not implemented Source');
  end;
end;

procedure TForm11.FormResize(Sender: TObject);
begin
  button1.Align := alLeft;
  button2.Align := alRight; 
end;

end.
