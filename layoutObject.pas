unit layoutObject;

interface

uses Sysutils, Classes, ExtCtrls, Controls, Math, types, typehelper, graphics,
     forms, jpeg, GDIPOBJ, GDIPAPI, control, inifiles;

type TLayoutObject = class
  private
    FTurn,Fstate,FAdr : integer;
    FSrc,Fout : Timage;
    FTyp, FSrcPath, FFileName,
    FDescr, FReferencePath : string;
    FPos : Tpoint;
    Fdec : Tdecoder;
    FControl : TControl;

    {Beim verwenden der RotateFuncktion sowie generell beim verwenden der
     TBitmap Komponente sollte darauf geachtet werden das nur *.bmp Dateien
     geladen werden, da sonst die Verwendung von TBitmap nicht Funktioniert}
    procedure RotateBitmap(Bmp: TBitmap; Degs: Integer; AdjustSize: Boolean;
      BkColor: TColor = clNone);
    procedure SetDescription(str : string);
  public
    property Typ : string read Ftyp;
    property Turned : integer read Fturn;
    property state : integer read FState;
    property position : Tpoint read FPos;
    property Adr : integer read Fadr;
    property DecType : Tdecoder read FDec;
    property Description : string read FDescr write SetDescription;

    constructor create(typ : string; position : Tpoint;
      state:integer=0; turn:integer=0);

    procedure Turn(t : integer);
    procedure TurnNext;
    procedure ChangeState(s : integer);
    procedure ChangePos(x,y : integer);
    procedure putOnImage(var img : Timage);
    function Getgraphics:Tgraphic;
    procedure resetBitmap;
    procedure Assign(source : TLayoutObject);
    procedure Free;
    procedure SetControl(adr : integer; dec : TDecoder);
    procedure SetReference(f : string);
//    procedure SaveToIni(var ini : Tinifile);
end;

implementation

procedure Tlayoutobject.resetBitmap;
begin
  Fout.Picture.Bitmap.Assign(FSrc.Picture.Bitmap);
end;

constructor TLayoutObject.create(typ : string; position : Tpoint;
  state:integer=0; turn:integer=0);
var temp : Tbitmap;
begin
  inherited create;
  FTurn := turn;
  FState := state;
  Ftyp := typ;
  FPos := position;
  Fadr := -1;
  if typ = 'empty' then exit;
  if typ <> 'text' then begin
    FSrcPath := extractfilepath(application.ExeName)+'icons\layout\Gleisbild\';
    Fsrc := Timage.Create(nil);
    FOut := Timage.Create(nil);

    FFileName := FSrcpath+Ftyp+'_'+inttostr(Fstate)+'.bmp';
    Fsrc.Picture.LoadFromFile(FFilename);
    Fout.Picture.LoadFromFile(FFilename);

    if (Fturn > 0) and (Fturn <= 4) then begin
      temp := Tbitmap.Create;
      temp.Assign(FSrc.Picture.Bitmap);
      RotateBitmap(temp,turn*90,true,clwhite);
      Fout.Picture.Bitmap.Assign(temp);
      temp.Free;
    end;
  end;
end;

function TLayoutObject.Getgraphics:Tgraphic;
begin
  if Ftyp = 'text' then exit;
  result := Fout.Picture.Graphic;
end;


procedure TLayoutObject.Turn(t : integer);
var temp : Tbitmap;
begin
  if Ftyp = 'text' then exit;
  if (t > 0) and (t <= 4) then begin
    temp := Tbitmap.Create;
    temp.Assign(FSrc.Picture.Bitmap);
    RotateBitmap(temp,t*90,true,clwhite);
    Fout.Picture.Bitmap.Assign(temp);
    temp.Free;
    Fturn := t;
  end;
end;

procedure TLayoutObject.TurnNext;
begin
  if Ftyp = 'text' then exit;
  case Fturn of
    0:Turn(1);
    1:Turn(2);
    2:Turn(3);
    3:Turn(4);
    4:Turn(1);
  end;
end;

procedure TLayoutObject.ChangeState(s : integer);
var temp : TBitmap;
begin
  if Ftyp = 'text' then exit;
  Fstate := s;
  temp := Tbitmap.Create;
  FSrc.Picture.LoadFromFile(FSrcpath+Ftyp+'_'+inttostr(Fstate)+'.bmp');
  temp.Assign(Fsrc.Picture.Bitmap);
  if (Fturn > 0) and (Fturn <= 4) then
    RotateBitmap(temp,Fturn*90,true,clwhite);
  FOut.Picture.Bitmap.Assign(temp);
  temp.Free;
end;

procedure TlayoutObject.ChangePos(x,y : integer);
begin
  if Ftyp = 'text' then exit;
  Fpos := point(x,y);
end;

procedure TlayoutObject.putOnImage(var img : TImage);
begin
  if Ftyp = 'text' then exit;
  img.Picture.LoadFromFile(FSrcpath+Ftyp+'_'+inttostr(Fstate)+'.bmp');
end;

procedure TLayoutObject.Assign(source : TLayoutObject);
begin
end;

procedure TLayoutobject.Free;
begin
  Fsrc.Free;
  Fout.Free;
  inherited free;
end;


procedure TLayoutObject.RotateBitmap(Bmp: TBitmap; Degs: Integer; AdjustSize: Boolean;
  BkColor: TColor = clNone);
var
  Tmp: TGPBitmap;
  Matrix: TGPMatrix;
  C: Single;
  S: Single;
  NewSize: TSize;
  Graphs: TGPGraphics;
  P: TGPPointF;
begin
  Tmp := TGPBitmap.Create(Bmp.Handle, Bmp.Palette);
  Matrix := TGPMatrix.Create;
  try
    Matrix.RotateAt(Degs, MakePoint(0.5 * Bmp.Width, 0.5 * Bmp.Height));
    if AdjustSize then
    begin
      C := Cos(DegToRad(Degs));
      S := Sin(DegToRad(Degs));
      NewSize.cx := Round(Bmp.Width * Abs(C) + Bmp.Height * Abs(S));
      NewSize.cy := Round(Bmp.Width * Abs(S) + Bmp.Height * Abs(C));
      Bmp.Width := NewSize.cx;
      Bmp.Height := NewSize.cy;
    end;
    Graphs := TGPGraphics.Create(Bmp.Canvas.Handle);
    try
      Graphs.Clear(ColorRefToARGB(ColorToRGB(BkColor)));
      Graphs.SetTransform(Matrix);
      Graphs.DrawImage(Tmp, (Cardinal(Bmp.Width) - Tmp.GetWidth) div 2,
        (Cardinal(Bmp.Height) - Tmp.GetHeight) div 2);
    finally
      Graphs.Free;
    end;
  finally
    Matrix.Free;
    Tmp.Free;
  end;
end;

procedure TlayoutObject.SetControl(adr : integer; dec : TDecoder);
begin
  Fadr := adr;
  Fdec := dec;
end;

procedure TlayoutObject.SetDescription(str : string);
begin
  FDescr := str;
end;

procedure TlayoutObject.SetReference(f : string);
begin
  FReferencepath := f;
end;

end.
