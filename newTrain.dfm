object Form7: TForm7
  Left = 1151
  Top = 130
  Width = 522
  Height = 374
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Neue Lok'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Image13: TImage
    Left = 224
    Top = 136
    Width = 105
    Height = 105
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 265
    Height = 336
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object Image1: TImage
      Left = 0
      Top = 0
      Width = 265
      Height = 145
      Align = alTop
      Proportional = True
    end
    object Label1: TLabel
      Left = 0
      Top = 248
      Width = 38
      Height = 13
      Caption = 'Adresse'
    end
    object Label2: TLabel
      Left = 0
      Top = 200
      Width = 62
      Height = 13
      Caption = 'Decoder-Typ'
    end
    object Button1: TButton
      Left = 150
      Top = 148
      Width = 113
      Height = 25
      Caption = 'Durchsuchen...'
      TabOrder = 0
      OnClick = Button1Click
    end
    object LabeledEdit1: TLabeledEdit
      Left = 0
      Top = 172
      Width = 121
      Height = 21
      EditLabel.Width = 28
      EditLabel.Height = 13
      EditLabel.Caption = 'Name'
      TabOrder = 1
      Text = 'Lok'
    end
    object SpinEdit1: TSpinEdit
      Left = 0
      Top = 264
      Width = 73
      Height = 22
      MaxValue = 1024
      MinValue = 1
      TabOrder = 2
      Value = 1
    end
    object ComboBox1: TComboBox
      Left = 0
      Top = 216
      Width = 153
      Height = 21
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 3
      Text = 'MM2 Codierschalter'
      OnChange = ComboBox1Change
      Items.Strings = (
        'MM2 Codierschalter'
        'MM2 Programmierbar'
        'DCC')
    end
    object Button2: TButton
      Left = 72
      Top = 264
      Width = 75
      Height = 22
      Caption = 'CV-Lesen'
      Enabled = False
      TabOrder = 4
    end
    object Button3: TButton
      Left = 128
      Top = 296
      Width = 129
      Height = 33
      Caption = 'Erstellen'
      TabOrder = 5
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 8
      Top = 296
      Width = 121
      Height = 33
      Caption = 'Abbrechen'
      TabOrder = 6
      OnClick = Button4Click
    end
  end
  object GroupBox1: TGroupBox
    Left = 265
    Top = 0
    Width = 139
    Height = 336
    Align = alClient
    Caption = 'Einstellungen'
    TabOrder = 1
    object Label3: TLabel
      Left = 8
      Top = 16
      Width = 90
      Height = 13
      Caption = 'Anfahrverz'#246'gerung'
    end
    object Label4: TLabel
      Left = 8
      Top = 64
      Width = 88
      Height = 13
      Caption = 'Bremsverz'#246'gerung'
    end
    object Label5: TLabel
      Left = 8
      Top = 160
      Width = 27
      Height = 13
      Caption = 'VMax'
    end
    object Label6: TLabel
      Left = 8
      Top = 112
      Width = 24
      Height = 13
      Caption = 'VMin'
    end
    object Label7: TLabel
      Left = 8
      Top = 208
      Width = 50
      Height = 13
      Caption = 'Lautst'#228'rke'
    end
    object Label8: TLabel
      Left = 8
      Top = 256
      Width = 31
      Height = 13
      Caption = 'Tacho'
    end
    object SpinEdit2: TSpinEdit
      Left = 8
      Top = 32
      Width = 121
      Height = 22
      MaxValue = 255
      MinValue = 0
      TabOrder = 0
      Value = 20
    end
    object SpinEdit3: TSpinEdit
      Left = 8
      Top = 80
      Width = 121
      Height = 22
      MaxValue = 255
      MinValue = 0
      TabOrder = 1
      Value = 20
    end
    object SpinEdit4: TSpinEdit
      Left = 8
      Top = 224
      Width = 121
      Height = 22
      MaxValue = 255
      MinValue = 0
      TabOrder = 2
      Value = 100
    end
    object SpinEdit5: TSpinEdit
      Left = 8
      Top = 128
      Width = 121
      Height = 22
      MaxValue = 255
      MinValue = 0
      TabOrder = 3
      Value = 0
    end
    object SpinEdit6: TSpinEdit
      Left = 8
      Top = 176
      Width = 121
      Height = 22
      MaxValue = 255
      MinValue = 0
      TabOrder = 4
      Value = 255
    end
    object SpinEdit7: TSpinEdit
      Left = 8
      Top = 272
      Width = 121
      Height = 22
      MaxValue = 600
      MinValue = 1
      TabOrder = 5
      Value = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 404
    Top = 0
    Width = 102
    Height = 336
    Align = alRight
    Caption = 'Funktionen'
    TabOrder = 2
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Left = 232
    Top = 176
  end
  object PopupMenu1: TPopupMenu
    Left = 412
    Top = 24
    object FunktionAktiv1: TMenuItem
      AutoCheck = True
      Caption = 'Funktion Aktiv'
      Checked = True
      Default = True
    end
  end
end
