unit ControlListener;

interface

uses typehelper, Sysutils, rcvparser,
     main, articlecontrol, Control, cs2File, csSystem, debugger,
     digitCrane, digitTurntable, GridImage, imageEx, keyboard, layoutObject,
     MenuitemExtend, newArticle, newLayout, newTrain, package, ruleeditor,
     RuleHandler, traincontroller, TraincontrolWindow, trainthread,
     turntableControl, turntableGraphics, UDPlistenThread, UDPsend;

type TListenForControl = class
  private
    FListenFor : TCmdInst;
    FLCmdByte : byte;
    parser : TBuffparser;
  public
    constructor create(listen:string='');overload;
    constructor create(listen:TCmdInst);overload;

    procedure Listen(buff : Tbuff);
end;

implementation

constructor TListenForControl.create(listen:string='');
begin
end;

constructor TListenForControl.create(listen:TCmdInst);
begin
  inherited create;
  FListenFor := listen;
  FLCmdbyte := CmdByte(listen);
  parser := TBuffParser.create;
end;

procedure TlistenForcontrol.Listen(buff : Tbuff);
begin
   parser.ParseBuff(buff,false);
   FListenFor := Parser.Cmd;
  {case FListenFor of
    ciSystem:begin
             end;
    ciTrainDiscover:begin
                    end;
    ciMFXBind:begin
              end;
    ciMFXverify:begin
                end;
    ciTrainSpeed:begin
                 end;
    ciTrainDirection:begin
                     end;
    ciTrainFunction:begin
                    end;
    ciReadConf:begin
               end;
    ciWriteConf:begin
                end;
    ciShiftAccess:begin
                  end;
    ciConfAccess:begin
                 end;
    ciS88Poll:begin
              end;
    ciS88Event:begin
               end;
    ciSX1Event:begin
               end;
    ciPing:begin
           end;
    ciOfferUpdate:begin
                  end;
    ciReadConfData:begin
                   end;
    ciCANBind:begin
              end;
    ciRailsBind:begin
                end;
    ciStateConf:begin
                end;
    ciReqConfData:begin
                  end;
    ciConfDataStream:begin
                     end;
    ciOldDataStream:begin
                    end;
  end; }
end;

end.
