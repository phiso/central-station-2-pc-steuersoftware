object Form3: TForm3
  Left = 652
  Top = 255
  Width = 930
  Height = 420
  BorderStyle = bsSizeToolWin
  Caption = 'Keyboard'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 193
    Height = 362
    Align = alLeft
    Caption = 'Manuell'
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 24
      Width = 41
      Height = 13
      Caption = 'Decoder'
    end
    object Label2: TLabel
      Left = 8
      Top = 64
      Width = 38
      Height = 13
      Caption = 'Adresse'
    end
    object SpinEdit1: TSpinEdit
      Left = 8
      Top = 80
      Width = 81
      Height = 22
      MaxValue = 1014
      MinValue = 1
      TabOrder = 0
      Value = 1
      OnEnter = SpinEdit1Enter
      OnExit = SpinEdit1Exit
    end
    object ComboBox1: TComboBox
      Left = 8
      Top = 40
      Width = 81
      Height = 21
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 1
      Text = 'MM2'
      OnChange = ComboBox1Change
      Items.Strings = (
        'MM2'
        'DCC')
    end
    object GroupBox3: TGroupBox
      Left = 0
      Top = 104
      Width = 185
      Height = 129
      Caption = 'Stellung'
      TabOrder = 2
      object Button1: TButton
        Left = 8
        Top = 16
        Width = 153
        Height = 25
        Caption = 'Gelb/Links/HP02'
        TabOrder = 0
        OnClick = Button1Click
      end
      object Button2: TButton
        Left = 8
        Top = 40
        Width = 153
        Height = 25
        Caption = 'Ein/Gr'#252'n/Gerade/HP1'
        TabOrder = 1
        OnClick = Button2Click
      end
      object Button3: TButton
        Left = 8
        Top = 64
        Width = 153
        Height = 25
        Caption = 'Aus/Rund/Rot/Rechts/HP0'
        TabOrder = 2
        OnClick = Button3Click
      end
      object Button5: TButton
        Left = 8
        Top = 88
        Width = 153
        Height = 25
        Caption = 'Weis/SH0'
        TabOrder = 3
        OnClick = Button5Click
      end
    end
  end
  object ScrollBox1: TScrollBox
    Left = 193
    Top = 0
    Width = 721
    Height = 362
    HorzScrollBar.Smooth = True
    HorzScrollBar.Tracking = True
    Align = alClient
    TabOrder = 1
  end
  object OpenDialog1: TOpenDialog
    Filter = 'cs2 Dateien (.cs2)|*.cs2'
    Left = 160
    Top = 8
  end
  object MainMenu1: TMainMenu
    Left = 128
    Top = 32
    object Magnetartikel1: TMenuItem
      Caption = 'Magnetartikel'
      object Neu1: TMenuItem
        Caption = 'Neu...'
        OnClick = Neu1Click
      end
      object Neuladen1: TMenuItem
        Caption = 'Neu laden'
        OnClick = Neuladen1Click
      end
      object Importieren1: TMenuItem
        Caption = 'Importieren'
        object cs2Datei1: TMenuItem
          Caption = 'cs2 Datei'
        end
        object iniDatei1: TMenuItem
          Caption = 'ini Datei'
        end
      end
    end
  end
end
