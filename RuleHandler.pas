unit RuleHandler;

interface

uses SysUtils, IniFiles, typehelper, Classes, Dialogs;

type TRuleHandling = class
    FSrcFile : Tinifile;
    FSrcPath : string;
    FRules : TRuleArray;
  private
    procedure LoadIni;
    procedure writeNewRule(desc : string);overload;
    procedure writeNewRule;overload;
    function GetRuleIndex(name : string):integer;
  public
    property Rules : TRuleArray read FRules;

    constructor create(path : string);
    procedure newRule(name,buf : string; dyn : Boolean);
    procedure EditRule(index : integer; name,buf : string; dyn : boolean);
    procedure SetDescription(name,desc : string);

    function GetRule(index : integer):TRule; overload;
    function GetRule(name : string):TRule; overload;
    function GetDescription(name : string):string;

    procedure DeleteRule(index : integer); overload;
    procedure DeleteRule(name : string); overload;
end;

implementation

constructor TRuleHandling.create(path : string);
begin
  inherited create;
  FSrcFile := Tinifile.Create(path);
  FSrcPath := path;
  loadini;
end;

procedure TRuleHandling.LoadIni;
var sections : Tstringlist;
    i : integer;
begin
  sections := Tstringlist.Create;
  FSrcFile.ReadSections(sections);
  Setlength(Frules,Sections.Count);
  try
    try
      for i := 0 to sections.Count-1 do begin
        FRules[i].Name := Sections[i];
        FRules[i].Buffer := FSrcFile.ReadString(Sections[i],'Buffer','err');
        FRules[i].IsDynamic := FSrcFile.ReadBool(Sections[i],'dyn',false);
      end;
    except
      showmessage('Errorhandle:Failed to read from ini');//TODO
    end;
  finally
    sections.Free;
  end;
end;

procedure TRuleHandling.newRule(name,buf : string; dyn : Boolean);
begin
  Setlength(Frules,length(Frules)+1);
  FRules[length(FRules)-1].Name := name;
  FRules[length(FRules)-1].Buffer := buf;
  FRules[length(FRules)-1].IsDynamic := dyn;
  WriteNewRule;
end;

procedure TRuleHandling.writeNewRule;
begin
  FSrcFile.WriteString(FRules[length(FRules)-1].name,'Buffer',
                        FRules[length(FRules)-1].buffer);

  FSrcFile.WriteBool(FRules[length(FRules)-1].name,'dyn',
                      FRules[length(FRules)-1].IsDynamic);

  FSrcFile.WriteString(FRules[length(FRules)-1].name,'desc','None');
end;

procedure TRuleHandling.writeNewRule(desc : string);
begin
  FSrcFile.WriteString(FRules[length(FRules)-1].name,'Buffer',
                        FRules[length(FRules)-1].buffer);

  FSrcFile.WriteBool(FRules[length(FRules)-1].name,'dyn',
                      FRules[length(FRules)-1].IsDynamic);

  FSrcFile.WriteString(FRules[length(FRules)-1].name,'desc',desc);
end;

procedure TRuleHandling.SetDescription(name,desc : string);
begin
  FSrcFile.WriteString(name,'Desc',desc);
end;

function TRulehandling.GetRule(index : integer):TRule;
begin
  result := FRules[index];
end;

function Trulehandling.GetRuleIndex(name : string):integer;
var i : integer;
begin
  result := -1;
  for i := 0 to length(Frules)-1 do begin
    if Frules[i].Name = name then begin
      result := i;
      exit;
    end;
  end;
end;

function TRuleHandling.GetRule(name : string):TRule;
var n : integer;
begin
  n := getRuleIndex(name);
  result := FRules[n];
end;

procedure TRuleHandling.EditRule(index : integer; name,buf : string; dyn : boolean);
var oldname : string;
begin
  oldname := FRules[index].Name;
  Frules[index].Name := name;
  Frules[index].Buffer := buf;
  FRules[index].IsDynamic := dyn;
  FSrcFile.EraseSection(oldname);
  FSrcFile.WriteString(name,'Buffer',buf);
  FSrcFile.WriteBool(name,'dyn',dyn);
  FSrcFile.WriteString(name,'desc','None');
end;

procedure TRuleHandling.DeleteRule(index : integer);
var i : integer;
begin
  FSrcFile.EraseSection(Frules[index].Name);
  for i := index+1 to length(FRules)-1 do begin
    Frules[i-1] := Frules[i];
  end;
  Setlength(Frules,length(Frules)-1);
end;

procedure TRulehandling.DeleteRule(name : string);
var i,n : integer;
begin
  n := getRuleIndex(name);
  FSrcFile.EraseSection(Frules[n].Name);
  for i := n+1 to length(FRules)-1 do begin
    Frules[i-1] := Frules[i];
  end;
  Setlength(Frules,length(Frules)-1);
end;

function TRuleHandling.GetDescription(name : string):string;
begin
  result := FSrcFile.ReadString(name,'desc','err');
end;

end.
