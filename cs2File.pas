unit cs2File;
{Liest nur lokomotive.cs2 und magnetartikel.cs2 da die Bestimmung der Artikel
 auf einer Layout Seite nicht m�glich ist wurde diese Date nicht Implementiert}

interface

uses Sysutils, Classes, typehelper, types, dialogs;

type Tcs2File = class
    FFile : Textfile;
    FAll : Tstringlist;
    FFunctions : array of TFunctions;
    FCVValues : array of TCVValues;
    FSection,FKeys,FValues : array of Tstringlist;
    FFilepath : string;
    FType : Tcs2FileType;
    FStreamType : Tcs2FileStream;
    FEntries : integer;
  private
    procedure parseEntries;
    procedure CheckFunctions(lok : integer);
    procedure CheckCV(lok : integer);
    function ParseCSTypeHex(str : string):integer;
    function Parsedecoder(dec : string):TDecoder;
    function ParseElemPos(ele : string):Tpoint;
  public
    property entries : integer read FEntries;

    constructor create(path : string; param : Tcs2FileType; stream : Tcs2FileStream);

    function GetValue(key : string; lok : integer):integer;
    function GetString(key : string; lok : integer):string;
    function GetLoks:Tstringlist;
    function GetArticles:Tstringlist;
    function GetTrain(nr : integer):Ttrain;
    function GetArticle(nr : integer):TArticle;
end;

implementation

constructor Tcs2File.create(path : string; param : Tcs2FileType; stream : Tcs2FileStream);
var tempstr,search : string;
    i,n : integer;
begin
  inherited create;
  FFilePath := path;
  Ftype := param;
  FAll := Tstringlist.Create;
  AssignFile(FFile,path);
  reset(FFile);
  while not eof(FFile) do begin
    readln(FFile,tempstr);
    Fall.Add(tempstr);
  end;
  if stream = cfsOpenRead then Closefile(FFile);

  case param of
    cfpLoks:search := 'lokomotive';
    cfpArticles:search := 'artikel';
    cfpLayout:search := 'element';
  end;
  if  stream = cfsOpenread then begin
    n := 0;
    while n < FAll.Count-1 do begin
      if Fall[n] = search then begin
        setlength(FSection,length(FSection)+1);
        FSection[length(FSection)-1] := Tstringlist.Create;
        inc(n);
        while (FAll[n] <> search) and (n < FAll.Count-1) do begin
          FSection[length(FSection)-1].Add(FAll[n]);
          inc(n);
        end;
      end else inc(n);
    end;
    setlength(Fkeys,length(FSection));
    setlength(FValues,length(FSection));
    FEntries := length(FSection);
    for i := 0 to length(FSection)-1 do begin
      Fkeys[i] := Tstringlist.Create;
      FValues[i] := Tstringlist.Create;
    end;

    parseEntries;
    for i := 0 to length(FSection)-1 do begin
      checkFunctions(i);
      checkCV(i);
    end;
  end;
end;

procedure Tcs2File.parseEntries;
var i,j,p : integer;
    temp : string;
begin
  for i := 0 to length(FSection)-1 do begin
    for j := 0 to FSection[i].Count-1 do begin
      p := pos('=',FSection[i][j]);
      temp := copy(FSection[i][j],3,p-3);
      Fkeys[i].Add(temp);
      FValues[i].Add(copy(FSection[i][j],p+1,255));
    end;
  end;
end;

function Tcs2File.GetValue(key : string; lok : integer):integer;
var i,temp : integer;
begin
  for i := 0 to FKeys[lok].Count-1 do begin
    if FKeys[lok][i] = key then begin
      try
        try
          temp := strToInt(FValues[lok][i]);
        except
          temp := ParseCSTypeHex(FValues[lok][i]);
        end;
      finally
        result := temp;
      end;
      exit;
    end else result := 0;
  end;
end;

function Tcs2File.ParseCSTypeHex(str : string):integer;
var p : integer;
    temp : string;
begin
  p := pos('x',str);
  temp := copy(str,p+1,255);
  result := HexToint(temp);
end;

function Tcs2File.GetString(key : string; lok : integer):string;
var i : integer;
begin
  for i := 0 to Fkeys[lok].Count-1 do begin
    if FKeys[lok][i] = key  then begin
      result := FValues[lok][i];
      exit;
    end else result := 'err';
  end;
end;

procedure Tcs2File.CheckFunctions(lok : integer);
var i : integer;
begin
  setlength(FFunctions,length(FFunctions)+1);
  for i := 0 to FSection[lok].Count-1 do begin
    if FSection[lok][i] = '.funktionen' then begin
      if FSection[lok][i+2] <> '.funktionen' then begin
        setlength(FFunctions,length(FFunctions[length(FFunctions)-1])+1);
        FFunctions[length(FFunctions)-1][length(FFunctions[length(FFunctions)-1])-1].nr := FSection[lok][i+1];
        FFunctions[length(FFunctions)-1][length(FFunctions[length(FFunctions)-1])-1].typ := FSection[lok][i+2];
      end;
    end;
  end;
end;

procedure Tcs2File.CheckCV(lok : integer);
var i : integer;
begin
  setlength(FCVValues,length(FCVValues)+1);
  for i := 0 to FSection[lok].Count-1 do begin
    if FSection[lok][i] = '.prg' then begin
      setlength(FCVvalues,length(FCVValues[length(FCVValues)-1])+1);
      FCVValues[length(FCVValues)-1][length(FCVValues[length(FCVValues)-1])-1].nr := FSection[lok][i+1];
      FCVValues[length(FCVValues)-1][length(FCVValues[length(FCVValues)-1])-1].name := FSection[lok][i+2];
      FCVValues[length(FCVValues)-1][length(FCVValues[length(FCVValues)-1])-1].val := FSection[lok][i+3];
    end;
  end;
end;

function Tcs2File.GetLoks:Tstringlist;
var i : integer;
begin
  result := Tstringlist.Create;
  for i := 0 to length(FSection)-1 do begin
    result.Add(getString('name',i));
  end;
end;

function Tcs2File.GetArticles:Tstringlist;
var i : integer;
begin
  result := Tstringlist.Create;
  for i := 0 to length(FSection)-1 do begin
    result.Add(getString('name',i)+getstring('typ',i));
  end;
end;

function Tcs2File.Parsedecoder(dec : string):Tdecoder;
begin
  if (dec='mm2_dil8') or (dec='mm2_dil4') then result := dcMM2Shift
  else if dec='mm2_prg' then result := dcMM2Prog
  else if dec='dcc' then result := dcDCC
  else if dec='mfx' then result := dcMFX
  else result := dcMM2Prog;
end;

function Tcs2File.GetTrain(nr : integer):Ttrain;
begin
  result.Name := getstring('name',nr);
  result.pic := getstring('icon',nr)+'.jpg';
  result.decoder := Parsedecoder(getstring('typ',nr));
  if (result.decoder = dcDCC) or (result.decoder = dcMFX) then
    result.Adress := getvalue('adresse',nr)
  else result.Adress := getvalue('uid',nr);
  result.anf := getvalue('av',nr);
  result.brems := getvalue('bv',nr);
  if getvalue('vmin',nr) = -1 then
    result.vmin := 0
  else result.vmin := getvalue('vmin',nr);
  if getvalue('vmax',nr) = -1 then
    result.vmax := 255
  else result.vmax := getvalue('vmax',nr);
  result.loud := getvalue('volume',nr);
  result.tacho := getvalue('tachomax',nr);
end;

function Tcs2File.GetArticle(nr : integer):TArticle;
begin
  result.Adr := getvalue('id',nr);
  result.State := getvalue('stellung',nr);
  result.shiftTime := getvalue('schaltzeit',nr);
  result.dectype := parsedecoder(getstring('dectyp',nr));
  result.Typ := getstring('typ',nr);
  result.name := getstring('name',nr);
end;

function Tcs2File.ParseElemPos(ele : string):Tpoint;
var temp : string;
    p : integer;
begin
  p := pos('x',ele);
  temp := copy(ele,p+2,255);
  if length(temp) <> 4 then showmessage('err in parsing: elem id');
end;

end.
