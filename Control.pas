unit Control;

interface

uses typehelper, Sysutils, Classes, RuleHandler, package;

type TControl = class
    FAdress : integer;
    FDecoder : TDecoder;
    FPackage : Tpackage;
    FBuffer : Tbuffer;
  private
    function Checkresponse(cmd : TBuffer):boolean;
  public
    constructor create(adr : integer; dc : TDecoder);

    function ShiftSwitch(way : byte):boolean;
end;

implementation

uses main;

constructor Tcontrol.create(adr : integer; dc : TDecoder);
begin
  inherited create;
  FDecoder := dc;
  FAdress := adr;
  FBuffer.decType := dc;
  Fpackage := Tpackage.create(pmCreate);
end;

function TControl.Checkresponse(cmd : TBuffer):boolean;
var resp : Tbuff;
begin
  resp := MainUDPListener.ResponseBuff;
  if ord(resp[1]) = cmd.cmdbyte+1 then result := true
    else result := false;
end;

function TControl.ShiftSwitch(way : byte):boolean;
begin
  Fbuffer.cmdbyte := 22;
  FBuffer.Adress := FAdress;
  FBuffer.DLCbyte := 6;
  FBuffer.B2Val[0] := way;
  FBuffer.B2Val[1] := 1;
  Fpackage.Buildpackage(FBuffer,1);
  Fpackage.SendPackage(false);
  sleep(0);
  result := Checkresponse(Fbuffer);
end;

end.
