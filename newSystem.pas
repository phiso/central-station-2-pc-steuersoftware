unit newSystem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, inifiles;

type
  TForm12 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    LabeledEdit1: TLabeledEdit;
    LabeledEdit2: TLabeledEdit;
    Button3: TButton;
    Button4: TButton;
    LabeledEdit3: TLabeledEdit;
    OpenDialog1: TOpenDialog;
    ListBox1: TListBox;
    Label1: TLabel;
    SaveDialog1: TSaveDialog;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form12: TForm12;

implementation

uses main;

{$R *.dfm}

procedure TForm12.Button1Click(Sender: TObject);
begin //Loks Datei
  opendialog1.InitialDir := form1.Default_SystemPath;
  opendialog1.FilterIndex := 1;
  if opendialog1.Execute then begin
    labelededit1.Text := opendialog1.FileName;
  end;
end;

procedure TForm12.Button2Click(Sender: TObject);
begin //Magnetartikel Datei
  opendialog1.InitialDir := form1.Default_SystemPath;
  opendialog1.FilterIndex := 1;
  if opendialog1.Execute then begin
    labelededit2.Text := opendialog1.FileName;
  end;
end;

procedure TForm12.Button3Click(Sender: TObject);
var i : integer;
begin //Gleisbilder Dateien
  opendialog1.InitialDir := form1.Default_SystemPath;
  opendialog1.FilterIndex := 2;
  if opendialog1.Execute then begin
    for i := 0 to opendialog1.Files.Count-1 do begin
      listbox1.Items.Add(opendialog1.Files[i]);
    end;
  end;
end;

procedure TForm12.Button4Click(Sender: TObject);
var ini : Tinifile;
    i : integer;
begin
  savedialog1.InitialDir := form1.Default_SystemPath;
  if savedialog1.Execute then begin
    ini := Tinifile.Create(savedialog1.FileName);
    try
      if (labelededit3.Text = '') or (labelededit3.Text = 'empty') or (labelededit3.Text = 'default') then
        ini.WriteString('INFO','Name',extractfilename(savedialog1.FileName))
      else ini.WriteString('INFO','Name',labelededit3.Text);
      
      ini.WriteString('LOKS','file',labelededit1.Text);
      ini.WriteString('ARTICLES','file',labelededit2.Text);
      ini.WriteInteger('LAYOUTS','count',listbox1.Count);
      for i := 0 to listbox1.Count-1 do begin
        ini.WriteString('LAYOUTS','layout_'+inttostr(i),listbox1.Items[i]);
      end;
    finally
      ini.Free;
    end;
    form1.newProject(savedialog1.FileName);
  end;
  close;
end;

end.
