unit digitTurntable;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Turntablegraphics, Spin, Turntablecontrol, ComCtrls,
  ExtCtrls, typehelper, Menus, inifiles, math;

type
  TForm4 = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Button5: TButton;
    Button6: TButton;
    Button8: TButton;
    StatusBar1: TStatusBar;
    GroupBox3: TGroupBox;
    Button3: TButton;
    Button4: TButton;
    GroupBox4: TGroupBox;
    Button9: TButton;
    GroupBox5: TGroupBox;
    Button2: TButton;
    Button7: TButton;
    Button1: TButton;
    Button10: TButton;
    CheckBox1: TCheckBox;
    LabeledEdit1: TLabeledEdit;
    ComboBox1: TComboBox;
    SpinEdit1: TSpinEdit;
    PopupMenu1: TPopupMenu;
    benennen1: TMenuItem;
    Button11: TButton;
    MainMenu1: TMainMenu;
    Drehscheibe1: TMenuItem;
    Animationsgeschwindigkeit1: TMenuItem;
    N01: TMenuItem;
    LabeledEdit2: TLabeledEdit;
    procedure Button1Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure benennen1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure N01Click(Sender: TObject);
  private
    Turntable : TTurnTableGraph;
    TurntableAdr, Animspeed, FActPos, PrevAdr : integer;
    TurntableControl : TTurntableControl;
    Mapp : array of array[0..1] of integer;
    DefPath : string;
    FisEnabled, FexternalCall : boolean;

    function getmapping(src : string):TIntArray;
    procedure saveConfig(f : string);
    procedure getConfig(f : string);
  public
    property ExtCall : boolean read FExternalCall;
    property isEnabled : boolean read FisEnabled;
    procedure applySettings;
    procedure MouseOnCircle(p : double; clicked : boolean);
    procedure startExtCall;
    procedure StopExtCall;
  end;

var
  Form4: TForm4;

implementation

uses main, settings;

{$R *.dfm}

procedure Tform4.applySettings;
begin
  with form10 do begin
    if checkbox7.Checked then begin
      TurnTableAdr := ((SpinEdit2.Value-1)*16)-1;
      animspeed := form10.SpinEdit5.Value;
    end else begin
      TurnTableAdr := 225;
      animspeed := 0;
    end;
    FisEnabled := checkbox7.Checked;
  end;
end;

procedure TForm4.Button1Click(Sender: TObject);
begin
  TurnTableControl.ending;
end;

procedure TForm4.Button4Click(Sender: TObject);
begin
//  TurnTable.turn(7.5);
  TurntableControl.StepForward;
end;

procedure TForm4.Button3Click(Sender: TObject);
begin
//  TurnTable.turn(7.5*(-1));
  TurntableControl.StepBackward;
end;

procedure TForm4.Button2Click(Sender: TObject);
begin
  Turntablecontrol.TurnTo(1);
end;

procedure TForm4.Button5Click(Sender: TObject);
begin
  TurntableControl.turn;
//  Turntable.TurnTo(180,true);
end;

procedure TForm4.FormCreate(Sender: TObject);
begin
  TurntableAdr := 225;
  animspeed := 0;
  PrevAdr := -1;
  DefPath := form1.Default_SystemPath+'data\conf\turntable.ini';
  FisEnabled := true;
  FExternalCall := false;
end;

procedure TForm4.Button6Click(Sender: TObject);
begin
  TurntableControl.StartProgramming;
end;

procedure TForm4.Button7Click(Sender: TObject);
begin
  Turntablecontrol.input;
end;

procedure TForm4.Button8Click(Sender: TObject);
var map, map2 : TIntArray;
    i : integer;
begin
  if application.MessageBox('Nach dem Start der automatischen Programmierung'+#13#10+
                            'd�rfen keine Eingaben an der Central Station erfolgen'+#13#10+
                            'bis die Programmierung abgeschlossen ist.'+#13#10+
                            'Ansonsten kommt es zu Fehlern bei der Programmierung','Warnung',mb_OK or mb_ICONWARNING) = IDOK then
  begin
    if (labelededit2.Text <> '') and (labelededit2.Text <> ' ') then begin
      map2 := getmapping(labelededit2.Text);
      TurnTableControl.autoMapping(map2);
    end else begin
      if checkbox1.checked then begin
        for i := 1 to 24 do begin
          combobox1.Items.Add(inttostr(i));
        end;
        TurntableControl.autoMapping
      end else begin
        map := getmapping(labelededit1.Text);
        for i := 0 to length(map)-1 do begin
          combobox1.Items.Add(inttostr(map[i]));
        end;
        TurntableControl.Mapping(map);
      end;
    end;
  end;
end;

procedure TForm4.Button9Click(Sender: TObject);
var turn : integer;
begin
  turn := spinedit1.Value;
  if turn > 24 then turn := turn - 24;
  TurntableControl.TurnTo(turn);
end;

procedure TForm4.FormShow(Sender: TObject);
begin
  TurntableControl := TTurntableControl.create(groupbox2,Defpath);
  MainUDPListener.addListener('csTurnTable',self);
  if not FisEnabled then Showmessage('Die Verwendung einer Drehscheibe ist nicht Konfiguriert');
  form1.applyKeys(1);
end;

procedure TForm4.Button10Click(Sender: TObject);
begin
  TurntableControl.clear;
end;

procedure TForm4.Button11Click(Sender: TObject);
begin
//  TurntableControl.mark(3);
end;

function TForm4.getmapping(src : string):TIntArray;
var p : integer;
    temp,posi : string;
begin
  temp := src;
  p := pos(',',temp);
  while p <> 0 do begin
    posi := copy(temp,0,p-1);
    temp := copy(temp,p+1,255);
    setlength(result,length(result)+1);
    result[length(result)-1] := strtoint(posi);
    p := pos(',',temp);
  end;
  setlength(result,length(result)+1);
  result[length(result)-1] := strtoint(temp);
end;

procedure TForm4.CheckBox1Click(Sender: TObject);
begin
  if checkbox1.Checked then begin
    labelededit1.Visible := false;
    button11.Visible := false;
  end else begin
    labelededit1.Visible := true;
    button11.Visible := true;
  end;
end;

procedure TForm4.ComboBox1Change(Sender: TObject);
begin
  spinedit1.Value := strToInt(combobox1.text);
end;

procedure TForm4.benennen1Click(Sender: TObject);
var inp : string;
begin
  if inputQuery('Benennen','Geben Sie einen Namen f�r die Position an.',inp) then
  begin
    setlength(Mapp,length(Mapp)+1);
    Mapp[length(Mapp)-1][0] := strtoint(combobox1.Items[combobox1.ItemIndex]);
    Mapp[length(Mapp)-1][1] := combobox1.ItemIndex;
    combobox1.Items[combobox1.ItemIndex] := inp+' ('+combobox1.Items[combobox1.ItemIndex]+')';
  end;
end;

procedure Tform4.saveConfig(f : string);
var ini : Tinifile;
    i : integer;
begin
  ini := Tinifile.Create(f);
  try
    try
      ini.WriteInteger('INFO','Address',TurntableAdr);
    except
      ini.WriteInteger('INFO','Address',225);
    end;
    for i := 0 to combobox1.Items.Count-1 do begin
      ini.WriteInteger('Positions','pos_'+inttostr(i),Mapp[i][0]);
      try
        ini.WriteString('Naming','name_'+inttostr(i),combobox1.Items[Mapp[i][1]]);
      except
        ini.WriteString('Naming','name_'+inttostr(i),'none');
      end;
    end;
  finally
    ini.Free;
  end;
end;

procedure Tform4.getConfig(f : string);
var ini : Tinifile;
begin
  ini := Tinifile.Create(f);
  try
    TurntableAdr := ini.ReadInteger('INFO','Address',225);

  finally
    ini.Free;
  end;
end;

procedure Tform4.MouseOnCircle(p : double; clicked : boolean);
var tadr,tadr_s : integer;
begin
  tadr := floor((p+3.75)/7.5)+13;
  if tadr > 48 then tadr := tadr - 48;
  FActPos := tadr;
  if clicked then begin
    if tadr > 24 then tadr_s := tadr - 24
    else tadr_s := tadr;
    {if (tadr-2)>=0 then TurnTableControl.MarkCirclePos(tadr-2);
    if PrevAdr <> -1 then TurnTableControl.MarkCirclePos(PrevAdr-2);}
    statusbar1.Panels[0].Text := 'Moving To Position '+inttostr(tadr);
    PrevAdr := tadr;
    TurnTableControl.TurnTo(tadr_s);
  end else begin
    statusbar1.Panels[1].Text := 'Position: '+inttostr(tadr);
  end;
end;

procedure TForm4.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  form1.unregKeys(1);
  Turntablecontrol.close;
end;

procedure TForm4.N01Click(Sender: TObject);
begin
  animspeed := strtoint(inputbox('Animationsgeschwindigkeit','0-100','0'));
  TurntableControl.SetAnimSpeed(animspeed);
  N01.Caption := inttostr(animspeed);
end;

procedure Tform4.startExtCall;
begin
  FExternalCall := true;
  TurntableControl.StartExtCall;
end;

procedure Tform4.StopExtCall;
begin
  FExternalCall := false;
  TurnTableControl.StopExtCall;
end;

end.
