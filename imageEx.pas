unit imageEx;

interface

uses Extctrls, Sysutils, Classes, controls;

type TClickEvent = procedure(Sender : Tobject; nr : integer) of object;
type TImageEx = class(Timage)
  private
    Fclick,Fdown,Fup,FMouse : TClickEvent;
    Fnr : integer;
    procedure Clicking(Sender : Tobject);
    procedure ondown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure onUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  protected
    procedure Click(Sender : TObject; nr : integer);
    procedure down(Sender : Tobject; nr : integer);
    procedure up(Sender : Tobject; nr : integer);
    procedure MouseOn(Sender : TObject; nr : integer);
  public
    property DoClick : TClickEvent read FClick write Fclick;
    property MouseDown : TclickEvent read FDown write FDown;
    property MouseUp : TClickEvent read FUp write Fup;
    property IsMouseOn : TClickEvent read FMouse write FMouse;

    constructor create(Owner : Tcomponent; nr : integer);
end;

implementation

constructor TImageEx.create(Owner : Tcomponent; nr : integer);
begin
  inherited create(Owner);
  Fnr := nr;
  self.OnClick := clicking;
  self.OnMouseDown := ondown;
  self.OnMouseUp := onUp;
  self.OnMouseMove := MouseMove;
end;

procedure TimageEX.Click(Sender : TObject; nr : integer);
begin
  if Assigned(Fclick) then
    Fclick(Sender,nr);
end;

procedure TimageEx.Clicking(Sender : Tobject);
begin
  Click(Sender,Fnr);
end;

procedure TimageEx.down(Sender : Tobject; nr : integer);
begin
  if Assigned(Fdown) then
    Fdown(Sender,nr);
end;

procedure TimageEx.MouseOn(Sender : TObject; nr : integer);
begin
  if Assigned(FMouse) then
    FMouse(Sender,nr);
end;

procedure TimageEx.up(Sender : Tobject; nr : integer);
begin
  if Assigned(Fup) then
    Fup(Sender,nr);
end;

procedure TimageEx.ondown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  down(Sender,Fnr);
end;

procedure TimageEx.onUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Up(Sender, Fnr);
end;

procedure TimageEx.MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  MouseOn(Sender,Fnr);
end;

end.
