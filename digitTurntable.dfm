object Form4: TForm4
  Left = 927
  Top = 238
  BorderStyle = bsToolWindow
  Caption = 'Drehscheibe'
  ClientHeight = 491
  ClientWidth = 646
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 461
    Top = 0
    Width = 185
    Height = 472
    Align = alRight
    Caption = 'Kontrollen'
    TabOrder = 0
    object Button5: TButton
      Left = 8
      Top = 176
      Width = 81
      Height = 25
      Caption = '180'#176' Drehung'
      TabOrder = 0
      OnClick = Button5Click
    end
    object Button6: TButton
      Left = 8
      Top = 208
      Width = 145
      Height = 25
      Caption = 'Programmiermodus starten'
      TabOrder = 1
      OnClick = Button6Click
    end
    object Button8: TButton
      Left = 8
      Top = 240
      Width = 161
      Height = 25
      Caption = 'Automatische Programmierung'
      TabOrder = 2
      OnClick = Button8Click
    end
    object GroupBox3: TGroupBox
      Left = 8
      Top = 16
      Width = 169
      Height = 57
      Caption = 'Step'
      TabOrder = 3
      object Button3: TButton
        Left = 8
        Top = 16
        Width = 73
        Height = 33
        Caption = '<<<'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        OnClick = Button3Click
      end
      object Button4: TButton
        Left = 88
        Top = 16
        Width = 73
        Height = 33
        Caption = '>>>'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        OnClick = Button4Click
      end
    end
    object GroupBox4: TGroupBox
      Left = 8
      Top = 72
      Width = 169
      Height = 97
      Caption = 'Ziel anfahren'
      TabOrder = 4
      object Button9: TButton
        Left = 8
        Top = 66
        Width = 153
        Height = 25
        Caption = 'anfahren'
        TabOrder = 0
        OnClick = Button9Click
      end
      object ComboBox1: TComboBox
        Left = 8
        Top = 40
        Width = 153
        Height = 21
        ItemHeight = 13
        PopupMenu = PopupMenu1
        TabOrder = 1
        Text = 'Ziele'
        OnChange = ComboBox1Change
      end
      object SpinEdit1: TSpinEdit
        Left = 8
        Top = 16
        Width = 153
        Height = 22
        MaxValue = 24
        MinValue = 0
        TabOrder = 2
        Value = 0
      end
    end
    object GroupBox5: TGroupBox
      Left = 8
      Top = 384
      Width = 169
      Height = 81
      Caption = 'Steuerbefehle'
      TabOrder = 5
      object Button2: TButton
        Left = 88
        Top = 16
        Width = 73
        Height = 25
        Caption = 'Reset'
        TabOrder = 0
        OnClick = Button2Click
      end
      object Button7: TButton
        Left = 8
        Top = 16
        Width = 73
        Height = 25
        Caption = 'input'
        TabOrder = 1
        OnClick = Button7Click
      end
      object Button1: TButton
        Left = 8
        Top = 48
        Width = 75
        Height = 25
        Caption = 'end'
        TabOrder = 2
        OnClick = Button1Click
      end
      object Button10: TButton
        Left = 88
        Top = 48
        Width = 73
        Height = 25
        Caption = 'clear'
        TabOrder = 3
        OnClick = Button10Click
      end
    end
    object CheckBox1: TCheckBox
      Left = 8
      Top = 272
      Width = 97
      Height = 17
      Caption = 'Linear'
      Checked = True
      State = cbChecked
      TabOrder = 6
      OnClick = CheckBox1Click
    end
    object LabeledEdit1: TLabeledEdit
      Left = 8
      Top = 352
      Width = 161
      Height = 21
      EditLabel.Width = 41
      EditLabel.Height = 13
      EditLabel.Caption = 'Mapping'
      TabOrder = 7
      Visible = False
    end
    object Button11: TButton
      Left = 56
      Top = 335
      Width = 75
      Height = 16
      Caption = 'speichern'
      TabOrder = 8
      Visible = False
    end
    object LabeledEdit2: TLabeledEdit
      Left = 8
      Top = 312
      Width = 161
      Height = 21
      EditLabel.Width = 93
      EditLabel.Height = 13
      EditLabel.Caption = 'Belegte Anschl'#252'sse'
      TabOrder = 9
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 0
    Width = 461
    Height = 472
    Align = alClient
    Caption = 'Drehscheibe'
    TabOrder = 1
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 472
    Width = 646
    Height = 19
    Panels = <
      item
        Width = 300
      end
      item
        Width = 150
      end
      item
        Width = 50
      end>
  end
  object PopupMenu1: TPopupMenu
    Left = 604
    Top = 104
    object benennen1: TMenuItem
      Caption = 'benennen'
      OnClick = benennen1Click
    end
  end
  object MainMenu1: TMainMenu
    Left = 8
    Top = 24
    object Drehscheibe1: TMenuItem
      Caption = 'Drehscheibe'
      object Animationsgeschwindigkeit1: TMenuItem
        Caption = 'Animationsgeschwindigkeit'
        object N01: TMenuItem
          Caption = '0'
          OnClick = N01Click
        end
      end
    end
  end
end
