unit ControlListenThread;

interface

uses
  Classes, typehelper, rcvparser, Sysutils;

type
  TControlListener = class(TThread)
  private
    FListenFor : TCmdInst;
    FLCmdByte : byte;
    FParser : TBuffparser;
    FAdress, FSpeed : integer;
    FBuff : TBuff;
    FReceiver : TObject;
    FListenString : string;
    Fstopped : boolean;

    procedure Setreceiver(obj : TObject);
  protected
    procedure Execute; override;
  public
    property Sender : Tobject read FReceiver write setReceiver;
    property isStopped : boolean read Fstopped;
    property Cmd : TCmdInst read FlistenFor;

    constructor create(listen:string='');overload;
    constructor create(listen:TCmdInst);overload;
    constructor create(listen:TCmdInst; Receiver : TObject);overload;

    procedure Listen(buff : Tbuff);
    procedure stopListening;
    procedure startListening;
  end;

implementation

uses main, articlecontrol, Control, cs2File, csSystem, debugger,
     digitCrane, digitTurntable, GridImage, imageEx, keyboard, layoutObject,
     MenuitemExtend, newArticle, newLayout, newTrain, package, ruleeditor,
     RuleHandler, traincontroller, TraincontrolWindow, trainthread,
     turntableControl, turntableGraphics, UDPlistenThread, UDPsend;

{ Important: Methods and properties of objects in visual components can only be
  used in a method called using Synchronize, for example,

      Synchronize(UpdateCaption);

  and UpdateCaption could look like,

    procedure TControlListener.UpdateCaption;
    begin
      Form1.Caption := 'Updated in a thread';
    end; }

{ TControlListener }

constructor TControlListener.create(listen:string='');
begin
  inherited create(false);
  FlistenString := listen;
  if listen = 'csTurnTable' then FlistenFor := ciShiftAccess;
  Fparser := TBuffParser.create;
  Fstopped := false;
end;

constructor TControlListener.create(listen:TCmdInst);
begin
  create;
  FListenFor := listen;
  FLCmdbyte := CmdByte(listen);
end;

constructor TControlListener.create(listen:TCmdInst; Receiver : TObject);
begin
  create(listen);
  FReceiver := Receiver;
end;

procedure TControlListener.Execute;
begin

end;

procedure TControlListener.Setreceiver(obj : TObject);
begin
  Freceiver := obj;
end;

procedure TControlListener.Listen(buff : TBuff);
var i,s : integer;
begin
  if Fstopped then exit;
  
  FParser.ParseBuff(buff,false);
  if Fparser.Cmd = FListenFor then begin
    case FListenFor of
      ciSystem:begin
                 case ord(buff[9]) of
                   0:begin
                       if FReceiver is TForm1 then begin
                         (FReceiver as Tform1).Button1.Caption := 'GO';
                         (FReceiver as Tform1).Stopped := true;
                       end;
                     end;
                   1:begin
                       if FReceiver is TForm1 then begin
                         (FReceiver as Tform1).Button1.Caption := 'STOP';
                         (FReceiver as Tform1).Stopped := false;
                       end;
                     end;
                   2:begin
                     end;
                   3:begin
                     end;
                   4:begin
                     end;
                 end;
               end;
      ciTrainDirection:begin
                         FAdress := FParser.Adress;
                         if FReceiver is TForm1 then begin
                           with (FReceiver as TForm1) do begin
                             for i := 0 to length(Trains)-1 do begin
                               if Trains[i].Adress = Fadress then begin
                                 with Trainthread[i].FtrainControlPanel do begin
                                   case ord(buff[9]) of
                                     1:begin
                                         FForward := true;
                                         image2.Picture.LoadFromFile(path+'icons\arrow_up.bmp');
                                         image2.Refresh;
                                       end;
                                     2:begin
                                         FForward := false;
                                         image2.Picture.LoadFromFile(path+'icons\arrow_down.bmp');
                                         image2.Refresh;
                                       end;
                                     3:begin
                                         if FForward then begin
                                           FForward := false;
                                           image2.Picture.LoadFromFile(path+'icons\arrow_down.bmp');
                                           image2.Refresh;
                                         end else begin
                                           FForward := true;
                                           image2.Picture.LoadFromFile(path+'icons\arrow_up.bmp');
                                           image2.Refresh;
                                         end;
                                       end;
                                   end;
                                 end;
                                 exit;
                               end;
                             end;
                           end;
                         end;
                       end;
      ciTrainSpeed:begin
                     FAdress := FParser.Adress;
                     FSpeed := Fparser.Speed;
                     if FReceiver is TForm1 then begin
                       with (FReceiver as Tform1)do begin
                         for i := 0 to length(Trains)-1 do begin
                           if Trains[i].Adress = Fadress then begin
                             Trainthread[i].FtrainControlPanel.externalCall := true;
                             //////////////////////////////////////////////////////////////////////
                             if Trainthread[i].FtrainControlPanel.TrackBar1.Max < Fspeed then begin
                               Trainthread[i].FtrainControlPanel.TrackBar1.Max := FSpeed;
                               Trainthread[i].FtrainControlPanel.Gauge1.MaxValue := Fspeed;
                             end;
                             Trainthread[i].FtrainControlPanel.TrackBar1.Position := FSpeed;
                             //////////////////////////////////////////////////////////////////////
                             Trainthread[i].FtrainControlPanel.externalCall := false;
                             exit;
                           end;
                         end;
                       end;
                     end;
                   end;
      ciShiftAccess:begin
                      FAdress := Fparser.Adress;
                      if FListenString = 'csTurnTable' then begin
                        if FReceiver is TForm4 then begin
                          with (FReceiver as Tform4) do begin
                            startExtCall;
                            if (buff[2] = char(99)) and (buff[3] = char(24)) and (buff[10] = char(1)) then begin
                              case Fadress of
                                224:begin
                                      if buff[9] = char(0) then begin
                                        statusbar1.Panels[0].Text := 'Kommando: "end"';
                                      end else if buff[9] = char(1) then begin
                                        statusbar1.Panels[0].Text := 'Kommando: "input"';
                                      end;
                                    end;
                                225:begin
                                      if buff[9] = char(0) then begin
                                        statusbar1.Panels[0].Text := 'Kommando: "clear"';
                                      end else if buff[9] = char(1) then begin
                                        button5.Click;
                                        statusbar1.Panels[0].Text := 'Kommando: "turn"';
                                      end;
                                      exit;
                                    end;
                                226:begin
                                      if buff[9] = char(0) then begin
                                        button4.Click;
                                        statusbar1.Panels[0].Text := 'Kommando: "step >"';
                                        pause(2000);
                                      end else if buff[9] = char(1) then begin
                                        button3.Click;
                                        statusbar1.Panels[0].Text := 'Kommando: "step <"';
                                        pause(2000);
                                      end;
                                    end;
                                228:begin
                                      if buff[9] = char(0) then spinedit1.Value := 1
                                      else if buff[9] = char(1) then spinedit1.Value := 2;
                                      button9.Click;
                                    end;
                                229:begin
                                      if buff[9] = char(0) then spinedit1.Value := 3
                                      else if buff[9] = char(1) then spinedit1.Value := 4;
                                      button9.Click;
                                    end;
                                230:begin
                                      if buff[9] = char(0) then spinedit1.Value := 5
                                      else if buff[9] = char(1) then spinedit1.Value := 6;
                                      button9.Click;
                                    end;
                                231:begin
                                      if buff[9] = char(0) then spinedit1.Value := 7
                                      else if buff[9] = char(1) then spinedit1.Value := 8;
                                      button9.Click;
                                    end;
                                232:begin
                                      if buff[9] = char(0) then spinedit1.Value := 9
                                      else if buff[9] = char(1) then spinedit1.Value := 10;
                                      button9.Click;
                                    end;
                                233:begin
                                      if buff[9] = char(0) then spinedit1.Value := 11
                                      else if buff[9] = char(1) then spinedit1.Value := 12;
                                      button9.Click;
                                    end;
                                234:begin
                                      if buff[9] = char(0) then spinedit1.Value := 13
                                      else if buff[9] = char(1) then spinedit1.Value := 14;
                                      button9.Click;
                                    end;
                                235:begin
                                      if buff[9] = char(0) then spinedit1.Value := 15
                                      else if buff[9] = char(1) then spinedit1.Value := 16;
                                      button9.Click;
                                    end;
                                236:begin
                                      if buff[9] = char(0) then spinedit1.Value := 17
                                      else if buff[9] = char(1) then spinedit1.Value := 18;
                                      button9.Click;
                                    end;
                                237:begin
                                      if buff[9] = char(0) then spinedit1.Value := 19
                                      else if buff[9] = char(1) then spinedit1.Value := 20;
                                      button9.Click;
                                    end;
                                238:begin
                                      if buff[9] = char(0) then spinedit1.Value := 21
                                      else if buff[9] = char(1) then spinedit1.Value := 22;
                                      button9.Click;
                                    end;
                                239:begin
                                      if buff[9] = char(0) then spinedit1.Value := 23
                                      else if buff[9] = char(1) then spinedit1.Value := 24;
                                      button9.Click;
                                    end;
                              end;
                            end;
                            StopExtCall;
                          end;
                        end;
                      end else begin
                        if FReceiver is TForm3 then begin
                          with (Freceiver as Tform3) do begin
                            for i := 0 to getObjectcount-1 do begin
                              if GetObjectAdr(i) = Fadress then begin
                                s := GetObjectState(i);
                                StartExtCallOn(i);
                                case s of
                                  0:ChangeStateOn(i,1);
                                  1:if GetNextAdrOn(i) >= 1 then ChangeStateOn(i,2) else  ChangeStateOn(i,0);
                                  2:if GetNextAdrOn(i) >= 2 then ChangeStateOn(i,3) else ChangeStateOn(i,0);
                                  3:if GetNextAdrOn(i) > 0 then ChangeStateOn(i,0);
                                end;
                                StopExtCallOn(i);
                                exit;
                              end;
                            end;
                          end;
                        end;
                      end;
                    end;
    end;
  end;

end;

procedure Tcontrollistener.stopListening;
begin
  Fstopped :=true;
end;

procedure Tcontrollistener.startListening;
begin
  Fstopped := false;
end;


end.
