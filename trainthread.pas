unit trainthread;

interface

uses
  Classes, typehelper, traincontroller, trainControlwindow, dialogs, sysutils,
  extctrls, math;

type
  TTrainThread = class(TThread)
    FTrainController : TTrainController;
    FtrainControlPanel : TTrainControlPanel;
    FTrain : TTrain;
    FTimer : TTimer;
    FactSpeed,FmaxSpeed : integer;
  private
    procedure OnTrainSpeedChange(Sender : Tobject; speed,adr : integer);
    procedure OnTrainDirectionChange(Sender : Tobject; d,adr : integer);
    procedure OnTimer(Sender : TObject);
  protected
    procedure Execute; override;
  public
    property actSpeed : integer read FactSpeed;
    property maxSpeed : integer read FmaxSpeed;
    constructor create(train : TTrain; n : integer);
    procedure Show;
    procedure ClosePanel;
    procedure ReducePanelElements(lvl : integer);
  end;

implementation

uses main;

{ Important: Methods and properties of objects in visual components can only be
  used in a method called using Synchronize, for example,

      Synchronize(UpdateCaption);

  and UpdateCaption could look like,

    procedure TTrainThread.UpdateCaption;
    begin
      Form1.Caption := 'Updated in a thread';
    end; }

{ TTrainThread }

procedure TTrainThread.Execute;
begin
  { Place thread code here }
end;

constructor TTrainThread.create(train : TTrain; n : integer);
begin
  inherited create(false);
  Ftimer := TTimer.Create(nil);
  Ftimer.Interval := 400;
  Ftimer.Enabled := false;
  Ftimer.OnTimer := OnTimer;
  FactSpeed := 0;
  FmaxSpeed := 0;
  Ftrain := train;
  FTrainController := TTraincontroller.create(train.Adress,train.decoder);
  FTrainControlPanel := TTRainControlPanel.create(form1,train);
  FTrainControlPanel.OnSpeedChange := OnTrainSpeedChange;
  FTrainControlPanel.OnDirectionChange := OnTrainDirectionChange;
  FTrainControlPanel.Caption := train.Name;
  FTrainControlPanel.Left := 5+n*(FTrainControlPanel.width div 3);
  FTrainControlpanel.nr := n;
end;

procedure TTrainThread.OnTrainSpeedChange(Sender : Tobject; speed,adr : integer);
begin
//  FTrainController := TTraincontroller.create(Adr,Ftrain.decoder);
//  FTrainControlPanel.Response := FTrainController.SetSpeed(speed);
  FmaxSpeed := speed;
  FTimer.Enabled := true;
end;

procedure TTrainThread.OnTrainDirectionChange(Sender : Tobject; d,adr : integer);
begin
  if d = 0 then begin
    FTrainControlPanel.response := FTrainController.ForceBackward;
  end else if d = 1 then begin
    FTrainControlPanel.Response := FTrainController.ForceForward;
  end;
end;

procedure TTrainThread.Show;
begin
  FTrainControlPanel.Show;
end;

procedure TTrainthread.ClosePanel;
begin
  FTrainControlPanel.Close;
end;

procedure TTrainThread.ReducePanelElements(lvl : integer);
begin
  case lvl of
    0:begin
        FTrainControlPanel.Groupbox3.Visible := true;
        FtrainControlPanel.Groupbox1.Visible := true;
        FtrainControlPanel.Width := FTrainControlPanel.Width+FTrainControlPanel.Groupbox3.Width;
        FtrainControlPanel.Height := FTrainControlPanel.Height+FTrainControlPanel.Groupbox1.Height;
      end;
    1:begin
        FTrainControlPanel.Groupbox3.Visible := false;
        FtrainControlPanel.Width := FTrainControlPanel.Width-FTrainControlPanel.Groupbox3.Width;
      end;
    2:begin
        FTrainControlPanel.Groupbox3.Visible := false;
        FtrainControlPanel.Groupbox1.Visible := false;
        FtrainControlPanel.Width := FTrainControlPanel.Width-FTrainControlPanel.Groupbox3.Width;
        FtrainControlPanel.Height := FTrainControlPanel.Height-FTrainControlPanel.Groupbox1.Height;
      end;
  end;
end;

procedure TTrainThread.OnTimer(Sender : TObject);
begin
  if FmaxSpeed > FactSpeed then begin
    if (FmaxSpeed-Factspeed) >= 50 then inc(Factspeed,floor((FmaxSpeed-Factspeed)*0.15))
    else begin
      Factspeed := FmaxSpeed;
      Ftimer.Enabled := false;
      MainUDPListener.resumeListener(ciTrainSpeed);
    end;
    FTrainControlPanel.Response := FTrainController.SetSpeed(Factspeed);
    FTrainControlPanel.ControlSpeed(Factspeed);
  end else if FmaxSpeed < FactSpeed then begin
    if (FactSpeed-FmaxSpeed) >= 50 then dec(Factspeed,floor((FactSpeed-FmaxSpeed)*0.15))
    else begin
      Factspeed := FmaxSpeed;
      Ftimer.Enabled := false;
    end;
    FTrainControlPanel.Response := FTrainController.SetSpeed(Factspeed);
    FTrainControlPanel.ControlSpeed(Factspeed);
  end; 

end;

end.
