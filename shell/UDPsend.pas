unit UDPsend;

interface

uses Sysutils, IdBaseComponent, IdComponent, IdUDPBase, IdUDPClient, typehelper,
     package;

type TUDPSender = class
    FUDPClient : TidUDPClient;
    FHost : string;
    FPort : integer;
    FBuffer,FResponse : TBuff;
    FPackageStack : array of Tpackage;
    FisSenderFree : boolean;
//    FListenForResponse : boolean;
  private
    procedure SendFromStack;
  public
    property Host : String read FHost;
    property Port : integer read FPort;

    constructor create(idClient : TidUDPClient; host : string; port : integer);

    procedure changeHost(ip : string);

    procedure ToStack(package : Tpackage);
    procedure Send(package : Tpackage); overload;
    procedure Send(paketStr : string); overload;
    procedure Send(rule : Trule); overload;
    procedure Send(buff : TBuff); overload;
end;

implementation

constructor TUDPSender.create(idClient : TidUDPClient; host : string; port : integer);
begin
  inherited create;
  FHost := host;
  FPort := port;
  FUDPCLient := idClient;
  FUDPClient.Port := port;
  FUDPClient.Host := host;
  FUDPClient.Active := true;
  FisSenderFree := true;
end;

procedure TUDPSender.ToStack(package : Tpackage);
begin
  setlength(FpackageStack,length(FpackageStack)+1);
  FpackageStack[length(FpackageStack)-1] := package;
//  FpackageStack[length(FpackageStack)-1].Buildpackage(package.InBuffer);
  SendFromStack;
end;

procedure TUDPSender.Send(package : Tpackage);
begin
  FisSenderFree := false;
  Fbuffer := package.buffer;
  FUDPClient.SendBuffer(FBuffer,13);
  if ord(Fresponse[1]) = ord(FBuffer[1])+1 then begin
    FisSenderFree := true;
    SendFromStack;
  end;
end;

procedure TUDPSender.Send(buff : TBuff);
begin
  FUDPClient.SendBuffer(buff,13);
  if ord(Fresponse[1]) = ord(FBuffer[1])+1 then begin
    FisSenderFree := true;
    SendFromStack;
  end;
end;

procedure TUDPSender.Send(paketStr : string);
var tempStr : TStrArray;
    i,n : integer;
begin
  n := explode(tempStr,' ',paketStr);
  if n < 13 then begin
    setlength(tempstr,13);
    for i := n to 12 do begin
      tempstr[i] := '0';
    end;
  end;
  for i := 0 to 12 do begin
    if tempstr[i][1] in ['0'..'9'] then FBuffer[i] := char(strtoint(tempStr[i]))
    else if tempstr[i][1] in ['a'..'z'] then Fbuffer[i] := tempstr[i][1];
  end;
  FUDPClient.SendBuffer(FBuffer,13);
end;

procedure TUDPSender.Send(rule : Trule);
var tempStr : TStrArray;
    i : integer;
begin
  explode(tempStr,' ',rule.Buffer);
  for i := 0 to 12 do begin
    FBuffer[i] := char(strtoint(tempStr[i]));
  end;
  FUDPClient.SendBuffer(FBuffer,13);
end;

procedure TUDPSender.changeHost(ip : string);
begin
  FUDPClient.Active := false;
  FUDPClient.Host := ip;
  FUDPClient.Active := true;
end;

procedure TUDPSender.SendFromStack;
var i : integer;
begin
  if length(FpackageStack)>0 then begin
    if FisSenderFree then begin
      send(FpackageStack[0]);
      for i := 0 to length(FpackageStack)-2 do begin
        Fpackagestack[i] := Fpackagestack[i+1];
      end;
      setlength(FpackageStack,length(FpackageStack)-1);
    end;
  end;
end;

end.
