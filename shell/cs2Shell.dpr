program cs2Shell;

{$APPTYPE CONSOLE}

uses
  SysUtils,types,typehelper,package,udpsend,IdBaseComponent, IdComponent,
  IdUDPBase, IdUDPClient,idGlobal, windows, forms, IdUDPServer, IdSocketHandle,
  classes, shellapi;

var
  i,CmdCounter,n,p,j : integer;
  CmdArr : array of string;
  FBuffer,ListenFor,received : TBuff;
  BufferStr,InputLine,temp,hlpPath : string;
  finished,receiver,inBuff,listener,
  listenall,startlistening : boolean;
  udpClient : TIdUDPClient;
  udpServer : TIdUDPServer;
  Fpackage : Tpackage;
  buf : TStrArray;

  FFile : Textfile;
  tempstr : string;

  function GetParam(param : string):string;
  begin
    i := 0;
    while i <= paramcount do begin
      if paramstr(i) = param then begin
        result := paramstr(i+1);
        exit;
      end;
      inc(i);
    end;
    result := '';
  end;

  function GetCmd(cmd : string):string;
  begin
    result := '';
    for i := 0 to length(CmdArr)-1 do begin
      if CmdArr[i] = cmd then begin
        result := CmdArr[i+1];
        exit;
      end;
    end;
  end;

  function GetCmdIndex(param : string):integer;
  begin
    result := -1;
    for i := 0 to length(CmdArr)-1 do begin
      if CmdArr[i] = param then begin
        result := i;
        exit;
      end;
    end;
  end;

  function GetParamIndex(param : string):integer;
  begin
    result := -1;
    for i := 0 to paramcount do begin
      if paramstr(i) = param then begin
        result := i;
        exit;
      end;
    end;
  end;

  Function QuotedStr(_sString: String): String;
   Begin
      Result := #034 + _sString + #034;
   End;

begin
  finished := false;
  receiver := false;
  inBuff := false;
  listener := false;
  listenall := false;
  startlistening := false;
  hlpPath := extractfilepath(application.ExeName)+'doc\hlp\';

  udpClient := TidUDPClient.Create(nil);
  udpClient.Port := 15731;
  udpClient.Host := '0.0.0.0';
  udpClient.Active := true;

  udpServer := TidUDPServer.Create(nil);
  udpServer.DefaultPort := 15730;
  udpServer.Active := false;

  Fpackage := Tpackage.create(pmcreate);
  CmdCounter := 0;
  BufferStr := '';
  n := 0;
  p := 1;
///////////////////////////////////////////////////////////////////////////
////////////////Parsing Start-Parameters///////////////////////////////////
///////////////////////////////////////////////////////////////////////////
  if paramcount > 0 then begin
    if getParam('-ip') <> '' then udpClient.Host := getparam('-ip');
    if getparam('-l') <> '' then startlistening := true;
    if getparam('-d') <> '' then begin
      for i := getparamIndex('-d')+1 to paramcount do begin
        FBuffer[i-getparamIndex('-d')+1] := char(strtoint(paramstr(i)));
      end;
      udpclient.SendBuffer(Fbuffer,13);
      exit;
    end;
    if getparam('-c') <> '' then begin
      for i := getparamIndex('-c')+1 to paramcount do begin
        Bufferstr := bufferstr + paramstr(i)+' ';
      end;
      if Fpackage.ParseUserCmd(bufferstr) then begin
        Fbuffer := Fpackage.Buffer;
        udpClient.SendBuffer(Fbuffer,13);
      end;
      exit;
    end;
  end;
///////////////////////////////////////////////////////////////////////////////
////////////////////////////STARTING AS SERVER LISTENER////////////////////////
///////////////////////////////////////////////////////////////////////////////
  if startlistening then begin
    listener := true;
    udpServer.Active := true;
    writeln('Filter Listening??>>>');
    readln(temp);
    if temp <> '' then begin
      p := 1;
      i := 0;
      while p <> 0 do begin
        p := pos(' ',temp);
        ListenFor[i] := char(strtoint(copy(temp,0,p-1)));
        temp := copy(temp,p+1,length(temp));
        inc(i);
      end;
      listenfor[i] := char(strtoint(temp));
    end;
  end;
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
  temp := '';
  if Bufferstr <> '' then begin
  end else begin
    if udpClient.Host = '0.0.0.0' then begin
      writeln('cs2Shell started without Params;');
      writeln('You need to setip fisrt!');
    end;
    write('cs2Shell>>>');
    repeat ////////////////////////////////////////REPEAT_START////////////////
      if listener then begin
        readln;
      end else begin
        ReadLn(temp);
//      temp := inputLine;
        setlength(CmdArr,0);
        p := 1;

        while p <> 0 do begin
          p := pos(' ',temp);
          if p <> 0 then begin
            inc(CmdCounter);
            setlength(Cmdarr,length(Cmdarr)+1);
            CmdArr[length(CmdArr)-1] := copy(temp,0,p-1);
            temp := copy(temp,p+1,length(temp));
          end;
        end;
        setlength(CmdArr,length(Cmdarr)+1);
        CmdArr[length(Cmdarr)-1] := temp;


        if CmdCounter > 0 then begin
          if CmdArr[0] = 'setip' then begin
            udpclient.Host := CmdArr[1];
            writeln('Changed IP to: '+Cmdarr[1]);
          end;
          if CmdArr[0] = 'send' then begin
            if CmdArr[1] = '-b' then begin
              for i := 2 to length(CmdArr)-1 do begin
                Fbuffer[i-2] := char(strtoint(CmdArr[i]));
              end;
              udpClient.SendBuffer(FBUffer,13);
            end;
            if CmdArr[1] = '-c' then begin
              bufferstr := '';
              for i := 2 to length(CmdArr)-1 do begin
                Bufferstr := bufferstr + Cmdarr[i]+' ';
              end;
              if Fpackage.ParseUserCmd(bufferstr) then begin
                Fbuffer := Fpackage.Buffer;
                udpClient.SendBuffer(Fbuffer,13);
              end;
            end;
          end;
          if CmdArr[0] = 'listen' then begin
            listener := true;
            if getCmdIndex('-a') <> -1 then listenall := true;
            if getCmdIndex('-s') <> -1 then begin
              for i := getCmdindex('-s')+1 to length(CmdArr)-1 do begin
                ListenFor[i-getCmdindex('-s')+1] := char(strtoint(CmdArr[i]));
              end;
            end;
            if getcmd('-p') <> '' then udpServer.DefaultPort := strtoint(getcmd('-p'));
            udpServer.Active := true;
          end;

          if CmdArr[0] = 'start' then begin
            Bufferstr := '';
            for i := 1 to length(CmdArr)-1 do begin
              Bufferstr := bufferstr + ' '+ CmdArr[i];
            end;
            Bufferstr := copy(Bufferstr,1,length(Bufferstr));
            if length(CmdArr)>1 then n := shellexecute(0,'open',Pchar('Project1.exe'),Pchar(bufferstr),nil,SW_SHOWNORMAL)
            else n := shellexecute(0,'open',Pchar('Project1.exe'),nil,nil,SW_SHOW);
            writeln('Started GUI with Params: ');
            writeln(bufferstr);
            writeln(inttostr(n));
          end;

          if CmdArr[0] = 'show' then begin
            if CmdArr[1] = 'ip' then writeln(udpClient.host);
          end;
          if CmdArr[0] = 'exit' then exit;

          if CmdArr[0] = 'help' then begin
            if CmdArr[1] <> '-o' then begin
              AssignFile(FFile,hlpPath+CmdArr[1]+'.txt');
              reset(FFile);
              while not eof(FFile) do begin
                readln(FFile,tempstr);
                writeln(tempstr);
              end;
            end else begin
              shellexecute(0,'open',PChar(hlpPath+CmdArr[2]+'.txt'),nil,nil,SW_SHOW);
            end;
          end;

          if CmdArr[1] = '-h' then begin
            shellexecute(0,'open',PChar(hlpPath+'Console_hlp.txt'),nil,nil,SW_SHOW);
          end;
        end else begin
          if temp = 'exit' then exit;
        end;

        write('cs2shell>>>');
      end;
    until finished;
  end;

  readln;
end.
