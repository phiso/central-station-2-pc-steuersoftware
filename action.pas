unit action;

interface

uses Sysutils, typehelper;

type TActionTarget = (atTrain,atArticle);
type TAction = class
  private
    FTargetAdr : integer;
    Ftarget : TActionTarget;
  public
    property Adr : integer read FtargetAdr;
    property Target : TActionTarget read Ftarget;

    constructor create(adr : integer; target : TActiontarget);
end;

implementation

constructor Taction.create(adr : integer; target : TActiontarget);
begin
  inherited create;
  FtargetAdr := adr;
  Ftarget := target;
end;

end.
