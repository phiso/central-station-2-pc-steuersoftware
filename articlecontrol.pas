unit articlecontrol;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, typehelper, jpg, control, inifiles;

type
  TArticlePanel = class(Tform)
    Panel1: TPanel;
    Image1: TImage;
    procedure Image1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Image1Click(Sender : TObject);
    procedure FormCreate(Sender: TObject);
  private
    FArticle : Tarticle;
    FNextAdr,Fstate : integer;
    FpicString : string;
    FpicPath : Tstringlist;
    Fpics : array of TBitmap;
    Fposition : Tpoint;
    control : TControl;
    externalCall : boolean;

    procedure LoadPics;
    procedure getCounts;
  public
    property Article : Tarticle read Farticle;
    property PosX : integer read Fposition.x;
    property PosY : integer read Fposition.y;
    property Position : Tpoint read Fposition;
    property State : integer read FState;
    property NextAdr : integer read FNextAdr;
    property Extcall : boolean read externalCall;

    constructor create(Owner : TComponent; article : Tarticle; position:Tpoint);

    function changestate(s : integer):boolean;
    procedure StartExternalCall;
    procedure StopExternalCall;
  end;

var
  ArticlePanel : TArticlePanel;

implementation

uses main, digitTurntable;

{$R *.dfm}

constructor TArticlepanel.create(Owner : TComponent; article : Tarticle; position:Tpoint);
var ini : Tinifile;
    p : integer;
begin
  inherited create(Owner);
  self.Parent := (Owner as TWinControl);
  (self.Parent.Parent as Tform).Height := 5*95;
  self.BorderStyle := bsToolWindow;
  self.Width := 56;
  self.Height := 80;
  image1.OnClick := Image1Click;
  FArticle := article;
  Fstate := article.State;
  getCounts;
  control := Tcontrol.create(article.Adr,article.dectype);
  Fposition := position;

  ini := Tinifile.Create(form1.Default_SystemPath+'icons\PicMapping.ini');
  Fpicstring := ini.ReadString(FArticle.Typ,'prefix','err');
  p := pos('|',FPicstring);
  FPicpath := Tstringlist.Create;
  while p <> 0 do begin
    Fpicpath.Add(form1.Default_SystemPath+'icons\articles\'+copy(Fpicstring,0,p-1)+'.bmp');
    FpicString := copy(Fpicstring,p+1,length(Fpicstring));
    p := pos('|',FPicstring);
    if p = 0 then
      if (Fpicstring <> '') and (Fpicstring <> ' ') then
        FPicpath.Add(form1.Default_SystemPath+'icons\articles\'+Fpicstring+'.bmp');
  end;
  loadpics;

  self.Caption := 'Adr:'+inttostr(Farticle.adr)+'-'+Farticle.name;
  self.Hint := self.Caption;
  image1.Hint := self.Caption;
  image1.ShowHint := true;
  self.ShowHint := true;
  self.Top := 1+(position.Y*self.Height);
  self.Left := 1+(position.X*self.Width);
  self.Show;
  ini.Free;
end;

procedure TArticlePanel.getCounts;
begin
  if (Farticle.Typ = 'dreiwegweiche') or (Farticle.Typ = 'dkw_2antriebe') or
     (Farticle.Typ = 'lichtsignal_HP012_SH01') or (Farticle.Typ = 'formsignal_HP012') or
     (Farticle.Typ = 'lichtsignal_HP012') or (Farticle.Typ = 'urc_lichtsignal_HP012_SH01') or
     (Farticle.Typ = 'urc_lichtsignal_HP01') then FnextAdr := 1
  else if (Farticle.Typ = 'formsignal_HP012_SH01') then FnextAdr := 2
  else FNextAdr := 0;
end;

procedure TArticlepanel.LoadPics;
var i : integer;
begin
  setlength(Fpics,Fpicpath.Count);
  if Fpicpath.count = 0 then showmessage(Farticle.Typ+'  '+Farticle.name);
  for i := 0 to FpicPath.Count-1 do begin
    Fpics[i] := Tbitmap.Create;
    Fpics[i].LoadFromFile(FPicPath[i]);
  end;
  image1.Picture.Bitmap.Assign(Fpics[0]);
end;

procedure TArticlePanel.Image1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  panel1.BevelInner := bvlowered;
end;

procedure TArticlePanel.Image1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  panel1.BevelInner := bvRaised;
end;

procedure TArticlePanel.FormCreate(Sender: TObject);
begin
  panel1.BevelInner := bvraised;
end;

function TArticlePanel.changestate(s : integer):boolean;
begin
  if not externalCall then begin
    case s of
      0:result := control.ShiftSwitch(0);
      1:result := control.ShiftSwitch(1);
      2:result := control.ShiftSwitch(16);
      3:result := control.ShiftSwitch(17);
    end;
  end;
  if result then begin
    Fstate := s;
    image1.Picture.Bitmap.Assign(Fpics[s]);
  end;
end;

procedure TArticlePanel.Image1Click(Sender : TObject);
begin
  if FArticle.Typ = 'digitaldrehscheibe' then begin
    form4.Show;
  end else begin
    case Fstate of
      0:changestate(1);
      1:if FNextAdr >= 1 then changestate(2) else  changestate(0);
      2:if FNextAdr >= 2 then changestate(3) else changestate(0);
      3:if FNextAdr> 0 then changestate(0);
    end;
  end;
end;

procedure TArticlePanel.StartExternalCall;
begin
  externalCall := true;
end;

procedure TArticlePanel.StopExternalCall;
begin
  externalCall := false;
end;

end. 
