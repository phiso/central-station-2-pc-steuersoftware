object Form12: TForm12
  Left = 650
  Top = 352
  Width = 331
  Height = 310
  AutoSize = True
  Caption = 'Neues System'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 136
    Width = 79
    Height = 13
    Caption = 'Gleisbild Dateien'
  end
  object Button1: TButton
    Left = 288
    Top = 64
    Width = 25
    Height = 20
    Caption = '...'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 288
    Top = 104
    Width = 25
    Height = 20
    Caption = '...'
    TabOrder = 1
    OnClick = Button2Click
  end
  object LabeledEdit1: TLabeledEdit
    Left = 0
    Top = 64
    Width = 289
    Height = 21
    EditLabel.Width = 51
    EditLabel.Height = 13
    EditLabel.Caption = 'Loks Datei'
    TabOrder = 2
  end
  object LabeledEdit2: TLabeledEdit
    Left = 0
    Top = 104
    Width = 289
    Height = 21
    EditLabel.Width = 92
    EditLabel.Height = 13
    EditLabel.Caption = 'Magnetartikel Datei'
    TabOrder = 3
  end
  object Button3: TButton
    Left = 288
    Top = 152
    Width = 25
    Height = 20
    Caption = '...'
    TabOrder = 4
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 208
    Top = 240
    Width = 107
    Height = 32
    Caption = 'Erstellen'
    TabOrder = 5
    OnClick = Button4Click
  end
  object LabeledEdit3: TLabeledEdit
    Left = 0
    Top = 16
    Width = 169
    Height = 21
    EditLabel.Width = 65
    EditLabel.Height = 13
    EditLabel.Caption = 'System Name'
    TabOrder = 6
  end
  object ListBox1: TListBox
    Left = 0
    Top = 152
    Width = 289
    Height = 81
    ItemHeight = 13
    TabOrder = 7
  end
  object OpenDialog1: TOpenDialog
    Filter = 'cs2-Datei (.cs2)|*.cs2|ini-datei (.ini)|*.ini'
    Left = 232
    Top = 32
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '.ini'
    Filter = 'ini-Dateien (.ini)|*.ini'
    Left = 264
    Top = 32
  end
end
