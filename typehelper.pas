unit typehelper;

interface

uses Sysutils, math, dialogs, types, extctrls, windows, forms, classes;


type TWMHotKey = record
  Msg : Cardinal;
  idHotKey : word;
  Mofifiers : integer;
  VirtKey : integer;
end;

type TDecoder = (dcMM2Shift,dcMM2Prog,dcDCC,dcMFX);
type TBuffer = record
  cmdbyte : Byte;
  Adress : integer;
  decType : TDecoder;
  case DLCbyte : Byte of
    5: (SingleVal : Byte);
    6: (B2Val : array[0..1] of Byte);
    7: (B3Val : array[0..2] of Byte);
    8: (B4Val : array[0..3] of Byte);
end;

type TTrain = record
  Name,pic : string;
  Adress : integer;
  decoder : TDecoder;
  anf,brems,vmin,vmax,
  loud,tacho : integer;
end;
type TTrainArray = array of TTrain;

type TIntArray = array of integer;
type TRule = record
       Name, Buffer : string;
       IsDynamic : Boolean;
     end;
type TRuleArray = array of TRule;
type TStrArray = array of string;
type Tbytes = array of Byte;
type TCharArray = array of Char;
type TBuff = array[0..12] of char;
type TBuffArray = array of TBuff;
type TSwitchDirection = (sdLeft,sdRight,sdStraight);
type Tcs2FileType = (cfpLoks,cfpArticles,cfpLayout);
type Tcs2FileStream = (cfsOpenRead,cfsCreateWrite);
type TFunction = record
        nr,typ : string;
end;
type TFunctions = array of TFunction;

type TCVValue = record
        nr,name,val : string;
end;
type TCVValues = array of TCVValue;

type TArticle = record
        Adr,State,shiftTime: integer;
        dectype : TDecoder;
        Typ,name : string;
end;
type TArticleArray = array of TArticle;

type TCmdtype = (ctSystem,
                 ctControl,
                 ctResponse,
                 ctAppend,
                 ctSoftware,
                 ctGUI,
                 ctAutomated);

type TPrioType = (ptStopGO,
                  ptResponse,
                  ptStopTrain,
                  ptControl);

type TCmdInst = (ciSystem,
                 ciTrainDiscover,
                 ciMFXBind,
                 ciMFXverify,
                 ciTrainSpeed,
                 ciTrainDirection,
                 ciTrainFunction,
                 ciReadConf,
                 ciWriteConf,
                 ciShiftAccess,
                 ciConfAccess,
                 ciS88Poll,
                 ciS88Event,
                 ciSX1Event,
                 ciPing,
                 ciOfferUpdate,
                 ciReadConfData,
                 ciCANBind,
                 ciRailsBind,
                 ciStateConf,
                 ciReqConfData,
                 ciConfDataStream,
                 ciOldDataStream);


function Explode(var a: TStrArray; Border, S: string): Integer;
function Implode(const cSeparator: String; const cArray: TStrArray): String;
function BuffToStr(Buff : Tbuff):string;
function CmdToStr(cmd : TCmdInst):string;
function PrioToStr(prio : TprioType):string;
function CmdTypeToStr(ct : TCmdType):string;
function ParseAdress(buf0,buf1 : char):integer;
function CmdByte(cmd : TCmdInst):byte;
function ResponseByte(cmd : TCmdInst):byte;
function HexToInt(hex : String):integer;
function CalcSpeedBytes(speed : integer):TBytes;
function BytesToSpeed(int1,int2 : integer):integer;
function ShiftSpeedBytes(speed : integer):TCharArray;
function BytesToInt(Bytes : Tbytes):integer;
function PointToStr(p : Tpoint):string;
function ObjektAssigned(ppeObjRef: TObject): Boolean;
function decoderToStr(dec : Tdecoder):string;
function StrToDec(str : string):Tdecoder;
procedure pause(length : integer);
function get2BitSwitchAdr(buf1,buf2 : char):integer;
//function TurnTableAdrToPos(adr : integer):integer;
procedure PostKeyEx32(key: Word; const shift: TShiftState; specialkey: Boolean);
function IsInt(int : String):boolean;

implementation

function Explode(var a: TStrArray; Border, S: string): Integer;
var
  S2: string;
begin
  Result  := 0;
  S2 := S + Border;
  repeat
    SetLength(A, Length(A) + 1);
    a[Result] := Copy(S2, 0,Pos(Border, S2) - 1);
    Delete(S2, 1,Length(a[Result] + Border));
    Inc(Result);
  until S2 = '';
end;

function Implode(const cSeparator: String; const cArray: TStrArray): String;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Length(cArray) -1 do begin
    Result := Result + cSeparator + cArray[i];
  end;
  System.Delete(Result, 1, Length(cSeparator));
end;

function BuffToStr(Buff : Tbuff):string;
var i : integer;
begin
  result := '';
  for i := 0 to 12 do begin
    result := result + inttostr(ord(Buff[i]))+' ';
  end;
  result := copy(result,0,length(result)-1);
end;

function CmdToStr(cmd : TCmdInst):string;
begin
  case cmd of
    ciSystem: result := 'System';
    ciTrainDiscover: result := 'Train Discovery';
    ciMFXBind: result := 'MFX Bind';
    ciMFXverify: result := 'MFX Verify';
    ciTrainSpeed: result := 'Train Speed';
    ciTrainDirection: result := 'Train Direction';
    ciTrainFunction: result := 'Train Function';
    ciReadConf: result := 'Read Config';
    ciWriteConf:result := 'Write Config';
    ciShiftAccess:result := 'Shift Accessoire';
    ciConfAccess:  result := 'Config Accessoire';
    ciS88Poll: result := 'S88 Polling';
    ciS88Event: result := 'S88 Event';
    ciSX1Event: result := 'SX1 Event';
    ciPing:   result := 'Ping';
    ciOfferUpdate:result := 'Offer Update';
    ciReadConfData:result := 'Read Config Data';
    ciCANBind:  result := 'Bootloader CAN Bound';
    ciRailsBind:result := 'Bootloader Rails Bound';
    ciStateConf: result := 'Statedata Config';
    ciReqConfData: result := 'Request Config Data Stream';
    ciConfDataStream:result := 'Config Data Stream';
    ciOldDataStream:result := 'Data Stream 6021 Adapter';
  end;
end;

function PrioToStr(prio : TprioType):string;
begin
  case prio of
    ptStopGO: result := 'Error/Stop/GO';
    ptResponse:result := 'Response';
    ptStopTrain:result := 'Stop Train?';
    ptControl:result := 'Train/Accessoire Orders';
  end;
end;

function CmdTypeToStr(ct : TCmdType):string;
begin
  case ct of
    ctSystem: result := 'System';
    ctControl: result := 'Management';
    ctResponse:result := 'Response';
    ctAppend: result := 'Accessoires';
    ctSoftware:result := 'Software Update/Config';
    ctGUI:result := 'GUI Information';
    ctAutomated: result := 'Automated Orders';
  end;
end;

function ParseAdress(buf0,buf1 : char):integer;
{Wird nur verwendet um DCC Adressen zu parsen, da sonstige Adresse nur das
letzte byte des Adressberecihs belegen uns somit keine Rechnung erfolgen muss.
Nur die letzten 2 byte des Adressbereichs d�rfen �bergeben werden}
var temp : integer;
begin
  if (ord(buf0) >= 64) and (ord(buf0) < 192)then begin
    temp := ord(buf0)-64;
    temp := (temp*253)+ord(buf1);
    result := temp-2;
  end else if ord(buf0)>=192 then begin
    temp := ord(buf0)-192;
    temp := (temp*255)+ord(buf1);
    result := temp;
  end else result := ord(buf1);
end;

function CmdByte(cmd : TCmdInst):byte;
begin
  case cmd of
    ciSystem: result := 0;
    ciTrainDiscover:result := 2;
    ciMFXBind: result := 3;
    ciMFXverify:result := 6;
    ciTrainSpeed:result := 8;
    ciTrainDirection:result := 10;
    ciTrainFunction:result := 12;
    ciReadConf:result := 14;
    ciWriteConf:result := 16;
    ciShiftAccess:result := 22;
    ciConfAccess:result := 24;
    ciS88Poll: result := 32;
    ciS88Event:result := 34;
    ciSX1Event:result := 36;
    ciPing:  result := 48;
    ciOfferUpdate:result := 50;
    ciReadConfData: result := 52;
    ciCANBind: result := 54;
    ciRailsBind: result := 56;
    ciStateConf: result := 58;
    ciReqConfData: result := 64;
    ciConfDataStream: result := 66;
    ciOldDataStream:result := 68;
  end;
end;

function ResponseByte(cmd : TCmdInst):byte;
begin
  result := CmdByte(cmd)+1;
end;

function HexToInt(hex : String):integer;
begin
  Result:=StrToInt('$' + Hex);
end;

function CalcSpeedBytes(speed : integer):TBytes;
var temp,hex1,hex2 : string;
begin
  setlength(result,2);
  result[0] := 0;
  result[1] := 0;
  if speed > 255 then begin
    temp := IntToHex(speed,4);
    hex1 := copy(temp,0,2);
    hex2 := copy(temp,3,4);
    result[0] := HexToInt(hex1);
    result[1] := HexToInt(hex2);
  end else begin
    result[1] := speed;
  end;
end;

function BytesToSpeed(int1,int2 : integer):integer;
var hex1,hex2 : string;
begin
  result := 0;
  hex1 := IntToHex(int1,1);
  hex2 := IntToHex(int2,1);
  result := HexTOInt(hex1+hex2);
end;

function ShiftSpeedBytes(speed : integer):TCharArray;
var s,temp,i : integer;
begin
  s := floor(speed/10);
  setlength(result,2);
  for i := 0 to 3 do result[i] := #0;

  if s <=255 then result[0] := char(s)
  else begin
    temp := floor(s/255);
    result[0] := char(temp);
    result[1] := char(s-(temp*255));
  end;
end;

function BytesToInt(Bytes : Tbytes):integer;
begin
  result := 0;
  if length(Bytes)>2 then Showmessage('Berechnung nur mit max 2 Byte m�glich')
  else result := bytes[0]*255+bytes[1];
end;

function PointToStr(p : Tpoint):string;
begin
  result := 'X:'+inttostr(p.x)+' Y:'+inttostr(p.Y);
end;

function ObjektAssigned(ppeObjRef: TObject): Boolean;
type
  PPVmt = ^PVmt;
  PVmt = ^TVmt;
  TVmt = record
    SelfPtr: TClass;
    Other: array[0..17] of Pointer;
  end;
var
  Vmt: PVmt;
begin
  Result := False;
  if Assigned(ppeObjRef) then
  begin
    try
      Vmt := PVmt(ppeObjRef.ClassType);
      Dec(Vmt);
      Result := ppeObjRef.ClassType = Vmt.SelfPtr;
    except
      // Exception ignorieren, Result ist False
    end;
  end;
end;

function decoderToStr(dec : Tdecoder):string;
begin
  case dec of
    dcMM2Shift:result := 'MM2Shift';
    dcMM2Prog:result := 'MM2Prog';
    dcDCC:result := 'DCC';
    dcMFX:result := 'MFX';
  end;
end;

function StrToDec(str : string):Tdecoder;
begin
  if str='MM2Shift' then result := dcMM2Shift
  else if str='MM2Prog' then result := dcMM2Prog
  else if str='DCC' then result := dcDCC
  else if str='MFX' then result := dcMFX
  else result := dcMM2Shift;
end;

procedure pause(length : integer);
var start,stop : longint;
begin
  start := gettickcount;
  repeat
    stop := gettickcount;
    application.ProcessMessages;
  until (stop-start) >= length;
end;

function get2BitSwitchAdr(buf1,buf2 : char):integer;
var diff : integer;
begin
  if ord(buf1) = 48 then result := ord(buf2)+1
  else if ord(buf1) = 49 then result := 257+ord(buf2)
  else if ord(buf1) >= 56 then begin
    if ord(buf1) = 56 then result := ord(buf2)+1
    else begin
      diff := ord(buf1)-56;
      result := (diff*257)+ord(buf2);
    end;
  end;
end;

procedure PostKeyEx32(key: Word; const shift: TShiftState; specialkey: Boolean);
{************************************************************
* Procedure PostKeyEx32
*
* Parameters:
*  key    : virtual keycode of the key to send. For printable
*           keys this is simply the ANSI code (Ord(character)).
*  shift  : state of the modifier keys. This is a set, so you
*           can set several of these keys (shift, control, alt,
*           mouse buttons) in tandem. The TShiftState type is
*           declared in the Classes Unit.
*  specialkey: normally this should be False. Set it to True to
*           specify a key on the numeric keypad, for example.
* Description:
*  Uses keybd_event to manufacture a series of key events matching
*  the passed parameters. The events go to the control with focus.
*  Note that for characters key is always the upper-case version of
*  the character. Sending without any modifier keys will result in
*  a lower-case character, sending it with [ssShift] will result
*  in an upper-case character!
// Code by P. Below
************************************************************}
type
  TShiftKeyInfo = record
    shift: Byte;
    vkey: Byte;
  end;
  byteset = set of 0..7;
const
  shiftkeys: array [1..3] of TShiftKeyInfo =
    ((shift: Ord(ssCtrl); vkey: VK_CONTROL),
    (shift: Ord(ssShift); vkey: VK_SHIFT),
    (shift: Ord(ssAlt); vkey: VK_MENU));
var
  flag: DWORD;
  bShift: ByteSet absolute shift;
  i: Integer;
begin
  for i := 1 to 3 do
  begin
    if shiftkeys[i].shift in bShift then
      keybd_event(shiftkeys[i].vkey, MapVirtualKey(shiftkeys[i].vkey, 0), 0, 0);
  end; { For }
  if specialkey then
    flag := KEYEVENTF_EXTENDEDKEY
  else
    flag := 0;

  keybd_event(key, MapvirtualKey(key, 0), flag, 0);
  flag := flag or KEYEVENTF_KEYUP;
  keybd_event(key, MapvirtualKey(key, 0), flag, 0);

  for i := 3 downto 1 do
  begin
    if shiftkeys[i].shift in bShift then
      keybd_event(shiftkeys[i].vkey, MapVirtualKey(shiftkeys[i].vkey, 0),
        KEYEVENTF_KEYUP, 0);
  end; { For }
end; { PostKeyEx32 }

function IsInt(int : String):boolean;
var temp : integer;
begin
  try
    temp := strtoint(int);
    result := true;
  except
    result := false;
  end;
end;

end.
