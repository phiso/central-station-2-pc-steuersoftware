unit eventlistener;

interface

uses
  Classes, typehelper, rcvparser;

type
  TEventListener = class(TThread)
  private
    FstartWith : TBuff;
    Factions : TbuffArray;
    Fparser : TBuffParser;
    Factive : boolean;
    FNr : integer;

  protected
    procedure Execute; override;
  public
    property Active : boolean read Factive;

    constructor create(start : TBuff; actions : TBuffArray; suspended : boolean=false);

    procedure listen(buff : Tbuff);
    procedure StopListening;
    procedure StartListening;
  end;

implementation

uses main;

{ Important: Methods and properties of objects in visual components can only be
  used in a method called using Synchronize, for example,

      Synchronize(UpdateCaption);

  and UpdateCaption could look like,

    procedure TEventListener.UpdateCaption;
    begin
      Form1.Caption := 'Updated in a thread';
    end; }

{ TEventListener }

procedure TEventListener.Execute;
begin
  { Place thread code here }
end;

constructor TEventlistener.create(start : TBuff; actions : TBuffArray; suspended : boolean=false);
var i,j : integer;
begin
  inherited create(false);
  Factive := suspended;
  for i := 0 to 12 do FstartWith[i] := start[i];
  setlength(Factions,length(actions));
  for j := 0 to length(actions)-1 do begin
    for i := 0 to 12 do Factions[j][i] := actions[j][i];
  end;
  MainUDPListener.addEvtListener(self);
end;

procedure TEventlistener.listen(buff : Tbuff);
var i : integer;
    temp : boolean;
begin
  if not Factive then exit;
  temp := true;
  for i := 0 to 12 do begin
    if FStartWith[i] <> buff[i] then temp := false;
  end;

  if temp then begin
    for i := 0 to length(Factions)-1 do begin
      MainUDPSender.Send(Factions[i]);
      pause(20); ////N�tig?
    end;
  end;
end;

procedure TEventlistener.StopListening;
begin
  Factive := false;
end;

procedure TEventlistener.StartListening;
begin
  Factive := true;
end;

end.
