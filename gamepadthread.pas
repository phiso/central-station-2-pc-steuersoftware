unit gamepadthread;

interface

uses
  Classes,dxJoystick,typehelper;
type TStickEvent = procedure(Sender : Tobject; axis : string; value : integer) of object;
type TButtonEvent = procedure(Sender : Tobject; button : integer; state : boolean) of object;
type TPOVEvent = procedure(Sender : Tobject; pov : integer) of object;
type
  TGamePadThread = class(TThread)
  private
    FStickMove : TStickEvent;
    FbuttonPress : TbuttonEvent;
    FPOVMove : TPOVEvent;
    DxJoyStick :TDxJoyStick;
    FStickPosX, FstickPosY, FStickPosRX,
    FstickPosRY, FstickPosRZ,FstickPosZ,
    FSlider1Pos,FSlider2Pos : integer;
    FStickPosXp, FstickPosYp, FStickPosRXp,
    FstickPosRYp, FstickPosRZp,FstickPosZp,
    FSlider1Posp,FSlider2Posp : integer;
    FPOV,FPOVp : integer;
    FButtons,Fbuttonsp : array[0..31] of Boolean;


    function getButton(index : integer):boolean;
  protected
    procedure StickMove(Sender : Tobject; axis : string; value : integer);
    procedure ButtonPress(Sender : Tobject; button : integer; state : boolean);
    procedure POVMove(Sender : Tobject; pov : integer);
    procedure Execute; override;
  public
    stopped : boolean;
    property XPos : integer read FStickPosX;
    property YPos : integer read FStickPosY;
    property ZPos : integer read FStickPosZ;
    property RXPos : integer read FStickPosRX;
    property RYPos : integer read FStickPosRY;
    property RZPos : integer read FStickPosRZ;
    property Buttons[index:integer] : boolean read getButton;
    property OnStickMove : TStickEvent read FstickMove write FstickMove;
    property OnButtonPress : TButtonEvent read FButtonPress write FButtonPress;
    property OnPovMove : TPOVEvent read FPOVMove write FPOVMove;
    constructor create;
  end;

implementation

uses main;

{ Important: Methods and properties of objects in visual components can only be
  used in a method called using Synchronize, for example,

      Synchronize(UpdateCaption);

  and UpdateCaption could look like,

    procedure TGamePadThread.UpdateCaption;
    begin
      Form1.Caption := 'Updated in a thread';
    end; }

{ TGamePadThread }

procedure TgamePadThread.StickMove(Sender : Tobject; axis : string; value : integer);
begin
  if assigned(Fstickmove) then
    FstickMove(Sender,axis,value);
end;

procedure TgamePadThread.ButtonPress(Sender : Tobject; button : integer; state : boolean);
begin
  if assigned(FButtonPress) then
    FButtonPress(Sender,button,state);
end;

procedure Tgamepadthread.POVMove(Sender : Tobject; pov : integer);
begin
  if assigned(FPOVMove) then
    FPOVMove(sender,pov);
end;

constructor TGamePadThread.create;
var i : integer;
begin
  DxJoystick := TDxJoyStick.Create(form1);
  inherited create(false);
  DxJoyStick.AutoPoll := false;
  DxjoyStick.Active := true;
  stopped := false;
  FstickPosX := 32767;
  FstickPosY := 32767;
  FstickPosZ := 32767;
  FstickPosRX := 32767;
  FstickPosRY := 32767;
  FstickPosRZ := 32767;
  FPOV := -1;
  for i := 0 to 7 do begin
    Fbuttonsp[i] := false;
  end;
end;

function TGamePadThread.getButton(index : integer):boolean;
begin
  result := Fbuttons[index];
end;

procedure TGamePadThread.Execute;
const
  MinX = 0;
  MaxX = 65535;
  MinY = 0;
  MaxY = 65535;
begin
  while true do begin
    DxJoyStick.Poll;

    {sind die kommentierten funktionen aktiv wird das Event st�ndig ausgel�st
     das f�hrt zum versagen der M�glichkeit die Steurung selbst zu belegen
     Allerdings sind dann die nullstellungen gesichert}
     
    FStickPosXp := DxJoyStick.PositionX;
    if FstickPosXp <> FstickPosX then begin
      StickMove(self,'X',FstickPosXp);
      FStickPosX := DxJoyStick.PositionX;
    end;// else StickMove(self,'X',0);

    FStickPosYp := DxJoyStick.PositionY;
    if FstickPosYp <> FstickPosY then begin
      StickMove(self,'Y',FstickPosYp);
      FStickPosY := DxJoyStick.PositionY;
    end;// else StickMove(self,'Y',0);

    FStickPosZp := DxJoyStick.PositionZ;
    if FstickPosZp <> FstickPosZ then begin
      StickMove(self,'Z',FstickPosZp);
      FStickPosZ := DxJoyStick.PositionZ;
    end;// else StickMove(self,'Z',0);

    FStickPosRXp := DxJoyStick.PositionRX;
    if FstickPosRXp <> FstickPosRX then begin
      StickMove(self,'RX',FstickPosRX);
      FStickPosRX := DxJoyStick.PositionRX;
    end;// else StickMove(self,'RX',0);

    FStickPosRYp := DxJoyStick.PositionRY;
    if FstickPosRYp <> FstickPosRY then begin
      StickMove(self,'RY',FstickPosRY);
      FStickPosRY := DxJoyStick.PositionRY;
    end;// else StickMove(self,'RY',0);

    FStickPosRZp := DxJoyStick.PositionRZ;
    if FstickPosRZp <> FstickPosRZ then begin
      StickMove(self,'RZ',FstickPosRZp);
      FStickPosRZ := DxJoyStick.PositionRZ;
    end;// else StickMove(self,'RZ',0);

    Fslider1posp := DXJoystick.PositionSlider1;
    Fslider2posp := DXJoystick.PositionSlider2;

    Fbuttonsp[0] := joButton1 in DxJoyStick.buttons;
    if FButtonsp[0] <> Fbuttons[0] then begin
      ButtonPress(self,0,Fbuttonsp[0]);
      Fbuttons[0] := Fbuttonsp[0];
    end;

    Fbuttonsp[1] := joButton2 in DxJoyStick.buttons;
    if FButtonsp[1] <> Fbuttons[1] then begin
      ButtonPress(self,1,Fbuttonsp[1]);
      Fbuttons[1] := Fbuttonsp[1];
    end;

    Fbuttonsp[2] := joButton3 in DxJoyStick.buttons;
    if FButtonsp[2] <> Fbuttons[2] then begin
      ButtonPress(self,2,Fbuttonsp[2]);
      Fbuttons[2] := Fbuttonsp[2];
    end;

    Fbuttonsp[3] := joButton4 in DxJoyStick.buttons;
    if FButtonsp[3] <> Fbuttons[3] then begin
      ButtonPress(self,3,Fbuttonsp[3]);
      Fbuttons[3] := Fbuttonsp[3];
    end;

    Fbuttonsp[4] := joButton5 in DxJoyStick.buttons;
    if FButtonsp[4] <> Fbuttons[4] then begin
      ButtonPress(self,4,Fbuttonsp[4]);
      Fbuttons[4] := Fbuttonsp[4];
    end;

    Fbuttonsp[5] := joButton6 in DxJoyStick.buttons;
    if FButtonsp[5] <> Fbuttons[5] then begin
      ButtonPress(self,5,Fbuttonsp[5]);
      Fbuttons[5] := Fbuttonsp[5];
    end;

    Fbuttonsp[6] := joButton7 in DxJoyStick.buttons;
    if FButtonsp[6] <> Fbuttons[6] then begin
      ButtonPress(self,6,Fbuttonsp[6]);
      Fbuttons[6] := Fbuttonsp[6];
    end;

    Fbuttonsp[7] := joButton8 in DxJoyStick.buttons;
    if FButtonsp[7] <> Fbuttons[7] then begin
      ButtonPress(self,7,Fbuttonsp[7]);
      Fbuttons[7] := Fbuttonsp[7];
    end;

    FPOVp := DxJoystick.PointOfView1;
    if FPOVp <> FPOV then begin
      POVMove(self,FPOVp);
      FPOV := DxJoystick.PointOfView1;
    end;

    pause(1);
    if stopped then begin
      break;
    end;
  end;
  self.Terminate;
end;

end.
