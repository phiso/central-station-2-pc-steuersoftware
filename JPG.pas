unit JPG;                    // 5.12.07
{
   load unterstützt die Formate  bmp, jpg, jpeg, gif
   save  kann bmp, und jpg

   Übergabeparameter sind immer
     Name des Bildes mit Pfad ( von openpicturedialog )
     das Bild ( image...  )
   nur load
     zwei inteder , die breite und hoehe des Bildes erhalten
   nur save
      die gewünschte jpegquality  ( von 10-100  )
}

//{$define Jpeg}

interface

uses jpeg,dialogs,
{$ifdef jpeg}
  gifimage,
{$endif}
     ExtCtrls,wintypes,sysutils,classes, graphics,ComCtrls;

function LoadPicture(dpicname:string; Var image1:Timage;Var w,h:integer):integer;

function SavePicture(dpicname:string;image1:Timage;jpegquality:integer):boolean;

implementation

function SavePicture(dpicname:string;image1:Timage;jpegquality:integer):boolean;
var suf:string;
    jp:Tjpegimage; saved:boolean;

begin      saved:=false;
           suf:= ExtractFileExt(dpicname);suf:=UpperCase(suf);
           if suf='.JPG'
           then begin
                 jp := TJPEGImage.Create;
                 jp.CompressionQuality:=  jpegQuality;
                 jp.assign(image1.Picture.Bitmap);
                 jp.jpegneeded;
                 jp.PixelFormat:=jf24Bit;
                 jp.savetoFile(dpicname);
                 saved:=true;
                 jp.destroy;
                end
           else  try
                   image1.Picture.savetoFile(dpicname);
                   saved:=true;
                  finally
                  end;
     result:=saved;
end;

function LoadPicture(dpicname:string; Var image1:Timage;Var w,h:integer):integer;

var  loaded:integer;suf:string;
     jp:Tjpegimage;
     w_original,h_original,maxhoehe:integer;
     map:Tbitmap;
     itemp:Timage;
     Ha:Thandle;p :Hpalette;ff:word;// copy -paste

begin
  image1.visible:=false;
  itemp:=Timage.Create(nil);
  with itemp do
  begin visible:=false;autosize:=true;
        loaded:=0;
        suf:= ExtractFileExt(dpicname);suf:=UpperCase(suf);
        if (suf='.JPG') or (suf='.JPEG')
        then try   jp := TJPEGImage.Create;
               jp.loadfromfile(dpicname) ;
               w_original:=jp.width;
               h_original:=jp.height;
               picture.bitmap.assign(jp);
               jp.destroy;
             except
               on EFopenError do loaded:=2;
               on EReadError do loaded:=2;
               on EInvalidGraphic do loaded:=3;
             end
       else try
              picture.loadfromfile(dpicname) ;
              {$ifdef jpeg}
              if suf='.GIF'
              then begin w_original:=gifwidth;
                         h_original:=gifheight;
                         map:=tbitmap.Create;
                         itemp.picture.savetoclipboardFormat(ff,Ha,p);
                         map.LoadFromClipBoardFormat(cf_BitMap,ha,p);
                         map.PixelFormat:=pf24bit;
                         itemp.Picture.Bitmap:=map;
                         map.free;
                   end
              else {$endif}
                begin w_original:=picture.bitmap.width;
                         h_original:=picture.bitmap.height;
                   end;
           except
             on EFopenError do loaded:=2;
             on EReadError do loaded:=2;
             on EInvalidGraphic do loaded:=3;
           end; (* try *)
       if loaded <>0
       then begin
             if loaded=2
             then messagedlg(dpicname+' nicht vorhanden !', mtERROR,[mbok],0)
             else messagedlg(dpicname+' nicht unterstützt !' , mtERROR,[mbok],0);
            end
       end;(* itemp *)
   w:= w_original; h:= h_original;
   image1.picture.assign(itemp.picture); (* übergeben *)
   itemp.picture:=nil;                   (* stretch true *)
   //image1.visible:=true;

   result:=loaded;
end;


end.
