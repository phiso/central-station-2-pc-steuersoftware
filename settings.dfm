object Form10: TForm10
  Left = 791
  Top = 306
  AutoSize = True
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'Einstellungen'
  ClientHeight = 432
  ClientWidth = 323
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 400
    Width = 323
    Height = 32
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Button4: TButton
      Left = 112
      Top = 0
      Width = 97
      Height = 32
      Caption = #220'bernehmen'
      Enabled = False
      TabOrder = 0
      OnClick = Button4Click
    end
    object Button5: TButton
      Left = 0
      Top = 0
      Width = 97
      Height = 32
      Caption = 'Abbrechen'
      TabOrder = 1
      OnClick = Button5Click
    end
    object Button6: TButton
      Left = 224
      Top = 0
      Width = 97
      Height = 32
      Caption = 'OK'
      TabOrder = 2
      OnClick = Button6Click
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 323
    Height = 400
    Cursor = crHandPoint
    ActivePage = TabSheet1
    Align = alClient
    BiDiMode = bdLeftToRight
    MultiLine = True
    ParentBiDiMode = False
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'Allgemein'
      object GroupBox2: TGroupBox
        Left = 0
        Top = 4
        Width = 313
        Height = 349
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 16
          Width = 120
          Height = 13
          Caption = 'Standard Keyboard breite'
        end
        object Label2: TLabel
          Left = 72
          Top = 36
          Width = 11
          Height = 13
          Caption = 'px'
        end
        object SpinEdit1: TSpinEdit
          Left = 8
          Top = 32
          Width = 65
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 0
          Value = 0
          OnChange = SpinEdit1Change
        end
        object CheckBox15: TCheckBox
          Left = 8
          Top = 64
          Width = 217
          Height = 17
          Caption = 'Keyboard ins hauptfenster integrieren'
          Checked = True
          State = cbChecked
          TabOrder = 1
          OnClick = CheckBox15Click
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Dateien/Projekte'
      ImageIndex = 1
      object GroupBox4: TGroupBox
        Left = 0
        Top = 0
        Width = 313
        Height = 353
        TabOrder = 0
        object CheckBox9: TCheckBox
          Left = 8
          Top = 16
          Width = 137
          Height = 17
          Caption = 'System beim Start laden'
          TabOrder = 0
          OnClick = CheckBox9Click
        end
        object LabeledEdit2: TLabeledEdit
          Left = 8
          Top = 48
          Width = 233
          Height = 21
          EditLabel.Width = 37
          EditLabel.Height = 13
          EditLabel.Caption = 'System:'
          TabOrder = 1
          Visible = False
        end
        object Button1: TButton
          Left = 240
          Top = 48
          Width = 25
          Height = 21
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          Visible = False
          OnClick = Button1Click
        end
        object LabeledEdit3: TLabeledEdit
          Left = 8
          Top = 96
          Width = 233
          Height = 21
          EditLabel.Width = 141
          EditLabel.Height = 13
          EditLabel.Caption = 'Standard Speicherverzeichnis'
          TabOrder = 3
        end
        object Button2: TButton
          Left = 240
          Top = 96
          Width = 25
          Height = 21
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          OnClick = Button2Click
        end
        object LabeledEdit4: TLabeledEdit
          Left = 8
          Top = 152
          Width = 233
          Height = 21
          EditLabel.Width = 120
          EditLabel.Height = 13
          EditLabel.Caption = '"auto-Import" Verzeichnis'
          Enabled = False
          TabOrder = 5
        end
        object Button3: TButton
          Left = 240
          Top = 152
          Width = 25
          Height = 21
          Caption = '...'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
          OnClick = Button3Click
        end
        object CheckBox10: TCheckBox
          Left = 136
          Top = 136
          Width = 25
          Height = 15
          TabOrder = 7
          OnClick = CheckBox10Click
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Drehscheibe'
      ImageIndex = 2
      object GroupBox6: TGroupBox
        Left = 8
        Top = 24
        Width = 305
        Height = 113
        Caption = 'Einstellungen'
        TabOrder = 0
        Visible = False
        object Label3: TLabel
          Left = 8
          Top = 20
          Width = 86
          Height = 13
          Caption = 'Keyboard Adresse'
        end
        object Label4: TLabel
          Left = 8
          Top = 64
          Width = 127
          Height = 13
          Caption = 'Animationsgeschwindigkeit'
        end
        object SpinEdit2: TSpinEdit
          Left = 8
          Top = 35
          Width = 65
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 0
          Value = 15
          OnChange = SpinEdit2Change
        end
        object SpinEdit5: TSpinEdit
          Left = 8
          Top = 80
          Width = 65
          Height = 22
          Hint = '0 = Echtzeit'
          MaxValue = 0
          MinValue = 0
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Value = 0
          OnChange = SpinEdit5Change
        end
      end
      object CheckBox7: TCheckBox
        Left = 8
        Top = 8
        Width = 201
        Height = 17
        Caption = 'Drehkreis 7686/7 vorhanden'
        TabOrder = 1
        OnClick = CheckBox7Click
      end
      object GroupBox15: TGroupBox
        Left = 8
        Top = 136
        Width = 305
        Height = 217
        Caption = 'Tastaturbelegung'
        TabOrder = 2
        object GroupBox16: TGroupBox
          Left = 8
          Top = 16
          Width = 89
          Height = 57
          Caption = 'Step </>'
          TabOrder = 0
          object Label22: TLabel
            Left = 8
            Top = 16
            Width = 8
            Height = 13
            Caption = '<'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label23: TLabel
            Left = 49
            Top = 16
            Width = 8
            Height = 13
            Caption = '>'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Edit11: TEdit
            Left = 8
            Top = 32
            Width = 33
            Height = 21
            TabOrder = 0
            OnChange = Edit11Change
          end
          object Edit12: TEdit
            Left = 48
            Top = 32
            Width = 33
            Height = 21
            TabOrder = 1
            OnChange = Edit12Change
          end
        end
        object GroupBox17: TGroupBox
          Left = 200
          Top = 16
          Width = 89
          Height = 57
          Caption = 'Ziel anfahren'
          TabOrder = 1
          object Edit13: TEdit
            Left = 8
            Top = 24
            Width = 73
            Height = 21
            TabOrder = 0
            OnChange = Edit13Change
          end
        end
        object GroupBox18: TGroupBox
          Left = 104
          Top = 16
          Width = 89
          Height = 57
          Caption = '180'#176' Drehung'
          TabOrder = 2
          object Edit14: TEdit
            Left = 8
            Top = 24
            Width = 73
            Height = 21
            TabOrder = 0
            OnChange = Edit14Change
          end
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Portalkran'
      ImageIndex = 3
      object GroupBox5: TGroupBox
        Left = 8
        Top = 24
        Width = 137
        Height = 65
        Caption = 'Adressen'
        TabOrder = 0
        Visible = False
        object Label5: TLabel
          Left = 8
          Top = 20
          Width = 34
          Height = 13
          Caption = 'Br'#252'cke'
        end
        object Label6: TLabel
          Left = 72
          Top = 20
          Width = 45
          Height = 13
          Caption = 'Kranhaus'
        end
        object SpinEdit3: TSpinEdit
          Left = 8
          Top = 35
          Width = 49
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 0
          Value = 0
          OnChange = SpinEdit3Change
        end
        object SpinEdit4: TSpinEdit
          Left = 72
          Top = 35
          Width = 49
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 1
          Value = 0
          OnChange = SpinEdit4Change
        end
      end
      object CheckBox8: TCheckBox
        Left = 8
        Top = 6
        Width = 137
        Height = 17
        Caption = 'Portalkran vorhanden'
        TabOrder = 1
        OnClick = CheckBox8Click
      end
      object GroupBox7: TGroupBox
        Left = 8
        Top = 88
        Width = 137
        Height = 265
        Caption = 'erweitert'
        TabOrder = 2
        object Label7: TLabel
          Left = 8
          Top = 208
          Width = 69
          Height = 13
          Caption = 'verw. Zubeh'#246'r'
        end
        object Label8: TLabel
          Left = 8
          Top = 16
          Width = 104
          Height = 13
          Caption = 'auto Br'#252'ckengeschw.'
        end
        object Label9: TLabel
          Left = 8
          Top = 64
          Width = 109
          Height = 13
          Caption = 'auto Kranhausgeschw.'
        end
        object Label10: TLabel
          Left = 8
          Top = 112
          Width = 87
          Height = 13
          Caption = 'auto Drehgeschw.'
        end
        object Label11: TLabel
          Left = 8
          Top = 160
          Width = 96
          Height = 13
          Caption = 'auto Hakengeschw.'
        end
        object ComboBox1: TComboBox
          Left = 8
          Top = 224
          Width = 121
          Height = 21
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 0
          Text = 'Keins'
          OnChange = ComboBox1Change
          Items.Strings = (
            'Keins'
            'Elektromagnet'
            'Greifer/Schaufel')
        end
        object SpinEdit6: TSpinEdit
          Left = 8
          Top = 32
          Width = 121
          Height = 22
          MaxValue = 997
          MinValue = 0
          TabOrder = 1
          Value = 100
          OnChange = SpinEdit6Change
        end
        object SpinEdit7: TSpinEdit
          Left = 8
          Top = 80
          Width = 121
          Height = 22
          MaxValue = 997
          MinValue = 0
          TabOrder = 2
          Value = 100
          OnChange = SpinEdit7Change
        end
        object SpinEdit8: TSpinEdit
          Left = 8
          Top = 128
          Width = 121
          Height = 22
          MaxValue = 997
          MinValue = 0
          TabOrder = 3
          Value = 100
          OnChange = SpinEdit8Change
        end
        object SpinEdit9: TSpinEdit
          Left = 8
          Top = 176
          Width = 121
          Height = 22
          MaxValue = 997
          MinValue = 0
          TabOrder = 4
          Value = 100
          OnChange = SpinEdit9Change
        end
      end
      object GroupBox9: TGroupBox
        Left = 158
        Top = 24
        Width = 147
        Height = 329
        Caption = 'Tastaturbelegung'
        TabOrder = 3
        object GroupBox10: TGroupBox
          Left = 8
          Top = 16
          Width = 129
          Height = 57
          Caption = 'Kranbr'#252'cke fahren'
          TabOrder = 0
          object Label14: TLabel
            Left = 8
            Top = 16
            Width = 41
            Height = 13
            Caption = 'Vorw'#228'rts'
          end
          object Label15: TLabel
            Left = 64
            Top = 16
            Width = 51
            Height = 13
            Caption = 'R'#252'ckw'#228'rts'
          end
          object Edit1: TEdit
            Left = 8
            Top = 28
            Width = 33
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 0
            OnChange = Edit1Change
          end
          object Edit2: TEdit
            Left = 64
            Top = 28
            Width = 33
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 1
            OnChange = Edit2Change
          end
        end
        object GroupBox11: TGroupBox
          Left = 8
          Top = 128
          Width = 129
          Height = 57
          Caption = 'Kranhaus fahren'
          TabOrder = 1
          object Label16: TLabel
            Left = 8
            Top = 16
            Width = 41
            Height = 13
            Caption = 'Vorw'#228'rts'
          end
          object Label17: TLabel
            Left = 64
            Top = 16
            Width = 51
            Height = 13
            Caption = 'R'#252'ckw'#228'rts'
          end
          object Edit3: TEdit
            Left = 8
            Top = 28
            Width = 33
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 0
            OnChange = Edit3Change
          end
          object Edit4: TEdit
            Left = 64
            Top = 28
            Width = 33
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 1
            OnChange = Edit4Change
          end
        end
        object GroupBox12: TGroupBox
          Left = 8
          Top = 72
          Width = 129
          Height = 57
          Caption = 'Kranhaus drehen'
          TabOrder = 2
          object Label18: TLabel
            Left = 64
            Top = 16
            Width = 51
            Height = 13
            Caption = 'R'#252'ckw'#228'rts'
          end
          object Label19: TLabel
            Left = 8
            Top = 16
            Width = 41
            Height = 13
            Caption = 'Vorw'#228'rts'
          end
          object Edit5: TEdit
            Left = 8
            Top = 28
            Width = 33
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 0
            OnChange = Edit5Change
          end
          object Edit6: TEdit
            Left = 64
            Top = 28
            Width = 33
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 1
            OnChange = Edit6Change
          end
        end
        object GroupBox13: TGroupBox
          Left = 8
          Top = 184
          Width = 129
          Height = 57
          Caption = 'Haken bedienen'
          TabOrder = 3
          object Label20: TLabel
            Left = 64
            Top = 16
            Width = 51
            Height = 13
            Caption = 'R'#252'ckw'#228'rts'
          end
          object Label21: TLabel
            Left = 8
            Top = 16
            Width = 41
            Height = 13
            Caption = 'Vorw'#228'rts'
          end
          object Edit7: TEdit
            Left = 8
            Top = 28
            Width = 33
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 0
            OnChange = Edit7Change
          end
          object Edit8: TEdit
            Left = 64
            Top = 28
            Width = 33
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 1
            OnChange = Edit8Change
          end
        end
        object GroupBox14: TGroupBox
          Left = 8
          Top = 240
          Width = 129
          Height = 65
          Caption = 'Funktionen'
          TabOrder = 4
          object Label12: TLabel
            Left = 8
            Top = 16
            Width = 23
            Height = 13
            Caption = 'Licht'
          end
          object Label13: TLabel
            Left = 53
            Top = 16
            Width = 60
            Height = 13
            Caption = 'Kranfunktion'
          end
          object Edit9: TEdit
            Left = 8
            Top = 28
            Width = 33
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 0
            OnChange = Edit9Change
          end
          object Edit10: TEdit
            Left = 53
            Top = 28
            Width = 33
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 1
            OnChange = Edit10Change
          end
        end
        object CheckBox11: TCheckBox
          Left = 8
          Top = 304
          Width = 129
          Height = 17
          Hint = 'STRG + die jeweilige Taste'
          Caption = 'Boost Funktion'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = CheckBox11Click
        end
      end
      object CheckBox14: TCheckBox
        Left = 160
        Top = 6
        Width = 121
        Height = 17
        Caption = 'Tastatursteuerung'
        TabOrder = 4
        OnClick = CheckBox14Click
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'Netzwerk'
      ImageIndex = 4
      object GroupBox3: TGroupBox
        Left = 0
        Top = 0
        Width = 313
        Height = 353
        TabOrder = 0
        object LabeledEdit1: TLabeledEdit
          Left = 8
          Top = 80
          Width = 145
          Height = 21
          EditLabel.Width = 51
          EditLabel.Height = 13
          EditLabel.Caption = 'IP-Adresse'
          Enabled = False
          TabOrder = 0
          OnChange = LabeledEdit1Change
        end
        object CheckBox1: TCheckBox
          Left = 8
          Top = 16
          Width = 105
          Height = 17
          Caption = 'Statische Adresse'
          TabOrder = 1
          OnClick = CheckBox1Click
        end
        object CheckBox2: TCheckBox
          Left = 8
          Top = 40
          Width = 121
          Height = 17
          Caption = 'auto-Suche'
          Checked = True
          State = cbChecked
          TabOrder = 2
          OnClick = CheckBox2Click
        end
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'Debugging'
      ImageIndex = 5
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 313
        Height = 353
        TabOrder = 0
        object CheckBox3: TCheckBox
          Left = 8
          Top = 8
          Width = 113
          Height = 17
          Caption = 'Debugging'
          TabOrder = 0
          OnClick = CheckBox3Click
        end
        object CheckBox4: TCheckBox
          Left = 8
          Top = 32
          Width = 65
          Height = 17
          Caption = 'Logging'
          TabOrder = 1
          OnClick = CheckBox4Click
        end
        object CheckBox5: TCheckBox
          Left = 8
          Top = 56
          Width = 113
          Height = 17
          Caption = 'Directsend Konsole'
          TabOrder = 2
          OnClick = CheckBox5Click
        end
        object CheckBox6: TCheckBox
          Left = 8
          Top = 80
          Width = 89
          Height = 17
          Caption = 'Befehlseditor'
          TabOrder = 3
          OnClick = CheckBox6Click
        end
      end
    end
    object TabSheet7: TTabSheet
      Caption = 'GamePad'
      ImageIndex = 6
      object GroupBox8: TGroupBox
        Left = 0
        Top = 0
        Width = 313
        Height = 353
        TabOrder = 0
        object CheckBox12: TCheckBox
          Left = 8
          Top = 16
          Width = 129
          Height = 17
          Caption = 'GamePad aktivieren'
          TabOrder = 0
          OnClick = CheckBox12Click
        end
        object CheckBox13: TCheckBox
          Left = 8
          Top = 40
          Width = 137
          Height = 17
          Caption = 'Steuerung per D-Pad'
          TabOrder = 1
          OnClick = CheckBox13Click
        end
        object GroupBox19: TGroupBox
          Left = 8
          Top = 64
          Width = 289
          Height = 281
          Caption = 'Belegung'
          TabOrder = 2
          object Label24: TLabel
            Left = 8
            Top = 24
            Width = 76
            Height = 13
            Caption = 'Br'#252'cke fahren : '
          end
          object Label25: TLabel
            Left = 8
            Top = 56
            Width = 87
            Height = 13
            Caption = 'Kranhaus fahren : '
          end
          object Label26: TLabel
            Left = 8
            Top = 88
            Width = 90
            Height = 13
            Caption = 'Kranhaus drehen : '
          end
          object Label27: TLabel
            Left = 8
            Top = 120
            Width = 134
            Height = 13
            Caption = 'Kranhaken heben/senken : '
          end
          object Label28: TLabel
            Left = 8
            Top = 184
            Width = 69
            Height = 13
            Caption = 'Licht an/aus : '
          end
          object Label29: TLabel
            Left = 8
            Top = 152
            Width = 122
            Height = 13
            Caption = 'Hakenfunktion schalten : '
          end
          object Label30: TLabel
            Left = 88
            Top = 24
            Width = 38
            Height = 13
            Caption = 'Label30'
          end
          object Label31: TLabel
            Left = 104
            Top = 56
            Width = 38
            Height = 13
            Caption = 'Label31'
          end
          object Label32: TLabel
            Left = 104
            Top = 88
            Width = 38
            Height = 13
            Caption = 'Label32'
          end
          object Label33: TLabel
            Left = 144
            Top = 120
            Width = 38
            Height = 13
            Caption = 'Label33'
          end
          object Label34: TLabel
            Left = 136
            Top = 152
            Width = 38
            Height = 13
            Caption = 'Label34'
          end
          object Label35: TLabel
            Left = 80
            Top = 184
            Width = 38
            Height = 13
            Caption = 'Label35'
          end
          object Label36: TLabel
            Left = 8
            Top = 216
            Width = 49
            Height = 13
            Caption = 'Stoppen : '
          end
          object Label37: TLabel
            Left = 64
            Top = 216
            Width = 38
            Height = 13
            Caption = 'Label37'
          end
          object Button7: TButton
            Left = 200
            Top = 19
            Width = 75
            Height = 20
            Caption = #228'ndern'
            TabOrder = 0
            OnClick = Button7Click
          end
          object Button8: TButton
            Left = 200
            Top = 51
            Width = 75
            Height = 20
            Caption = #228'ndern'
            TabOrder = 1
            OnClick = Button8Click
          end
          object Button9: TButton
            Left = 200
            Top = 83
            Width = 75
            Height = 20
            Caption = #228'ndern'
            TabOrder = 2
            OnClick = Button9Click
          end
          object Button10: TButton
            Left = 200
            Top = 115
            Width = 75
            Height = 20
            Caption = #228'ndern'
            TabOrder = 3
            OnClick = Button10Click
          end
          object Button11: TButton
            Left = 200
            Top = 147
            Width = 75
            Height = 20
            Caption = #228'ndern'
            TabOrder = 4
            OnClick = Button11Click
          end
          object Button12: TButton
            Left = 200
            Top = 179
            Width = 75
            Height = 20
            Caption = #228'ndern'
            TabOrder = 5
            OnClick = Button12Click
          end
          object Button13: TButton
            Left = 200
            Top = 211
            Width = 75
            Height = 20
            Caption = #228'ndern'
            TabOrder = 6
            OnClick = Button13Click
          end
        end
      end
    end
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '.ini'
    Filter = 'ini-Dateien (.ini)|*.ini'
    Top = 384
  end
  object OpenDialog1: TOpenDialog
    Filter = 'ini-Dateien (.ini)|*.ini'
    Left = 32
    Top = 384
  end
  object MainMenu1: TMainMenu
    Left = 64
    Top = 384
    object Einstellungen1: TMenuItem
      Caption = 'Einstellungen'
      object Voreinstellungladen1: TMenuItem
        Caption = 'Voreinstellung laden'
        OnClick = Voreinstellungladen1Click
      end
      object Einstellungenspeichern1: TMenuItem
        Caption = 'Einstellungen speichern'
        OnClick = Einstellungenspeichern1Click
      end
      object Beenden1: TMenuItem
        Caption = 'Beenden'
        OnClick = Beenden1Click
      end
    end
  end
  object Timer1: TTimer
    Interval = 100
    OnTimer = Timer1Timer
    Left = 288
    Top = 40
  end
end
