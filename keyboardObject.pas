unit keyboardObject;

interface

uses typehelper, Sysutils, Classes, stdCtrls, extCtrls, controls, math, types,
     graphics, jpeg;

type TKeyboardEvent = procedure(Sender : TObject; obj,state : integer) of object;
type TKeyboardObject = class(Tpanel)
  private
    FClickEvent : TKeyboardEvent;
    Fimage : array[0..3] of Timage;
    FBmpActive,FBmpInactive : array[0..3] of TBitmap;
    Flabel : Tlabel;
    Fcaption, FFilePath : string;
    FArticle : TArticle;
    Fparent : Tcomponent;
    FIndex  : integer;

    procedure SetCaption(cap : string);
    function getAdrCount(typ : string):integer;
    procedure ImageClick(Sender : Tobject);
  protected
    procedure click(Sender : Tobject; obj,state : integer);
  public
    property OnClick :TKeyboardEvent read FClickEvent write FClickevent;
    property Caption : string read Fcaption write setcaption;

    constructor create(article : TArticle; Owner : Tcomponent; nr : integer);
end;

implementation

uses main;

procedure TKeyboardObject.click(Sender : Tobject; obj,state : integer);
begin
  if Assigned(FclickEvent) then
    FclickEvent(Sender,obj,state);
end;

procedure TKeyboardObject.ImageClick(Sender : Tobject);
begin
  click(Sender,FIndex,strtoint((Sender as Timage).hint));
end;

constructor TkeyboardObject.create(article : TArticle; Owner : Tcomponent; nr : integer);
{Von der Nummerierung h�ngt ab ob gerade zahlen oben sind oder unten}
var i : integer;
begin
  inherited create(Owner);
  FArticle := article;
  FParent := Owner;
  Findex := nr;
  self.Height := 60;
  self.Width := getAdrCount(Farticle.Typ)*60;
  self.Left := 20+(floor(nr/2)*60);
  if self.Width > 60 then self.Left := self.Left + 60;
  if nr mod 2 = 0 then begin // ist die obere reihe dran oder die untere
    self.Top := 100;
  end else begin
    self.Top := 20;
  end;

  for i := 0 to 3 do begin
    Fimage[i] := Timage.Create(self);
    Fimage[i].Parent := self;
    if (i > 1) and (getAdrCount(Farticle.Typ) > 1) then
      Fimage[i].Visible := true
      else Fimage[i].Visible := false;
    Fimage[i].Width := 60;
    Fimage[i].Height := 60;
    if (i+1) mod 2 = 0 then Fimage[i].Top := 60
    else Fimage[i].Top := 0;
    case i of
      0,1:Fimage[i].Left := 0;
      2,3:Fimage[i].Left := 60;
    end;
    Fimage[i].Hint := inttostr(i);
    Fimage[i].OnClick := imageclick;
  end;

  FFilepath := form1.Default_IconPath+'\keyboard\';
  for i := 0 to 3 do begin
    FBmpActive[i] := Tbitmap.Create;
    FbmpInactive[i] := Tbitmap.Create;

    try
      FbmpActive[i].LoadFromFile(FFilepath+Farticle.Typ+'_1_'+inttostr(i)+'.bmp');
    except
    end;

    try
      FbmpInActive[i].LoadFromFile(FFilepath+Farticle.Typ+'_0_'+inttostr(i)+'.bmp');
    except
    end;
    
  end;
end;

function TkeyboardObject.getAdrCount(typ : string):integer;
begin
  result := 1;
  if (typ = 'dkw_2antriebe') or (typ = 'lichtsignal_HP012_SH01') or
     (typ = 'formsignal_HP012_SH01')
     then result := 2;
end;

procedure TkeyboardObject.SetCaption(cap : string);
begin
  Fcaption := cap;
  Flabel.Caption := cap;
  Flabel.Refresh;
end;

end.
