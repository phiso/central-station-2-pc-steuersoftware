unit GridImage;

interface

uses Sysutils, Classes, ExtCtrls, Controls, Math, types, typehelper, graphics,
     layoutObject, inifiles, insertLayoutObject;

const GRIDXY = 20;

type TGridMouseEvent = procedure(Sender : Tobject; x,y : integer) of object;
type TGridMoveEvent = procedure(Sender :Tobject; xlim,ylim : integer) of object;
type TGridImage = class
  private
    FImage : Timage;
    FDelImg : Tpicture;
    FGridClick, FGridMouseOver, FGridMouseDown, FGridMouseUp : TGridMouseEvent;
    FGridMove : TgridMoveEvent;
    FYMax,FXMax,FYMin,FXMin,FYRange,FXRange,
    FWidth,Fheight,FXDiv,FYDiv : integer;
    FConstructMode : boolean;
    FPos : Tpoint;
    Fobjects : array of array of  TLayoutObject;
//    FLayoutArray : TLayoutArray;
    FoccupiedFields : array of Tpoint;

    procedure DrawGrid;
    procedure ImageMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ImageMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ImageMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ImageClick(Sender : Tobject);
    function getCorner(x,y : integer):Tpoint;
    function getRect(x,y : integer):TRect;
    procedure drawNumbers;
  protected
    procedure GridClick(Sender : TObject; x,y : integer);
    procedure GridMouseOver(Sender : TObject; x,y : integer);
    procedure GridMove(Sender : Tobject; xlim,ylim : integer);
    procedure GridMouseDown(Sender : Tobject; x,y : integer);
    procedure GridMouseUp(Sender : Tobject; x,y : integer);
  public
    property OnGridClick : TGridMouseEvent read FGridClick write FGridClick;
    property OnGridMouseOver : TGridMouseEvent read FGridMouseOver write FGridMouseOver;
    property OnGridMouseDown : TGridMouseEvent read FGridMouseDown write FgridMouseDown;
    property OnGridMouseUp : TGridMouseEvent read FGridMouseUp write FGridMouseUp;
    property YMax : integer read FYMax;
    property YMin : integer read FYMin;
    property XMax : integer read FXMax;
    property XMin : integer read FXMin;
    property XRange : integer read FXRange;
    property YRange : integer read FYRange;

    constructor create(var img : Timage);

    procedure ToggleGrid(mode:integer=-1);
    function InsertImage(img : TGraphic; x,y : integer):boolean;
    procedure InsertText(x,y : integer; str : string);
    procedure InsertLayoutObject(lob : TLayoutObject; x,y : integer);
    procedure Turnposition(t,x,y : integer);
    procedure TurnNextAtPos(x,y : integer);
    procedure ResetAllLayoutBitmaps;
    procedure ClearPos(x,y : integer);
    procedure ClearAll;
    procedure AutoResize(xsize,ysize : integer);
    procedure Save(path : string);
    procedure Load(src : string; mode : boolean);
end;

implementation

uses main, newArticle;

procedure TGridImage.GridClick(Sender : TObject; x,y : integer);
begin
  if Assigned(FGridClick) then
    FGridClick(Sender,x,y);
end;

procedure TGridImage.GridMouseOver(Sender : TObject; x,y : integer);
begin
  if Assigned(FGridMouseOver) then
    FGridMouseOver(Sender,x,y);
end;

procedure Tgridimage.GridMove(Sender : Tobject; xlim,ylim : integer);
begin
  if assigned(FgridMove) then
    FGridMove(Sender, xlim,ylim);
end;

procedure TGridImage.GridMouseDown(Sender : Tobject; x,y : integer);
begin
  if assigned(FGridMouseDown) then
    FgridMouseDown(Sender,x,y);
end;

procedure TGridImage.GridMouseUp(Sender : Tobject; x,y : integer);
begin
  if assigned(FGridMouseUp) then
    FgridMouseUp(Sender,x,y);
end;

constructor TGridimage.create(var img : Timage);
begin
  inherited create;
  Fimage := img;
  Fimage.Align := alClient;
  Fwidth := Fimage.Width;
  Fimage.OnMouseMove := ImageMouseMove;
  Fimage.OnClick := ImageClick;
  
  //All White bmp to clear existing fields//
  FDelImg := Tpicture.Create;
  FDelImg.LoadFromFile(form1.Default_SystemPath+'data\sys\white.bmp');

  Fheight := Fimage.Height;
  FXRange := floor(Fwidth/GRIDXY);
  FYRange := floor(Fheight/GRIDXY);
  FYMin := 1;
  FXMin := 1;
  FXMax := FXRange;
  FYMax := FYRange;
  FXDiv := 0;
  FYDiv := 0;
  setlength(FObjects,FXmax,FYmax);
//  setlength(FLayoutArray,FXmax*Fymax);
  Fimage.Canvas.Pen.Mode := pmNotXor;
  Fimage.Canvas.Pen.Color := $FFd000;
  FConstructmode := false;
//  drawgrid;
end;

procedure TGridImage.DrawGrid;
var i : integer;
begin
  with Fimage.Canvas do begin
    for i := 1 to FYRange-1 do begin
      MoveTo(0,i*GRIDXY);
      LineTo(Fwidth,i*GRIDXY);
    end;
    for i := 1 to FXRange-1 do begin
      MoveTo(i*GRIDXY,0);
      LineTo(i*GRIDXY,Fheight);
    end;
  end;
  drawNumbers;
end;

procedure TGridImage.ToggleGrid(mode:integer=-1);
var i : integer;
begin
  if mode = -1 then begin
    if FconstructMode then begin
      FconstructMode := false;
    end else begin
      FconstructMode := true;
    end;
    Drawgrid;
  end else begin
    for i := 0 to mode do begin
      drawgrid;
    end;
  end;
end;

function TGridImage.getCorner(x,y : integer):Tpoint;
begin
  result.X := x*GRIDXY;
  result.Y := y*GRIDXY;
end;

function TGridImage.getRect(x,y : integer):TRect;
var c : Tpoint;
begin
  c := getCorner(x,y);
  if Fconstructmode then result := rect(c.X+1,c.Y+1,c.X+GRIDXY-1,c.Y+GRIDXY-1)
  else result := rect(c.X,c.Y,c.X+GRIDXY,c.Y+GRIDXY);
//  result := rect(c.X,c.Y,c.X+GRIDXY,c.Y+GRIDXY);
end;

procedure TGridImage.ImageMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
var new_x,new_Y : integer;
begin
  new_x := floor(x/GRIDXY)+FXDiv;
  new_y := floor(y/GRIDXY)+FYDiv;
  FPos := point(new_x,new_y);
  GridMouseOver(Sender,new_x,new_y);
end;

procedure TGridImage.drawNumbers;
var i : integer;
    p : Tpoint;
    rec : TRect;
begin
  for i := FXMin to FXMax-1 do begin
    p := getCorner(i,0);
    rec := rect(p.X+1,p.y+1,p.X+GRIDXY-1,p.Y+GRIDXY-1);
    Fimage.Canvas.TextRect(rec,p.X+3,p.Y+3,inttostr(i));
  end;
  for i := FYMin to FYMax-1 do begin
    p := getCorner(0,i);
    rec := rect(p.X+1,p.y+1,p.X+GRIDXY-1,p.Y+GRIDXY-1);
    Fimage.Canvas.TextRect(rec,p.X+3,p.Y+3,inttostr(i));
  end;
end;

function TGridImage.InsertImage(img : Tgraphic; x,y : integer):boolean;
var rec : TRect;
begin
  if (img.Width <= GRIDXY) and (img.Height <= GRIDXY) and (x>=1) and (y>=1) then begin
    rec := getRect(x,y);
    Fimage.Canvas.StretchDraw(rec,img);
    result := true;
  end else result := false;
  Fimage.Refresh;
end;

procedure TGridimage.ImageClick(Sender : Tobject);
begin
  GridClick(Fobjects[Fpos.x,Fpos.y],Fpos.X,Fpos.Y);
end;

procedure TGridimage.InsertLayoutObject(lob : TLayoutObject; x,y : integer);
var  list : Tstringlist;
begin
  list := TStringlist.Create;
  InsertImage(lob.Getgraphics,x,y);
  Fobjects[x,y] := TLayoutobject.create(lob.Typ,point(x,y),lob.state,lob.Turned);
  setlength(FoccupiedFields,length(FOccupiedFields)+1);
  {setlength(FLayoutArray,length(FLayoutArray)+1);
  FLayoutArray[length(FLayoutArray)-1] := TLayoutobject.create(lob.Typ,point(x,y),lob.state,lob.Turned);}
  FoccupiedFields[length(FOccupiedFields)-1] := point(x,y);

  if (lob.Typ <> 'text') and (lob.Typ <> 'bogen') and (lob.Typ <> 'doppelbogen') and
     (lob.Typ <> 'gerade') and (lob.Typ <> 'tunnel') and (lob.Typ <> 'unterfuehrung') and
     (lob.Typ <> 'prellbock') and (lob.Typ <> 'kreuzung') then
  begin
//    form9.Start(self, Fobjects[x,y]);
    form13.start(lob.Typ);
  end;
end;

procedure TGridimage.Turnposition(t,x,y : integer);
begin
  if assigned(Fobjects[x,y]) then begin
    Fobjects[x,y].Turn(t);
    insertImage(Fobjects[x,y].Getgraphics,x,y);
    Fimage.Refresh;
  end;
end;

procedure TGridimage.TurnNextAtPos(x,y : integer);
begin
  if assigned(Fobjects[x,y]) then begin
    Fobjects[x,y].TurnNext;
    insertImage(Fobjects[x,y].Getgraphics,x,y);
    Fimage.Refresh;
  end;
end;

procedure Tgridimage.ResetAllLayoutBitmaps;
var i : integer;
begin
  for i := 0 to length(FoccupiedFields)-1 do begin
    Fobjects[FoccupiedFields[i].X,FoccupiedFields[i].Y].resetBitmap;
  end;
end;

procedure TGridImage.ClearPos(x,y : integer);

  procedure DelPos(x,y : integer);
  var i,n : integer;
  begin
    i := length(FOccupiedFields)+1;
    n := 0;
    repeat
      if (FoccupiedFields[n].X = x) and (FoccupiedFields[n].Y = y) then
        i := n;
      if n > i then begin
        Foccupiedfields[n-1] := Foccupiedfields[n];
      end;
      inc(n);
    until n >= length(FOccupiedFields);
    setlength(Foccupiedfields,length(Foccupiedfields)-1);
  end;

var rec : Trect;
begin
  if ObjektAssigned(Fobjects[x,y]) then begin
    rec := getRect(x,y);
    Fimage.Canvas.StretchDraw(rec,FDelImg.Graphic);
    DelPos(x,y);
    Fobjects[x,y].Destroy;
  end;
end;

procedure TgridImage.ClearAll;
var x_i,y_i : integer;
begin
  drawgrid;
  drawgrid;
end;

procedure Tgridimage.InsertText(x,y : integer; str : string);
begin
  Fimage.Canvas.TextOut(getcorner(x,y).X,getcorner(x,y).Y,str);
  FObjects[x,y] := TLayoutObject.create('text',point(x,y));
  setlength(FoccupiedFields,length(FOccupiedFields)+1);
  FoccupiedFields[length(FOccupiedFields)-1] := point(x,y);
end;

procedure Tgridimage.AutoResize(xsize,ysize : integer);
begin
  Fwidth := xsize;
  Fheight := ysize;
  Fimage.Width := xsize;
  Fimage.Height := ysize;
  FXRange := floor(Fwidth/GRIDXY);
  FYRange := floor(Fheight/GRIDXY);
  FYMin := 1;
  FXMin := 1;
  FXMax := FXRange;
  FYMax := FYRange;
  togglegrid(1);
end;

procedure Tgridimage.Save(path : string);
  procedure SaveLayoutObject(var f : Tinifile; section : string; lob :TLayoutObject);
  begin
    f.WriteString(section,'typ',lob.Typ);
    f.WriteInteger(section,'Adr',lob.adr);
    f.WriteString(section,'dectype',decoderTostr(lob.decType));
    f.Writeinteger(section,'turn',lob.Turned);
    f.WriteInteger(section,'state',lob.state);
    f.WriteInteger(section,'posx',lob.position.X);
    f.WriteInteger(section,'posy',lob.position.Y);
  end;
var ini : Tinifile;
    x,y,n : integer;
begin
  ini := Tinifile.Create(path);
  n := 0;
  try
    ini.WriteInteger('HEAD','xmax',FXmax);
    ini.WriteInteger('HEAD','ymax',FYmax);
    for x := 0 to FXmax-1 do begin
      for y := 0 to FYMax-1 do begin
        if ObjektAssigned(Fobjects[x,y]) then
          SavelayoutObject(ini,'Object '+inttostr(n),Fobjects[x,y])
        else ini.WriteString('Object '+inttostr(n),'typ','empty');
        inc(n);
      end;
    end;
    ini.WriteInteger('INFO','Objects',n);
  finally
    ini.Free;
  end;
end;

procedure Tgridimage.Load(src : string; mode : boolean);
  procedure LoadLayoutObject(section : string; f : Tinifile; var result : TlayoutObject);
  var tempstr,tempdec : string;
      tempadr,tempturn,tempstate,
      x,y : integer;
  begin
    tempstr := f.ReadString(section,'typ','empty');
    tempadr := f.ReadInteger(section,'Adr',-1);
    tempturn := f.ReadInteger(section,'turn',0);
    tempstate :=  f.ReadInteger(section,'state',0);
    x := f.ReadInteger(section,'posx',0);
    y := f.ReadInteger(section,'posy',0);
    tempdec := f.ReadString(section,'dectype','MM2Shift');
    result := TLayoutobject.create(tempstr,point(x,y),tempstate,tempturn);
    result.SetControl(tempadr,StrToDec(tempdec));
  end;
var ini : Tinifile;
    len,x,y,n : integer;
begin
  ini := Tinifile.Create(src);
  n := 0;
  Fconstructmode := mode;
  try
    len := ini.ReadInteger('INFO','Objects',0);
    FXmax := ini.ReadInteger('HEAD','xmax',FXmax);
    FYmax := ini.ReadInteger('HEAD','ymax',FYmax);
    setlength(FObjects,FXmax,Fymax);
    for x := 0 to FXmax-1 do begin
      for y := 0 to FYmax-1 do begin
        LoadLayoutObject('Object '+inttostr(n),ini,Fobjects[x,y]);
        if Fobjects[x,y].Typ <> 'empty' then
          insertLayoutObject(FObjects[x,y],x,y);
        inc(n);
      end;
    end;
  finally
    ini.Free;
  end;
  Fimage.Refresh;
end;

procedure TgridImage.ImageMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
end;

procedure TGridImage.ImageMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
end;

end.
