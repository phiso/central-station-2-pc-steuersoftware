object Form17: TForm17
  Left = 1538
  Top = 232
  BorderStyle = bsToolWindow
  Caption = 'Aktion Hinzuf'#252'gen'
  ClientHeight = 300
  ClientWidth = 190
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 8
    Width = 31
    Height = 13
    Caption = 'Objekt'
  end
  object Label2: TLabel
    Left = 0
    Top = 56
    Width = 38
    Height = 13
    Caption = 'Adresse'
  end
  object Label3: TLabel
    Left = 0
    Top = 104
    Width = 30
    Height = 13
    Caption = 'Aktion'
    Visible = False
  end
  object Label4: TLabel
    Left = 0
    Top = 152
    Width = 23
    Height = 13
    Caption = 'Wert'
    Visible = False
  end
  object Label5: TLabel
    Left = 0
    Top = 152
    Width = 23
    Height = 13
    Caption = 'Wert'
    Visible = False
  end
  object ComboBox1: TComboBox
    Left = 0
    Top = 24
    Width = 169
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    Text = 'Ausw'#228'hlen....'
    OnChange = ComboBox1Change
    Items.Strings = (
      'Magnetartikel'
      'Zug')
  end
  object SpinEdit1: TSpinEdit
    Left = 0
    Top = 72
    Width = 73
    Height = 22
    MaxValue = 1024
    MinValue = 1
    TabOrder = 1
    Value = 1
  end
  object ComboBox2: TComboBox
    Left = 0
    Top = 120
    Width = 169
    Height = 21
    ItemHeight = 13
    TabOrder = 2
    Text = 'Ausw'#228'hlen...'
    Visible = False
    OnChange = ComboBox2Change
    Items.Strings = (
      'Richtung '#228'ndern'
      'Geschwindigkeit '#228'ndern'
      'Funktion schalten')
  end
  object ComboBox3: TComboBox
    Left = 0
    Top = 168
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 3
    Text = 'Ausw'#228'hlen...'
    Visible = False
    Items.Strings = (
      'Aus, Rund, Rot, Rechts, HP0'
      'Ein, Gr'#252'n, Gerade, HP1'
      'Gelb, Links, HP2'
      'Weis, SH0')
  end
  object SpinEdit2: TSpinEdit
    Left = 0
    Top = 168
    Width = 121
    Height = 22
    MaxValue = 1000
    MinValue = 0
    TabOrder = 4
    Value = 0
    Visible = False
  end
  object Button1: TButton
    Left = 0
    Top = 264
    Width = 89
    Height = 33
    Caption = 'Abbrechen'
    TabOrder = 5
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 96
    Top = 264
    Width = 89
    Height = 33
    Caption = 'Erstellen'
    TabOrder = 6
  end
  object ComboBox4: TComboBox
    Left = 0
    Top = 168
    Width = 145
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 7
    Text = 'Vorw'#228'rts'
    Visible = False
    Items.Strings = (
      'Vorw'#228'rts'
      'R'#252'ckw'#228'rts')
  end
end
