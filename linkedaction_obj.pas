unit linkedaction_obj;

interface

uses Forms, typehelper, Sysutils, Classes, stdctrls, types, dialogs, extctrls,
     controls;

type TActivateEvent = procedure(Sender : Tobject; id : integer) of object;
type TLinkedActionObj = class(TPanel)
    Box : TGroupBox;
    innerPanel : TPanel;
    img : Timage;
  private
    FName : string;
    FID : integer;

    FClickEvent : TActivateEvent;

    procedure PanelMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PanelMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  protected
    procedure clicking(Sender : Tobject; id : integer);
  public
    property OnActivate : TActivateEvent read FClickEvent write FClickevent;
    constructor create(name : string; nr : integer; owner : Tcomponent);
end;

implementation

procedure TLinkedActionObj.clicking(Sender : Tobject; id : integer);
begin
  if assigned(FClickEvent) then
    FClickEvent(sender,id);
end;

constructor TLinkedActionObj.create(name : string; nr : integer; owner : Tcomponent);
begin
  inherited create(Owner);
  self.Parent := (Owner as TWinControl);
  self.Width := 60;
  self.Height := 80;
  self.Top := 15;
  self.Left := 1+(nr*80);
  FID := nr;
  Fname := name;

  Box := Tgroupbox.Create(self);
  Box.Parent := self;
  Box.Align := alClient;
  Box.Caption := Fname;

  innerPanel := Tpanel.Create(box);
  innerpanel.Parent := box;
  innerpanel.Caption := '';
  innerpanel.BevelOuter := bvNone;
  innerpanel.Align := alClient;

  img := Timage.Create(innerPanel);
  img.Parent := innerPanel;
  img.Align := alClient;
  img.Proportional := true;
  img.OnMouseDown := panelMouseDown;
  img.OnMouseUp := panelmouseup;
end;

procedure TLinkedActionObj.PanelMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
begin
  innerpanel.BevelOuter := bvlowered;
end;

procedure TLinkedActionObj.PanelMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
begin
  innerpanel.BevelOuter := bvraised;
  clicking(sender,FID);
end;

end.
