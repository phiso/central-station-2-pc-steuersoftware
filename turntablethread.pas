unit turntablethread;

interface

uses
  Classes, turntablegraphics;

type
  TTurntableDrawer = class(TThread)
  private
    FGraph : TTurnTableGraph;
  protected
    procedure Execute; override;
  public
    constructor create(Owner : TComponent; rot : integer=270; animspeed : integer=100);
  end;

implementation

{ Important: Methods and properties of objects in visual components can only be
  used in a method called using Synchronize, for example,

      Synchronize(UpdateCaption);

  and UpdateCaption could look like,

    procedure TTurntableDrawer.UpdateCaption;
    begin
      Form1.Caption := 'Updated in a thread';
    end; }

{ TTurntableDrawer }

procedure TTurntableDrawer.Execute;
begin
  { Place thread code here }
end;

constructor TTurntableDrawer.create(Owner : TComponent; rot : integer=270; animspeed : integer=100);
begin
  inherited create(false);
  FGraph := TTurntablegraph.create(Owner,rot,animspeed);
end;

end.
