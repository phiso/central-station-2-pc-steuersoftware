unit package;

interface

uses SysUtils, typeHelper, RuleHandler, math, UDPListenThread, dialogs, classes;

type TPackageMode = (pmRcv, pmCreate);
type TPackage = class
    FMode : TpackageMode;
    FBuffStr : string;
    FBuffer : TBuff;
    FInBuffer : TBuffer;
    FAdress : integer;
    FAdrBits : TcharArray;
    ResponseListener : TUDPListener;
    RuleHandler : TRuleHandling;
  private
    function Calc2BitAdr(adr : integer):TCharArray;
    function Calc2BitSwitchAdr(adr : integer; decoder:TDecoder=dcMM2Shift):TcharArray;
  public
    property Buffer : TBuff read FBuffer;
    property InBuffer : Tbuffer read FInBuffer;

    constructor create(mode : TpackageMode);

    procedure Buildpackage(BuffInfo : TBuffer; adrMode:integer=0); overload;
    procedure Buildpackage(rule:string=''; index:integer=-1); overload;
    procedure SendPackage(stack : boolean);
    function ParseUserCmd(cmd : string):boolean;
end;

implementation

uses main;

constructor Tpackage.create(mode : TpackageMode);
var i : integer;
begin
  inherited create;
  Fmode := mode;
  setlength(FadrBits,2);
  
  case mode of
    pmCreate:begin
              // ResponseListener := TUDPListener.create(CSPORTRCV,false,100,form1);
               RuleHandler := TRuleHandling.create(form1.Rules);
             end;
    pmRcv:begin
            //No response listening needed
          end;
  end;
end;

function Tpackage.Calc2BitAdr(adr : integer):TCharArray;
var temp : integer;
begin
  setlength(Result,2);
  temp := floor(adr/255);
  if temp > 63 then begin
    Showmessage('Errorhandle:DCC Adress is to big');
  end;
  result[0] := char(192+temp);
end;

procedure Tpackage.Buildpackage(BuffInfo : TBuffer; adrMode:integer=0);
var i,n : integer;
begin
  FinBuffer := Buffinfo;
  n := 0;
  FBuffer[0] := #0;
  FBuffer[1] := char(BuffInfo.cmdbyte);
  Fbuffer[2] := #47;
  FBuffer[3] := #11;
  Fbuffer[4] := char(Buffinfo.DLCbyte);
  if Buffinfo.DLCbyte >= 4 then begin
    if adrMode = 0 then begin
      if Buffinfo.Adress <= 255 then begin
        Fbuffer[5] := #0;
        Fbuffer[6] := #0;
        Fbuffer[7] := #0;
        Fbuffer[8] := char(Buffinfo.Adress);
      end else begin
        Fbuffer[5] := #0;
        Fbuffer[6] := #0;
        Fbuffer[7] := Calc2BitAdr(Buffinfo.Adress)[0];
        Fbuffer[8] := Calc2BitAdr(Buffinfo.Adress)[1];
      end;
    end else begin
      Fbuffer[5] := #0;
      Fbuffer[6] := #0;
      Fbuffer[7] := Calc2BitSwitchAdr(BuffInfo.Adress,BuffInfo.decType)[0];
      Fbuffer[8] := Calc2BitSwitchAdr(BuffInfo.Adress,BuffInfo.decType)[1];
    end;
    case BuffInfo.DLCbyte of
      5:FBuffer[9] := char(BuffInfo.SingleVal);
      6:begin
          for i := 9 to 9+(BuffInfo.DLCbyte-5) do begin
            FBuffer[i] := char(BuffInfo.B2Val[n]);
            inc(n);
          end;
        end;
      7:begin
          for i := 9 to 9+(BuffInfo.DLCbyte-5) do begin
            FBuffer[i] := char(BuffInfo.B3Val[n]);
            inc(n);
          end;
        end;
      8:begin
          for i := 9 to 9+(BuffInfo.DLCbyte-5) do begin
            FBuffer[i] := char(BuffInfo.B4Val[n]);
            inc(n);
          end;
        end;
    end;
    for i := 4+BuffInfo.DLCbyte+1 to 12 do begin
      FBuffer[i] := #0;
    end;
  end else begin
    //Handling f�r Sonderbefehle...TODO  
  end;
end;

procedure Tpackage.Buildpackage(rule:string=''; index:integer=-1);
var srcRule : TRule;
    i : integer;
    buf : TStrArray;
begin
  if rule <> '' then SrcRule := Rulehandler.GetRule(rule)
  else if index <> -1 then SrcRule := RUleHandler.GetRule(index);

  explode(buf,' ',SrcRule.Buffer);
  for i := 0 to 12 do begin
    FBuffer[i] := char(strtoint(buf[i]));
  end;
end;

procedure Tpackage.SendPackage(stack : boolean);
begin
  if stack then MainUDPSender.ToStack(self)
  else MainUDPSender.Send(self);
//  MainUDPListener.listenforResponse(Fbuffer);
end;

function Tpackage.Calc2BitSwitchAdr(adr : integer; decoder:TDecoder=dcMM2Shift):TcharArray;
var temp : integer;
begin
  setlength(result,2);
  if (decoder = dcMM2Shift) or (decoder = dcMM2Prog) then begin
    if adr <= 256 then begin
      result[0] := char(48);
      result[1] := char(adr-1);
    end else if adr <= 320 then begin
      result[0] := char(49);
      result[1] := char(adr-257);
    end;
  end else if decoder = dcDCC then begin
    if adr <= 256 then begin
      result[0] := #56;
      result[1] := char(adr-1);
    end else begin
      temp := floor(adr/255);
      result[0] := char(56+temp);
      result[1] := char(adr-(temp*257));
    end;
  end else begin
    if adr <= 253 then begin
      result[0] := #64;
      result[1] := char(adr+2);
    end else begin
      //Keine Loks mit mfx adresse gr��er 253 vorhanden daher kann das Verhalten
      //nur vermutet werden
      temp := floor(adr/253);
      result[0] := char(64+temp);
      result[1] := char(adr-(temp*253));
    end;
  end;
end;

function Tpackage.ParseUserCmd(cmd : string):boolean;

  function getCmdParam(var cmd : string):string;
  var p : integer;
  begin
    p := pos('_',cmd);
    if p > 0 then result := copy(cmd,p+1,255)
    else result := '';
  end;

  function getCmd(cmd : string):string;
  var p : integer;
  begin
    p := pos('_',cmd);
    if p > 0 then result := copy(cmd,0,p-1)
    else result := cmd;
  end;

var i,p,adr,speed : integer;
    cmdStr,dectyp : string;
    CmdParts,cmds : TstrArray;
    decoder : TDecoder;
begin
  if cmd[1] = '\' then cmdStr := copy(cmd,2,255)
  else cmdstr := cmd;
  FBuffer[0] := #0;
  FBuffer[2] := #47;
  FBuffer[3] := #11;
  for i := 4 to 12 do FBuffer[i] := #0;
  cmdstr := lowercase(cmdstr);
  explode(CmdParts,' ',cmdstr);
  if length(cmdparts) <> 0 then begin
    {setlength(cmds,length(cmdparts));
    for i := 0 to length(cmdparts)-1 do begin
      cmds[i] := getcmd(cmdparts[i]);
    end;}
    if (getCmd(cmdparts[0]) = 'train') or (getCmd(cmdparts[0]) = 'zug') or (getCmd(cmdparts[0]) = 'lok') then begin
      if IsInt(getCmdParam(cmdparts[0])) then begin
        adr := strtoint(getcmdparam(cmdparts[0]));
        dectyp := lowercase(getCmd(cmdparts[1]));
        if (dectyp = '') or (dectyp = ' ') then begin
          dectyp := 'std';
        end;
        if dectyp = 'dcc' then decoder := dcDCC
        else if dectyp = 'mfx' then decoder := dcMFX
        else decoder := dcMM2Prog;
        if decoder = dcMM2prog then begin
          if adr <= 255 then begin
            Fbuffer[7] := #0;
            Fbuffer[8] := char(adr);
          end else begin
            showmessage('MM2 Dekoder lassen nur Adressen bis 255 zu!');
            result := false;
            exit;
          end;
        end else begin
          Fbuffer[7] := calc2Bitswitchadr(adr,decoder)[0];
          Fbuffer[8] := calc2Bitswitchadr(adr,decoder)[1];
        end;

      end else begin
        showmessage('Adresse erwartet aber keine Zahl gefunden!');
        result := false;
        exit;
      end;
      ///////////////Train Befehle///////////////////////////
      if (getcmd(cmdparts[2]) = 'speed') or (getcmd(cmdparts[2]) = 'setspeed') or (getcmd(cmdparts[2]) = 'geschwindigkeit') then begin
        FBuffer[1] := #8;
        Fbuffer[4] := #6;
        if isInt(getCmdparam(cmdparts[2])) then begin
          speed := strtoint(getcmdparam(cmdparts[2]));
          if speed > 1000 then speed := 1000;
          FBuffer[9] := char(calcspeedBytes(speed)[0]);
          FBuffer[10] := char(calcspeedbytes(speed)[1]);
        end else begin
          showmessage('�bergebene Geschwindigkeit wurde nicht als Zahl erkannt!');
          result := false;
          exit;
        end;
      end else if (getcmd(cmdparts[2]) = 'dir') or (getcmd(cmdparts[2]) = 'direction') or (getcmd(cmdparts[2]) = 'richtung') then begin
        FBuffer[1] := #10;
        FBuffer[4] := #5;
        if (getCmdparam(cmdparts[2]) = '1') or (getCmdparam(cmdparts[2]) = 'f') or (getCmdparam(cmdparts[2]) = 'forward') or (getCmdparam(cmdparts[2]) = 'vorw�rts') then begin
          FBuffer[9] := #1;
        end else if (getCmdparam(cmdparts[2]) = '0') or (getCmdparam(cmdparts[2]) = 'b') or (getCmdparam(cmdparts[2]) = 'backward') or (getCmdparam(cmdparts[2]) = 'r�ckw�rts') then begin
          Fbuffer[9] := #2;
        end else begin
          showmessage('Richtungsparameter nicht erkannt!');
          result := false;
          exit;
        end;
      end else if (getcmd(cmdparts[2]) = 'func') or (getcmd(cmdparts[2]) = 'function') or (getcmd(cmdparts[2]) = 'funktion') then begin
        Fbuffer[1] := #12;
        Fbuffer[4] := #6;
        if isInt(getcmdparam(cmdparts[2])) then begin
          Fbuffer[9] := char(strtoint(getcmdparam(cmdparts[2])));
          if (getcmd(cmdparts[3]) = '1') or (getcmd(cmdparts[3]) = 'activate') or (getcmd(cmdparts[3]) = 'active') then begin
            Fbuffer[10] := #1;
          end else if (getcmd(cmdparts[3]) = '0') or (getcmd(cmdparts[3]) = 'deactivate') or (getcmd(cmdparts[3]) = 'stop') then begin
            Fbuffer[10] := #0;
          end else begin
            showmessage('Funktionsstatus nicht erkannt!');
            result := false;
            exit;
          end;
        end else begin
          showmessage('Funktionsnummer nicht erkannt!');
          result := false;
          exit;
        end;
      end else if (getcmd(cmdparts[2]) = 'stop') or (getcmd(cmdparts[2]) = 'halt') or (getcmd(cmdparts[2]) = 'hold') then begin
        Fbuffer[1] := #0;
        Fbuffer[4] := #5;
        Fbuffer[9] := #3;
      end else if (cmdparts[2] = '') or (cmdparts[2] = ' ') then begin
        showmessage('Keine Operation f�r "'+getcmd(cmdparts[0])+'" �bergeben!');
        result := false;
        exit;
      end else begin
        showmessage('�bergebene Operation f�r "'+getcmd(cmdparts[0])+'" nicht erkannt!');
        result := false;
        exit;
      end;
    end else if (getCmd(cmdparts[0]) = 'weiche') or (getCmd(cmdparts[0]) = 'accessoire') then begin
    //////////Magenetartikel-Befehle///////////////////////////////////
      Fbuffer[1] := #22;
      Fbuffer[4] := #6;
      Fbuffer[10] := #1;
      if isInt(getcmdparam(cmdparts[0])) then begin
        adr := strtoint(getcmdparam(cmdparts[0]));
        FBuffer[7] := calc2BitSwitchAdr(adr,decoder)[0];
        FBuffer[8] := calc2BitSwitchAdr(adr,decoder)[1];
      end else begin
        showmessage('Magnetartikel-Adresse nicht erkannt!');
        result := false;
        exit;
      end;
      if isInt(getcmd(cmdparts[1])) then begin
        FBuffer[9] := char(strtoint(getcmd(cmdparts[1])));
      end else begin
        if (getcmd(cmdparts[1]) = 'l') or (getcmd(cmdparts[1]) = 'left') or (getcmd(cmdparts[1]) = 'links') or (getcmd(cmdparts[1]) = 'hp2') or (getcmd(cmdparts[1]) = 'gelb') or (getcmd(cmdparts[1]) = 'yellow') then FBuffer[9] := #10
        else if (getcmd(cmdparts[1]) = 'r') or (getcmd(cmdparts[1]) = 'right') or (getcmd(cmdparts[1]) = 'rechts') or (getcmd(cmdparts[1]) = 'off') or (getcmd(cmdparts[1]) = 'hp0') or (getcmd(cmdparts[1]) = 'rot') or (getcmd(cmdparts[1]) = 'red') then FBuffer[9] := #0
        else if (getcmd(cmdparts[1]) = 'on') or (getcmd(cmdparts[1]) = 'green') or (getcmd(cmdparts[1]) = 'gr�n') or (getcmd(cmdparts[1]) = 'straight') or (getcmd(cmdparts[1]) = 'gerade') or (getcmd(cmdparts[1]) = 'hp1') then Fbuffer[9] := #1
        else if (getcmd(cmdparts[1]) = 'white') or (getcmd(cmdparts[1]) = 'weis') or (getcmd(cmdparts[1]) = 'wei�') or (getcmd(cmdparts[1]) = 'sh0') then FBuffer := #11
        else begin
          showmessage('Stellung nicht erkannt!');
          result := false;
          exit;
        end;
      end;
    end else if (getCmd(cmdparts[0]) = 'system') or (getCmd(cmdparts[0]) = 'sys') or (getCmd(cmdparts[0]) = 'main') or (getCmd(cmdparts[0]) = 'global') then begin
    ///////////System-Befehle////////////////////////////
      FBuffer[1] := #0;
      FBuffer[4] := #5;
      if getCmdParam(cmdparts[0]) = '' then begin
        if cmdparts[1] = 'stop' then FBuffer[5] := #0
        else if (cmdparts[1] = 'halt') or ((cmdparts[1] = 'hold')) then FBuffer[9] := #2
        else if (cmdparts[1] = 'go') or (cmdparts[1] = 'start') then FBuffer[9] := #1
        else exit;
        MainUDPSender.Send(FBuffer);
      end else begin
        showmessage('Systembefehle ben�tigen keine Parameter!');
        result := false;
        exit;
      end;
    end;
  end else begin
    showmessage('Kein Befehl erkannt!');
    result := false;
    exit;
  end;
  result := true;
  MainUDPSender.Send(self);
end;

end.
