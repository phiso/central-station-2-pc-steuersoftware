object Form9: TForm9
  Left = 187
  Top = 132
  BorderStyle = bsToolWindow
  Caption = 'Neuer Magnetartikel'
  ClientHeight = 186
  ClientWidth = 275
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 275
    Height = 161
    Align = alClient
    Caption = 'Magnetartikel '
    TabOrder = 0
    object Label1: TLabel
      Left = 88
      Top = 16
      Width = 62
      Height = 13
      Caption = 'Decoder-Typ'
    end
    object Label2: TLabel
      Left = 8
      Top = 108
      Width = 18
      Height = 13
      Caption = 'Typ'
    end
    object Label3: TLabel
      Left = 185
      Top = 108
      Width = 80
      Height = 13
      Caption = 'Schaltzeit (msec)'
    end
    object Label4: TLabel
      Left = 8
      Top = 16
      Width = 38
      Height = 13
      Caption = 'Adresse'
    end
    object ComboBox1: TComboBox
      Left = 88
      Top = 32
      Width = 81
      Height = 21
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 0
      Text = 'MM2'
      Items.Strings = (
        'MM2'
        'DCC')
    end
    object ComboBoxEx1: TComboBoxEx
      Left = 8
      Top = 123
      Width = 161
      Height = 22
      ItemsEx = <
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end>
      Style = csExDropDownList
      ItemHeight = 16
      TabOrder = 1
      OnChange = ComboBoxEx1Change
      Images = ImageList1
      DropDownCount = 8
    end
    object LabeledEdit1: TLabeledEdit
      Left = 8
      Top = 76
      Width = 161
      Height = 21
      EditLabel.Width = 28
      EditLabel.Height = 13
      EditLabel.Caption = 'Name'
      TabOrder = 2
    end
    object SpinEdit1: TSpinEdit
      Left = 184
      Top = 123
      Width = 65
      Height = 22
      Increment = 50
      MaxValue = 5000
      MinValue = 0
      TabOrder = 3
      Value = 200
    end
    object SpinEdit2: TSpinEdit
      Left = 8
      Top = 32
      Width = 65
      Height = 22
      MaxValue = 2048
      MinValue = 1
      TabOrder = 4
      Value = 1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 161
    Width = 275
    Height = 25
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Button1: TButton
      Left = 160
      Top = 0
      Width = 115
      Height = 25
      Caption = 'Erstellen'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 0
      Top = 0
      Width = 115
      Height = 25
      Caption = 'Abbrechen'
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object ImageList1: TImageList
    Left = 192
    Top = 56
  end
end
