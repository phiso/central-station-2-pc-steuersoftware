unit digitCrane;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ImgList, traincontroller, Menus, ComCtrls,
  typehelper, math;


type
  TForm5 = class(TForm)
    MainMenu1: TMainMenu;
    Portalkran1: TMenuItem;
    GroupBox1: TGroupBox;
    TrackBar1: TTrackBar;
    Button2: TButton;
    Button1: TButton;
    Button3: TButton;
    StatusBar1: TStatusBar;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    TrackBar2: TTrackBar;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Button4: TButton;
    GroupBox4: TGroupBox;
    TrackBar3: TTrackBar;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    CheckBox1: TCheckBox;
    GroupBox5: TGroupBox;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    TrackBar4: TTrackBar;
    Button8: TButton;
    CheckBox2: TCheckBox;
    Button9: TButton;
    Einstellungen1: TMenuItem;
    Hilfe1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Button1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Button2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Button1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure TrackBar2Change(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure TrackBar2DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure BitBtn2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BitBtn1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BitBtn2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BitBtn1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure TrackBar3Change(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure Button7MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Button6MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Button6MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Button7MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure TrackBar4Change(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure BitBtn3MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BitBtn4MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BitBtn3MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BitBtn4MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Button9Click(Sender: TObject);
    procedure Button9MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Button9MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Einstellungen1Click(Sender: TObject);
    procedure astatursteuerung1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    CraneAdresses : array[0..1] of integer;
    BridgeControl,CraneControl : TTrainController;
    FisEnabled, hookfunction : boolean;
    Foldposition, FoldPosition2, activeFunction,
    bridgespeed,turnspeed,FOldPosition3,FoldPosition4,
    cranespeed,hookspeed : integer;

    procedure switchToFunction(f : integer);
//    procedure WMHotKey(var Msg : TWMHotKey);message WM_HOTKEY;
  public
    property isEnabled : boolean read FisEnabled;
    procedure applysettings;
//    procedure applyKeys;
    procedure boostControl(boost,key : integer);
  end;

var
  Form5: TForm5;

implementation

uses settings, hotkeyconfig, main;

{$R *.dfm}

procedure Tform5.applysettings;
begin
  CraneAdresses[0] := form10.SpinEdit3.Value;
  CraneAdresses[1] := form10.SpinEdit4.Value;
  bridgespeed := form10.SpinEdit6.Value;
  cranespeed := form10.SpinEdit7.Value;
  turnspeed := form10.SpinEdit8.Value;
  hookspeed := form10.SpinEdit9.Value;
  case form10.ComboBox1.ItemIndex of
    0:begin
        checkbox2.Enabled := false;
        button9.Enabled := false;
      end;
    1:begin
        checkbox2.Enabled := true;
        button9.Enabled := true;
        button9.Caption := 'Elektromagnet';
      end;
    2:begin
        checkbox2.Enabled := true;
        button9.Enabled := true;
        button9.Caption := 'Greifer';
      end;
  end;
  FisEnabled := form10.CheckBox8.Checked;
  BridgeControl := TTrainController.create(CraneAdresses[1],dcMM2Shift);
  CraneControl := TTrainController.create(CraneAdresses[0],dcMM2Shift);
end;

procedure TForm5.FormCreate(Sender: TObject);
begin
 Foldposition := 0;
 FOldPosition2 := 0;
 FOldPosition3 := 0;
 FOldPosition4 := 0;
 activeFunction := 0;
 bridgespeed := 100;
 turnspeed := 100;
 cranespeed := 100;
 hookspeed := 100;
 hookfunction := false;
end;

procedure TForm5.TrackBar1Change(Sender: TObject);
var speed : integer;
begin
  speed := abs(trackbar1.Position);
  if (trackbar1.Position > 0) and (FoldPosition <= 0) then BridgeControl.ForceForward
  else if (trackbar1.Position < 0) and (FoldPosition >=0) then BridgeControl.ForceBackward;
  BridgeControl.SetSpeed(speed);
  FOldPosition := trackbar1.Position;
  statusbar1.Panels[0].Text := 'Br�cke bewegen';
end;

procedure TForm5.Button3Click(Sender: TObject);
begin
  trackbar1.Position := 0;
  statusbar1.Panels[0].Text := 'gestoppt';
end;

procedure TForm5.Button2MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  BridgeControl.SetSpeed(0);
end;

procedure TForm5.Button1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  BridgeControl.SetSpeed(0);
end;

procedure TForm5.Button2MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  BridgeControl.ForceBackward;
  BridgeControl.SetSpeed(bridgespeed);
end;

procedure TForm5.Button1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  BridgeControl.ForceForward;
  BridgeControl.SetSpeed(bridgespeed);
end;

procedure TForm5.TrackBar2Change(Sender: TObject);
var speed : integer;
begin
  speed := abs(trackbar2.Position);
  if activeFunction <> 4 then switchToFunction(4);
  if (trackbar2.Position > 0) and (FoldPosition2 <= 0) then CraneControl.ForceForward
  else if (trackbar2.Position < 0) and (FoldPosition2 >=0) then CraneControl.ForceBackward;
  CraneControl.SetSpeed(speed);
  FOldPosition2 := trackbar2.Position;
  statusbar1.Panels[0].Text := 'Kranhaus drehen';
end;

procedure Tform5.switchToFunction(f : integer);
var i : integer;
begin
  for i := 0 to 4 do begin
    if  (i <> 2) and (i <> 0) then craneControl.DeactivateFunction(i);
  end;
  craneControl.ActivateFunction(f);
  activeFunction := f;
end;

procedure TForm5.Button4Click(Sender: TObject);
begin
   CraneControl.SetSpeed(0);
   trackbar3.Position := 0;
end;

procedure TForm5.TrackBar2DragDrop(Sender, Source: TObject; X, Y: Integer);
begin
  trackbar2.Position := 0;
end;

procedure TForm5.BitBtn2MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  craneControl.SetSpeed(0);
end;

procedure TForm5.BitBtn1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  craneControl.SetSpeed(0);
end;

procedure TForm5.BitBtn2MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if activeFunction <> 4 then switchToFunction(4);
  CraneControl.ForceForward;
  CraneControl.SetSpeed(turnspeed);
  statusbar1.Panels[0].Text := 'Kranhaus drehen';
end;

procedure TForm5.BitBtn1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if activeFunction <> 4 then switchToFunction(4);
  CraneControl.ForceBackward;
  CraneControl.SetSpeed(turnspeed);
  statusbar1.Panels[0].Text := 'Kranhaus drehen';
end;

procedure TForm5.TrackBar3Change(Sender: TObject);
var speed : integer;
begin
  speed := abs(trackbar3.Position);
  if activeFunction <> 1 then switchToFunction(1);
  if (trackbar3.Position > 0) and (FoldPosition3 <= 0) then CraneControl.ForceForward
  else if (trackbar3.Position < 0) and (FoldPosition3 >=0) then CraneControl.ForceBackward;
  CraneControl.SetSpeed(speed);
  FOldPosition3 := trackbar3.Position;
  statusbar1.Panels[0].Text := 'Kranhaus fahren';
end;

procedure TForm5.CheckBox1Click(Sender: TObject);
begin
  if checkbox1.Checked then CraneControl.ActivateFunction(2)
  else CraneControl.DeactivateFunction(2);
end;

procedure TForm5.Button7MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  cranecontrol.SetSpeed(0);
  trackbar3.Position := 0;
end;

procedure TForm5.Button6MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  cranecontrol.SetSpeed(0);
  trackbar3.Position := 0;
end;

procedure TForm5.Button6MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if activeFunction <> 1 then switchToFunction(1);
  CraneControl.ForceBackward;
  CraneControl.SetSpeed(cranespeed);
  statusbar1.Panels[0].Text := 'Kranhaus fahren';
end;

procedure TForm5.Button7MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if activeFunction <> 1 then switchToFunction(1);
  CraneControl.ForceForward;
  CraneControl.SetSpeed(cranespeed);
  statusbar1.Panels[0].Text := 'Kranhaus fahren';
end;

procedure TForm5.TrackBar4Change(Sender: TObject);
var speed : integer;
begin
  speed := abs(trackbar4.Position);
  if activeFunction <> 3 then switchToFunction(3);
  if (trackbar4.Position > 0) and (FoldPosition4 <= 0) then begin
    CraneControl.Forcebackward;
    statusbar1.Panels[0].Text := 'Kranhaken heben';
  end else if (trackbar4.Position < 0) and (FoldPosition4 >=0) then begin
    CraneControl.ForceForward;
    statusbar1.Panels[0].Text := 'Kranhaken senken';
  end;
  CraneControl.SetSpeed(speed);
  FOldPosition4 := trackbar4.Position;
end;
procedure TForm5.Button8Click(Sender: TObject);
begin
  craneControl.SetSpeed(0);
  trackbar4.Position := 0;
end;

procedure TForm5.BitBtn3MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  CraneControl.SetSpeed(0);
  trackbar4.Position := 0;
end;

procedure TForm5.BitBtn4MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  CraneControl.SetSpeed(0);
  trackbar4.Position := 0;
end;

procedure TForm5.BitBtn3MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if activeFunction <> 3 then switchToFunction(3);
  CraneControl.ForceForward;
  CraneControl.SetSpeed(hookspeed);
  statusbar1.Panels[0].Text := 'Kranhaken heben';
end;

procedure TForm5.BitBtn4MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if activeFunction <> 3 then switchToFunction(3);
  CraneControl.ForceBackward;
  CraneControl.SetSpeed(hookspeed);
  statusbar1.Panels[0].Text := 'Kranhaken senken';
end;

procedure TForm5.Button9Click(Sender: TObject);
begin
  if checkbox2.Checked then begin
    if not hookfunction then begin
      hookfunction := true;
      button9.Font.Style := [fsbold];
      CraneControl.ActivateFunction(0);
    end else begin
      hookfunction := false;
      button9.Font.Style := [];
      CraneControl.DeactivateFunction(0);
    end;
  end;
end;

procedure TForm5.Button9MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if not checkbox2.Checked then begin
    CraneControl.DeactivateFunction(0);
  end;
end;

procedure TForm5.Button9MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if not checkbox2.Checked then begin
    CraneControl.ActivateFunction(0);
  end;
end;

procedure TForm5.Einstellungen1Click(Sender: TObject);
begin
  form10.PageControl1.ActivePage := form10.TabSheet4;
  form10.ShowModal;
end;

procedure TForm5.astatursteuerung1Click(Sender: TObject);
begin
  form14.ShowModal;
end;

procedure TForm5.FormShow(Sender: TObject);
begin
  form1.applyKeys(0);
end;

procedure TForm5.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  form1.unregKeys(0);
end;

procedure Tform5.boostControl(boost,key : integer);
var oldbridge,oldcrane,oldturn,oldhook : integer;
begin
  oldbridge := bridgespeed;
  oldcrane := cranespeed;
  oldturn := turnspeed;
  oldhook := hookspeed;

  bridgespeed := 1000;
  turnspeed := 1000;
  hookspeed := 1000;
  cranespeed := 1000;

  case key of
    1:begin
        Button1MouseDown(self,mbleft,[],0,0);
        pause(50);
        Button1MouseUp(self,mbleft,[],0,0);
      end;
    2:begin
        Button2MouseDown(self,mbleft,[],0,0);
        pause(50);
        Button2MouseUp(self,mbleft,[],0,0);
      end;
    3:begin
        Button7MouseDown(self,mbleft,[],0,0);
        pause(50);
        Button7MouseUp(self,mbleft,[],0,0);
      end;
    4:begin
        Button6MouseDown(self,mbleft,[],0,0);
        pause(50);
        Button6MouseUp(self,mbleft,[],0,0);
      end;
    5:begin
        BitBtn2MouseDown(self,mbleft,[],0,0);
        pause(50);
        BitBtn2MouseUp(self,mbleft,[],0,0);
      end;
    6:begin
        BitBtn1MouseDown(self,mbleft,[],0,0);
        pause(50);
        BitBtn1MouseUp(self,mbleft,[],0,0);
      end;
    7:begin
        BitBtn3MouseDown(self,mbleft,[],0,0);
        pause(50);
        BitBtn3MouseUp(self,mbleft,[],0,0);
      end;
    8:begin
        BitBtn4MouseDown(self,mbleft,[],0,0);
        pause(50);
        BitBtn4MouseUp(self,mbleft,[],0,0);
      end;
  end;

  bridgespeed := oldbridge;
  cranespeed := oldcrane;
  turnspeed := oldturn;
  hookspeed := oldhook;
end;

end.
