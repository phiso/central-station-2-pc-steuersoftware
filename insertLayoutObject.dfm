object Form13: TForm13
  Left = 606
  Top = 319
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'Neues Layout-Objekt'
  ClientHeight = 118
  ClientWidth = 268
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 64
    Height = 13
    Caption = 'Magnetartikel'
  end
  object Button1: TButton
    Left = 32
    Top = 80
    Width = 113
    Height = 33
    Caption = 'Abbrechen'
    TabOrder = 0
  end
  object Button2: TButton
    Left = 152
    Top = 80
    Width = 113
    Height = 33
    Caption = 'Weiter'
    TabOrder = 1
  end
  object ComboBox1: TComboBox
    Left = 16
    Top = 32
    Width = 217
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 2
    Text = 'Neu anlegen...'
    Items.Strings = (
      'Neu anlegen...')
  end
end
