unit Zubehor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls,menus,shellapi,mmsystem,wininet,tlhelp32,
  winsock;

type Tdirection = (up,down,toleft,toright);
type Tdirections = set of Tdirection;


    procedure leftdown;
    procedure rightdown;
    procedure leftclick;
    procedure rightclick;
    procedure middledown;
    procedure middleclick;
    procedure doubleclick;
    procedure move(pos : Tpoint ; speed : integer);

  function pointtostr(point : Tpoint):string;
  function strtopoint(str : string):Tpoint;
  procedure simpleexecute(nfile : Tfilename);
  procedure texttowordlist(text : string ; out List : Tlistbox);
  procedure clearlist(var list : Tlistbox);
  function seeklist(sw : string; list : Tstrings):integer;
  procedure MakeScreenShot(const ATarget: TBitmap);
  function GetFileVersion(Path: string): string;
  function IsOnline: Boolean;
  function GetComputerName: String;
  function IsExeRunning(const AExeName: string): boolean;
  function KillTask(const AExeName: string): boolean;
  procedure GetProcessList(const aProcessList: TStrings);
  function getresolution:Tpoint;
  function ChangeResolution(sizex, sizey, bpp: DWORD): Boolean;
  function ExitWindows(const AFlag: Word): Boolean;
  procedure textout(text : string ; wait : integer);
  function GetLocalIPs(const Lines:TStrings):Boolean;
  procedure pause(length : integer);
  procedure dect(time : Tdatetime;sek : integer);
  procedure inct(time : Tdatetime;sek : integer);
  procedure getdesktop(x1,y1,x2,y2 : integer; dest : Tbitmap);
  {}

implementation

procedure leftdown;
begin
  mouse_event(MOUSEEVENTF_leftdown,0,0,0,0);
end;
procedure rightdown;
begin
  mouse_event(MOUSEEVENTF_rightdown,0,0,0,0);
end;
procedure leftclick;
begin
  mouse_event(MOUSEEVENTF_leftdown,0,0,0,0);
  mouse_event(MOUSEEVENTF_leftup,0,0,0,0);
end;
procedure rightclick;
begin
  mouse_event(MOUSEEVENTF_rightdown,0,0,0,0);
  mouse_event(MOUSEEVENTF_rightup,0,0,0,0);
end;
procedure middledown;
begin
  mouse_event(mouseeventf_middledown,0,0,0,0);
end;
procedure middleclick;
begin
  mouse_event(mouseeventf_middledown,0,0,0,0);
  mouse_event(mouseeventf_middleup,0,0,0,0);
end;
procedure doubleclick;
begin
  mouse_event(MOUSEEVENTF_leftdown,0,0,0,0);
  mouse_event(MOUSEEVENTF_leftup,0,0,0,0);
  mouse_event(MOUSEEVENTF_leftdown,0,0,0,0);
  mouse_event(MOUSEEVENTF_leftup,0,0,0,0);
end;

procedure move(pos : Tpoint ; speed : integer);
var temp,diff : Tpoint;
    ky : Tkeyboardstate;
begin
  temp := mouse.CursorPos;
  diff.X := pos.X-temp.X;
  diff.Y := pos.Y-temp.Y;

        if diff.X > 0 then begin
          repeat
            temp := mouse.CursorPos;
            inc(temp.X,1);
            sleep(speed);
            mouse.CursorPos := temp;
          until temp.X >= pos.X;
        end else begin
          repeat
            temp := mouse.CursorPos;
            dec(temp.X,1);
            sleep(speed);
            mouse.CursorPos := temp;
          until temp.X <= pos.X
        end;

        if diff.y > 0 then begin
          repeat
            temp := mouse.CursorPos;
            inc(temp.y,1);
            sleep(speed);
            mouse.CursorPos := temp;
          until temp.y >= pos.y;
        end else begin
          repeat
            temp := mouse.CursorPos;
            dec(temp.y,1);
            sleep(speed);
            mouse.CursorPos := temp;
          until temp.y <= pos.y
        end;
end;

procedure textout(text : string ; wait : integer);
var key : longword;
    i : integer;
begin
  for i := i to length(text) do begin
    key := loword(vkkeyscan(text[i]));
    keybd_event(key,0,0,0);
    keybd_event(key,0,keyeventf_keyup,0);
    sleep(wait);
  end;
end;

function pointtostr(point : Tpoint):string;
begin
   result := 'X: '+inttostr(point.X)+' Y: '+inttostr(point.Y);
end;

function strtopoint(str : string):Tpoint;
var tempx,tempy : string;
    l,i,j : integer;
begin
  l := length(str);
  for i := 0 to round(l/2) do begin
    if (ord(str[i]) >= 48) and (ord(str[i]) <= 57) then tempx := tempx + str[i];
  end;
  for j := round(l/2)+1 to l do begin
    if (ord(str[j]) >= 48) and (ord(str[j]) <= 57) then tempy := tempy + str[j];
  end;
  result := point(strtoint(tempx),strtoint(tempy));
end;

procedure simpleexecute(nfile : Tfilename);
begin
  shellexecute(0,'open',Pchar(nfile),nil,nil,SW_SHOW);
end;

procedure MakeScreenShot(const ATarget: TBitmap);
var
  DesktopDC: HDC;
begin
  DesktopDC := CreateDC('DISPLAY', nil, nil, nil);
  try
    ATarget.PixelFormat := pfDevice;
    ATarget.Width := Screen.Width;
    ATarget.Height := Screen.Height;
 
    BitBlt(ATarget.Canvas.Handle, 0, 0, Screen.Width, Screen.Height, DesktopDC, 0, 0, SRCCOPY);
  finally
    DeleteDC(DesktopDC);
  end;
end;

function GetFileVersion(Path: string): string;
var
  lpVerInfo: pointer;
  rVerValue: PVSFixedFileInfo;
  dwInfoSize: cardinal;
  dwValueSize: cardinal;
  dwDummy: cardinal;
  lpstrPath: pchar;

begin
  if Trim(Path) = EmptyStr then
    lpstrPath := pchar(ParamStr(0))
  else
    lpstrPath := pchar(Path);

  dwInfoSize := GetFileVersionInfoSize(lpstrPath, dwDummy);

  if dwInfoSize = 0 then
  begin
    Result := 'No version specification';
    Exit;
  end;

  GetMem(lpVerInfo, dwInfoSize);
  GetFileVersionInfo(lpstrPath, 0, dwInfoSize, lpVerInfo);
  VerQueryValue(lpVerInfo, '\', pointer(rVerValue), dwValueSize);

  with rVerValue^ do
  begin
    Result := IntTostr(dwFileVersionMS shr 16);
    Result := Result + '.' + IntTostr(dwFileVersionMS and $FFFF);
    Result := Result + '.' + IntTostr(dwFileVersionLS shr 16);
    Result := Result + '.' + IntTostr(dwFileVersionLS and $FFFF);
  end;
  FreeMem(lpVerInfo, dwInfoSize);

end;

function IsOnline: Boolean;
var
  dlvFlag : DWord;
begin
  Result := False;
  dlvFlag := Internet_Connection_Modem or Internet_Connection_Lan or Internet_Connection_Proxy;
  if InternetGetConnectedState(@dlvFlag, 0) then
    Result := (dlvFlag = 81);
end;

function GetComputerName: String;
var
  Len: DWORD;
begin
  Len:=MAX_COMPUTERNAME_LENGTH+1;
  SetLength(Result,Len);
  if Windows.GetComputerName(PChar(Result), Len) then
    SetLength(Result,Len)
  else
    RaiseLastOSError;
end;

function IsExeRunning(const AExeName: string): boolean;
var
  h: THandle;
  p: TProcessEntry32;
begin
  Result := False;

  p.dwSize := SizeOf(p);
  h := CreateToolHelp32Snapshot(TH32CS_SnapProcess, 0);
  try
    Process32First(h, p);
    repeat
      Result := AnsiUpperCase(AExeName) = AnsiUpperCase(p.szExeFile);
    until Result or (not Process32Next(h, p));
  finally
    CloseHandle(h);
  end;
end;

function KillTask(const AExeName: string): boolean;
var
  p: TProcessEntry32;
  h: THandle;
begin
  Result := false;
  p.dwSize := SizeOf(p);
  h := CreateToolHelp32Snapshot(TH32CS_SnapProcess, 0);
  try
    if Process32First(h, p) then
      repeat
        if AnsiLowerCase(p.szExeFile) = AnsiLowerCase(AExeName) then
          Result := TerminateProcess(OpenProcess(Process_Terminate,
                                                 false,
                                                 p.th32ProcessID),
                                     0);
      until (not Process32Next(h, p)) or Result;
  finally
    CloseHandle(h);
  end;
end;

procedure GetProcessList(const aProcessList: TStrings);
var
  Snap: THandle;
  ProcessE: TProcessEntry32;
begin
  aProcessList.Clear;
  Snap := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
  try
    ProcessE.dwSize := SizeOf(ProcessE);
    if Process32First(Snap, ProcessE) then
      Repeat
        aProcessList.Add(ProcessE.szExeFile);
      Until not Process32Next(Snap, ProcessE)
    else
      RaiseLastOSError;
  finally
    CloseHandle(Snap);
  end;
end;

function ChangeResolution(sizex, sizey, bpp: DWORD): Boolean;
var
  DeviceMode: TDeviceModeA;
  i: Integer;
begin
  i := 0;
  Result := False;
  while EnumDisplaySettings(nil, i, DeviceMode) do begin
    with DeviceMode do
      if (dmPelsWidth = sizex) and
        (dmPelsHeight = SizeY) and
        (dmBitsPerPel = bpp) then begin
        // erst testen, bevor wir umschalten!
        case ChangeDisplaySettings(DeviceMode, CDS_TEST) of
          // es wird klappen!
          DISP_CHANGE_SUCCESSFUL:
            Result := True;

          DISP_CHANGE_RESTART:
            Showmessage('Neustart erforderlich');

          DISP_CHANGE_BADFLAGS:
            Showmessage('Ung�ltige Bildschirmeinstellungen');

          DISP_CHANGE_FAILED:
            Showmessage('Aufl�sung konnte nicht ge�ndert werden');

          DISP_CHANGE_BADMODE:
            Showmessage('Bildschirm unterst�tzt diese Aufl�sung nicht');

          // Nur Windows NT
          DISP_CHANGE_NOTUPDATED:
            Showmessage('Registry konnte nicht aktualisiert werden');

        else
          Result := True;
        end;

        if Result then
          //jetzt wird umgeschaltet
          ChangeDisplaySettings(DeviceMode, CDS_FULLSCREEN)
      end;
    Inc(i);
  end;
end;

function getresolution:Tpoint;
begin
  result := point(screen.Width,screen.Height);
end;

function ExitWindows(const AFlag: Word): Boolean;
{M�gliche Werte f�r den Parameter Flag:

    * EWX_Logoff - Loggt den User aus
    * EWX_Poweroff - Unterst�tzt der Rechner, dieses Feature, wird der Computer nach dem Herunterfahren ausgeschaltet.
    * EWX_Reboot - Computer wird neu gestartet
    * EWX_Shutdown - Der Computer wird normal herruntergefahren
}
var
  vi     : TOSVersionInfo;
  hToken : THandle;
  tp     : TTokenPrivileges;
  h      : DWord;
begin
  result:= false;

  vi.dwOSVersionInfoSize:=SizeOf(vi);

  if GetVersionEx(vi) then
  begin
    if vi.dwPlatformId = VER_PLATFORM_WIN32_NT then
    begin
      // Windows NT
      // Achtung bei Delphi 2 mu� @hToken stehen ...
      if OpenProcessToken(GetCurrentProcess,TOKEN_ADJUST_PRIVILEGES,hToken) then
      begin
        LookupPrivilegeValue(nil,'SeShutdownPrivilege',tp.Privileges[0].Luid);
        tp.PrivilegeCount := 1;
        tp.Privileges[0].Attributes := SE_PRIVILEGE_ENABLED;
        h := 0;
        AdjustTokenPrivileges(hToken,False,tp,0,PTokenPrivileges(nil)^, h);
        CloseHandle(hToken);
        result := ExitWindowsEx(Aflag, 0);
      end;
    end
    else
    begin // Windows 95
      Result := ExitWindowsEx(Aflag, 0);
    end;
  end;
end;

procedure texttowordlist(text : string ; out List : Tlistbox);
var wort : string;
    i : integer;
begin
  wort := '';
  for i := 0 to length(text) do begin
    if not (text[i] = ' ') then begin
      wort := wort + text[i];
    end else begin
      list.Items.Add(wort);
      wort := '';
    end;
  end;
end;

procedure clearlist(var list : Tlistbox);
var i : integer;
begin
  i := 0;
  repeat
    if (list.Items[i] = '') or (list.Items[i] = ' ') then begin
      list.Items.Delete(i);
    end;
    inc(i);
  until i >= list.Items.Count-1;
end;

function seeklist(sw : string; list : Tstrings): integer;
var i : integer;
begin
  for i := 0 to list.Count-1 do begin
    if list[i] = sw then begin
      result := i;
      break;
    end;
  end;
end;

function GetLocalIPs(const Lines:TStrings):Boolean;
type
  PPInAddr= ^PInAddr;
var
  wsaData: TWSAData;
  HostInfo: PHostEnt;
  HostName: Array[0..255] of Char;
  Addr: PPInAddr;
begin
  Result:=False;
  Lines.Clear;
  if WSAStartup($0102, wsaData)=0 then
  try
    if gethostname(HostName, SizeOf(HostName)) = 0 then Begin
       HostInfo:= gethostbyname(HostName);
       if HostInfo<>nil then Begin
          Addr:=Pointer(HostInfo^.h_addr_list);
          if (Addr<>nil) AND (Addr^<>nil) then
             Repeat
                    Lines.Add(StrPas(inet_ntoa(Addr^^)));
                    inc(Addr);
             Until Addr^=nil;
       end;
    end;
    Result:=True;
  finally
    WSACleanup;
  end;
end;

procedure pause(length : integer);
var start,stop : longint;
begin
start := gettickcount;
repeat
  stop := gettickcount;
  application.ProcessMessages;
until (stop-start) >= length;
end;

procedure dect(time : Tdatetime; sek : integer);
var std,min,seku,temp : integer;
begin
  if sek > 60 then begin
    seku := sek mod 60;
    temp := sek div 60;
    min := temp mod 60;
    std := temp div 60;
  end else seku := sek;
  time := time - strtotime(inttostr(std)+':'+inttostr(min)+':'+inttostr(seku));
end;

procedure inct(time : Tdatetime; sek : integer);
var std,min,seku,temp : integer;
begin
  if sek > 60 then begin
    seku := sek mod 60;
    temp := sek div 60;
    min := temp mod 60;
    std := temp div 60;
  end else seku := sek;
  time := time + strtotime(inttostr(std)+':'+inttostr(min)+':'+inttostr(seku));
end;

procedure getdesktop(x1,y1,x2,y2 : integer; dest : Tbitmap);
var
  DesktopDC: HDC;
begin
  DesktopDC := CreateDC('DISPLAY', nil, nil, nil);
  try
    dest.PixelFormat := pfDevice;
    dest.Width := x2-x1;
    dest.Height := y1-y1;

    BitBlt(dest.Canvas.Handle, 0, 0, x2, y1, DesktopDC, x1, y1, SRCCOPY);
  finally
    DeleteDC(DesktopDC);
  end;
end;

end.
