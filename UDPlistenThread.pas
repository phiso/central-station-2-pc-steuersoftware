unit UDPlistenThread;

interface

uses
  Classes, IdBaseComponent, IdComponent, IdUDPBase, IdUDPServer, IdSocketHandle,
  Sysutils, typehelper, StdCtrls, dialogs, rcvparser, comctrls, controlListenThread,
  eventlistener;

type
  TUDPListener = class(TThread)
    FUDPServer : TidUDPServer;
    Fport, FStoCount : integer;
    FLastRcv,FResponse,FCheckResponse : TBuff;
    FStoredRcvs : TBuffArray;
    Flog,FStopped,FListenForResponse,
    Ffirst,Flastdata : boolean;
    FLogMsg : string;
    FLogtarget : Trichedit;
    parser : TBuffparser;
    ControlListener : array of TControlListener;
    Eventlistener : array of TEVentListener;
  private
    procedure ServerUDPRead(Sender: TObject; AData: TStream;
      ABinding: TIdSocketHandle);
    procedure updateStorageList;
  protected
    procedure Execute; override;
  public
    FDataStreamSave : TBuffArray;
    property Buffer : TBuff read FLastRCV;
    property ResponseBuff : TBuff read FResponse;
    property Stopped : Boolean read FStopped;
    property Listening : Boolean read FlistenForResponse;

    constructor create(port : integer; log : boolean;
      sto : integer; owner : TComponent);

    procedure SetLog(dest : Trichedit);
    function getStoredRcv(nr : integer):TBuff;
    procedure stopLog;
    procedure StartLog;
    procedure listenforResponse(send : TBuff);
    function IsStopped:Boolean;
//    procedure decomp;
    procedure addListener(cmd : TCmdInst; Sender : Tobject);overload;
    procedure addListener(str : string; sender : Tobject);overload;
    procedure addEvtListener(listener : TEventListener);
    procedure stopListener(cmd : TCmdInst);
    procedure resumeListener(cmd : TCmdInst);
    procedure listenForIP;
  end;

implementation

uses main;

{ Important: Methods and properties of objects in visual components can only be
  used in a method called using Synchronize, for example,

      Synchronize(UpdateCaption);

  and UpdateCaption could look like,

    procedure TUDPListener.UpdateCaption;
    begin
      Form1.Caption := 'Updated in a thread';
    end; }

{ TUDPListener }

procedure TUDPListener.Execute;
begin
  { Place thread code here }
end;

constructor TUDPListener.create(port : integer; log : boolean;
      sto : integer; owner : TComponent);
begin
  inherited create(false);
  FFirst := true;
  Flog := log;
  FStoCount:= sto;
  FStopped := false;
  Flastdata := false;
  FListenForResponse := false;
  FUDPServer := TidUDPServer.Create(Owner);
  FUDPServer.DefaultPort := port;
  FUDPServer.Active := true;
  FUDPServer.OnUDPRead := ServerUDPRead;
  parser := TBuffparser.create;
end;

function TUDPListener.IsStopped:Boolean;
begin
  if (BuffToStr(Flastrcv) = '0 0 3 0 6 67 83 207 74 48 1 0 0') or
     (BuffToStr(Flastrcv) = '0 0 3 0 6 67 83 207 74 48 0 0 0') then
      result := false
      else if (BuffToStr(Flastrcv) = '0 54 99 24 0 67 83 207 74 2 31 0 0') or
              (BuffToStr(Flastrcv) = '0 48 99 24 0 67 83 207 74 2 31 0 0') or
              (BuffToStr(Flastrcv) = '0 49 27 12 8 67 83 207 74 2 31 0 0') then
                result := true;
end;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
procedure TUDPListener.ServerUDPRead(Sender: TObject; AData: TStream;
      ABinding: TIdSocketHandle);
var i : integer;
    tempstr : string;
begin
  tempstr := '';
  Adata.Read(FlastRcv,13);
{  if Flastdata then begin
    testdecomp := TdecompressThread.create(FdataStreamSave,form1.Default_SystemPath+'data\test.cs2');
    FlastData := false;
  end;}

  if FListenForResponse then begin
    Fresponse := FLastRCV;
    {if ord(FResponse[1]) = ord(FCheckresponse[1])+1 then begin  //positive Response
    end; }
    FListenForresponse := false;
  end;

  if FFirst and form1.autoip then begin
    form1.isIPManual := false;
    form1.csIP := Abinding.PeerIP;
    FFirst := false;
    MainUDPSender.changeHost(form1.csIP);
  end;

  updateStorageList;
  for i := 0 to 12 do begin
    tempstr := tempstr + ' '+inttostr(ord(Flastrcv[i]));
  end;
  FlogMsg := datetimetostr(now)+'->'+'Recieving from:'+Abinding.PeerIP+
             ' : '+tempstr;

  for i := 0 to length(ControlListener)-1 do begin
    ControlListener[i].Listen(FlastRcv);
  end;

  for i := 0 to length(Eventlistener)-1 do begin
//    Eventlistener[i].listen(Flastrcv);
  end;

  if Flog then begin
    Flogtarget.Lines.Add(FlogMsg);
  end;
  parser.ParseBuff(Flastrcv,FLog);
  if parser.Cmd in [ciConfDataStream] then begin
//    if FlastRcv[12] = #0 then FlastData := true;
    setlength(FdataStreamSave,length(FdataStreamSave)+1);
    FdataStreamSave[length(FdatastreamSave)-1] := FlastRcv;
    Flogtarget.Lines.Add('saving stream');

  end;
end;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

{procedure TUDPListener.decomp;
var testdecomp : TDecompressthread;
begin
  testdecomp := TdecompressThread.create(FdataStreamSave,form1.Default_SystemPath+'data\test.zip');
end;}

procedure TUDPListener.updateStorageList;
var i : integer;
begin
  if length(FStoredRcvs) < FStoCount then begin
    setlength(FstoredRcvs,length(Fstoredrcvs)+1);
    FstoredRcvs[length(FstoredRcvs)-1] := FlastRcv;
  end else begin
    for i := 1 to length(FstoredRcvs)-1 do begin
      FstoredRcvs[i-1] := FstoredRcvs[i];
    end;
    FstoredRcvs[length(FstoredRcvs)-1] := FlastRcv;
  end;
end;

procedure TUDPListener.SetLog(dest : Trichedit);
begin
  FLogtarget := dest;
end;

function TUDPListener.getStoredRcv(nr : integer):TBuff;
begin
  result := FstoredRcvs[nr];
end;

procedure TUDPlistener.stopLog;
begin
  Flog := false;
end;

procedure TUDPListener.StartLog;
begin
  Flog := true;
end;

procedure TUDPlistener.listenforResponse(Send: Tbuff);
begin
  FlistenForResponse := true;
  FCheckResponse := send;
end;

procedure TUDPListener.addListener(cmd : TCmdInst; Sender : Tobject);
begin
  setlength(ControlListener,length(ControlListener)+1);
  ControlListener[length(ControlListener)-1] := TControlListener.create(cmd,Sender);
end;

procedure TUDPListener.addListener(str : string; sender : Tobject);
begin
  setlength(ControlListener,length(ControlListener)+1);
  ControlListener[length(ControlListener)-1] := TControlListener.create(str);
  ControlListener[length(ControlListener)-1].sender := Sender;
end;

procedure TUDPListener.addEvtListener(listener : TEventListener);
begin
  setlength(Eventlistener,length(Eventlistener)+1);
  Eventlistener[length(Eventlistener)-1] := listener;
end;

procedure TUDPlistener.stopListener(cmd : TCmdInst);
var n : integer;
begin
  for n := 0 to length(Controllistener)-1 do begin
    if Controllistener[n].Cmd = cmd then begin
      Controllistener[n].stopListening;
    end;
  end;
end;

procedure TUDPlistener.resumeListener(cmd : TCmdInst);
var n : integer;
begin
  for n := 0 to length(Controllistener)-1 do begin
    if Controllistener[n].Cmd = cmd then begin
      Controllistener[n].startListening;
    end;
  end;
end;

procedure TUDPListener.listenForIP;
begin
  FFirst := true;
end;

end.
