unit keyboard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, typehelper, package, control, StdCtrls, Spin, ExtCtrls, cs2File,
  articlecontrol, math, layoutobject, Menus, inifiles;

type
  TForm3 = class(TForm)
    GroupBox1: TGroupBox;
    SpinEdit1: TSpinEdit;
    ComboBox1: TComboBox;
    GroupBox3: TGroupBox;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    OpenDialog1: TOpenDialog;
    Button5: TButton;
    Label1: TLabel;
    Label2: TLabel;
    ScrollBox1: TScrollBox;
    MainMenu1: TMainMenu;
    Magnetartikel1: TMenuItem;
    Neu1: TMenuItem;
    Neuladen1: TMenuItem;
    Importieren1: TMenuItem;
    cs2Datei1: TMenuItem;
    iniDatei1: TMenuItem;
    procedure ComboBox1Change(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure SpinEdit1Enter(Sender: TObject);
    procedure SpinEdit1Exit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Neu1Click(Sender: TObject);
    procedure Neuladen1Click(Sender: TObject);
  private
    KeyBoardControl : TControl;
    Decoder : Tdecoder;
    FRows : integer;
    Article : TArticleArray;
    Objects : array of TArticlePanel;
//    TestArticle : TarticlePanel;
    cs2File : Tcs2File;
    isIntegrated : boolean;
    
    procedure InitVars;
//    procedure createPanels;
  public
    function getArticle(nr : integer):TArticle;
    function GetArticleCount:integer;
    procedure LoadArticlesFromFile(f : Tcs2File);
    procedure NewArticle(art : Tarticle);
    procedure saveToIni(f : string);
    procedure LoadFromIni(f : string);
    function GetObjectAdr(nr : integer):integer;
    function GetObjectState(nr : integer):integer;
    function GetObjectCount:integer;
    procedure StartExtCallOn(nr : integer);
    procedure StopExtCallOn(nr : integer);
    procedure ChangeStateOn(nr,s : integer);
    function GetNextAdrOn(nr : integer):integer;
    procedure openNewKeyboard(f : string);
    procedure applysettings;
  end;

var
  Form3: TForm3;

implementation

uses main, newArticle, settings;

{$R *.dfm}

procedure Tform3.applysettings;
begin
  scrollbox1.Width := form10.SpinEdit1.Value;
  isIntegrated := form10.CheckBox15.Checked;
  if isIntegrated then begin
    self.Parent := form1.TabSheet2;
    self.Width := form1.TabSheet2.ClientWidth;
    self.Height := form1.TabSheet2.ClientHeight-10;
    self.Top := 0;
    self.Left := 0;
    self.AutoSize := true;
    self.Show;
  end;
end;

procedure Tform3.InitVars;
begin
  Decoder := dcMM2Shift;
  doublebuffered := true;
  groupbox1.DoubleBuffered := true;
  scrollbox1.DoubleBuffered := true;
  FRows := 5;
  isIntegrated := true;
end;

procedure TForm3.ComboBox1Change(Sender: TObject);
begin
  case combobox1.ItemIndex of
    0:Decoder := dcMM2Shift;
    1:Decoder := dcDCC;
  end;
end;

procedure TForm3.Button1Click(Sender: TObject);
begin
  KeyboardControl.ShiftSwitch(16);
end;

procedure TForm3.Button2Click(Sender: TObject);
begin
  Keyboardcontrol.ShiftSwitch(1);
end;

procedure TForm3.Button3Click(Sender: TObject);
begin
  KeyboardControl.ShiftSwitch(0);
end;

procedure TForm3.SpinEdit1Enter(Sender: TObject);
begin
  spinedit1.SelectAll;
end;

procedure TForm3.SpinEdit1Exit(Sender: TObject);
begin
  if assigned(keyboardControl) then KEyBoardControl.Free;
  KeyboardControl := Tcontrol.create(spinedit1.Value,decoder);
end;

procedure TForm3.FormCreate(Sender: TObject);
begin
//  SetWindowLong(Edit1.Handle, GWL_STYLE, ES_CENTER);
  initVars;

end;

procedure Tform3.LoadArticlesFromFile(f : Tcs2File);
var i : integer;
    tempart : TArticle;
begin
  for i := 0 to f.entries-1 do begin
    tempart := f.GetArticle(i);
    if tempart.Typ <> 'std_rot_gruen' then begin
      setlength(Article,length(Article)+1);
      Article[length(article)-1] := tempart;
    end;
  end;
end;

procedure TForm3.Button4Click(Sender: TObject);
var i,j,n : integer;
begin
  n := 0;
  opendialog1.InitialDir := form1.Default_SystemPath+'data';
  if opendialog1.Execute then begin
    cs2File := Tcs2File.create(opendialog1.FileName,cfpArticles,cfsOpenRead);
    loadarticlesfromfile(cs2File);
    setlength(Objects,length(article));
    for i := 0 to floor(length(article)/Frows)-1 do begin
      for j := 0 to Frows-1 do begin
        Objects[i] := TArticlePanel.create(Scrollbox1,article[n],point(i,j));
        inc(n);
      end;
    end;
    for i := 1 to (length(article) mod FRows) do begin
      Objects[length(article)-i] := TArticlePanel.create(Scrollbox1,article[length(article)-i],point(floor(length(article)/Frows),i-1));
    end;

  end
end;

procedure TForm3.Button5Click(Sender: TObject);
begin
  Keyboardcontrol.ShiftSwitch(17);
end;

procedure Tform3.NewArticle(art : Tarticle);
var posi : Tpoint;
begin
  setlength(Article,length(Article)+1);
  setlength(Objects,length(Objects)+1);
  Article[length(Article)-1] := art;
  posi := point(floor(length(article)/Frows),(length(article) mod FRows)-1);
  Objects[length(Objects)-1] := TArticlepanel.create(scrollbox1,art,posi);
end;

procedure TForm3.Neu1Click(Sender: TObject);
begin
  form9.new(self);
end;

procedure Tform3.saveToIni(f : string);
var ini : Tinifile;
    i : integer;
begin
  ini := Tinifile.Create(f);
  try
    ini.WriteInteger('INFO','objects',length(Objects));

    for i := 0 to length(Objects)-1 do begin
      ini.WriteInteger('Object_'+inttostr(i),'Address',Objects[i].Article.Adr);
      ini.WriteString('Object_'+inttostr(i),'name',Objects[i].Article.name);
      ini.WriteInteger('Object_'+inttostr(i),'state',Objects[i].Article.State);
      ini.WriteInteger('Object_'+inttostr(i),'shifttime',Objects[i].Article.shiftTime);
      ini.WriteString('Object_'+inttostr(i),'typ',Objects[i].Article.Typ);
      ini.WriteString('Object_'+inttostr(i),'decoder',decoderToStr(Objects[i].Article.dectype));
    end;
  finally
    ini.Free;
  end;
end;

procedure Tform3.LoadFromIni(f : string);
var ini : Tinifile;
    tempArticle : TArticle;
    i,n : integer;
begin
  ini := Tinifile.Create(f);
  try
    n := ini.ReadInteger('INFO','objects',0);
    if n > 0 then begin
      for i := 0 to n-1 do begin
      end;
    end;
  finally
    ini.Free;
  end;
end;

function Tform3.GetObjectAdr(nr : integer):integer;
begin
  if Assigned(Objects[nr]) then begin
    result := Objects[nr].Article.Adr;
  end;
end;

function Tform3.GetObjectCount:integer;
begin
  result := length(Objects);
end;

function Tform3.GetObjectState(nr : integer):integer;
begin
  result := Objects[nr].State;
end;

procedure Tform3.StartExtCallOn(nr : integer);
begin
  Objects[nr].StartExternalCall;
end;

procedure TForm3.StopExtCallOn(nr : integer);
begin
  Objects[nr].StopExternalCall;
end;

procedure Tform3.ChangeStateOn(nr,s : integer);
begin
  Objects[nr].changestate(s);
end;

function Tform3.GetNextAdrOn(nr : integer):integer;
begin
  result := Objects[nr].NextAdr;
end;

procedure Tform3.openNewKeyboard(f : string);
var i,j,n : integer;
begin
  n := 0;
  cs2File := Tcs2File.create(f,cfpArticles,cfsOpenRead);
  loadarticlesfromfile(cs2File);
  setlength(Objects,length(article));
  form1.ProgressBar1.Max := floor(length(article)/Frows);
  for i := 0 to floor(length(article)/Frows)-1 do begin
    for j := 0 to Frows-1 do begin
      Objects[i] := TArticlePanel.create(Scrollbox1,article[n],point(i,j));
      inc(n);
      form1.ProgressBar1.Position := n;
    end;
  end;
  for i := 1 to (length(article) mod FRows) do begin
    Objects[length(article)-i] := TArticlePanel.create(Scrollbox1,article[length(article)-i],point(floor(length(article)/Frows),i-1));
  end;
 MainUDPListener.addListener(ciShiftAccess,self);
end;

procedure TForm3.Neuladen1Click(Sender: TObject);
var i,j,n : integer;
begin
  n := 0;
  opendialog1.InitialDir := form1.Default_SystemPath+'data';
  if opendialog1.Execute then begin
    cs2File := Tcs2File.create(opendialog1.FileName,cfpArticles,cfsOpenRead);
    loadarticlesfromfile(cs2File);
    setlength(Objects,length(article));
    for i := 0 to floor(length(article)/Frows)-1 do begin
      for j := 0 to Frows-1 do begin
        Objects[i] := TArticlePanel.create(Scrollbox1,article[n],point(i,j));
        inc(n);
      end;
    end;
    for i := 1 to (length(article) mod FRows) do begin
      Objects[length(article)-i] := TArticlePanel.create(Scrollbox1,article[length(article)-i],point(floor(length(article)/Frows),i-1));
    end;
  end;
  MainUDPListener.addListener(ciShiftAccess,self);
  show;
end;

function Tform3.getArticle(nr : integer):TArticle;
begin
  result := Article[nr];
end;

function TForm3.GetArticleCount:integer;
begin
  result := length(Article);
end;


end.
