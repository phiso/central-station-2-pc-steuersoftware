unit helpdisplay;

interface

uses typehelper, sysutils, Classes, shellapi, forms, zubehor;

type THelpDisplay = class
  private
    FDisplay : Tcomponent;
    FDefDir : string;
  public
    constructor create(display : TComponent);overload;
    constructor create;overload;

    procedure DisplayFile(f : string; isPath : boolean);
end;

implementation

constructor ThelpDisplay.create;
begin
  inherited create;
  FDefDir := extractfilepath(application.ExeName)+'doc\hlp\';
end;

constructor ThelpDisplay.create(display : TComponent);
begin
  create;
  Fdisplay := display;
end;

procedure ThelpDisplay.DisplayFile(f : string; isPath : boolean);
begin
  if not isPath then begin
    if FileExists(FDefDir+f) then
      simpleexecute(FDefDir+f);
  end else begin
    if Fileexists(f) then
      simpleexecute(f);
  end;
end;

end.
