unit newlinking;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, linking;

type
  TForm17 = class(TForm)
    ComboBox1: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    SpinEdit1: TSpinEdit;
    Label3: TLabel;
    ComboBox2: TComboBox;
    Label4: TLabel;
    ComboBox3: TComboBox;
    Label5: TLabel;
    SpinEdit2: TSpinEdit;
    Button1: TButton;
    Button2: TButton;
    ComboBox4: TComboBox;
    procedure ComboBox1Change(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    procedure hidefields;
  public
    { Public declarations }
  end;

var
  Form17: TForm17;

implementation

{$R *.dfm}

procedure TForm17.ComboBox1Change(Sender: TObject);
begin
  hidefields;
  case combobox1.ItemIndex of
    0:begin
        label4.Top := 104;
        combobox3.Top := 120;
        label4.Caption := 'Schalten';
        label4.Visible := true;
        combobox3.Visible := true;
      end;
    1:begin
        label4.Top := 152;
        combobox3.Top := 168;
        combobox2.Visible := true;
        label3.Visible := true;
      end;
  end;
end;

procedure TForm17.ComboBox2Change(Sender: TObject);
begin
  hidefields;
  case combobox2.ItemIndex of
    0:begin
        label4.Caption := 'Richtung';
        label4.Visible := true;
        combobox4.Visible := true;
      end;
    1:begin
        label5.Caption := 'Geschwindigkeit';
        label5.Visible := true;
        spinedit2.Visible := true;
      end;
    2:begin
        label5.Caption := 'Funktion';
        label5.Visible := true;
        spinedit2.MaxValue := 15;
        spinedit2.Value := 1;
        spinedit2.Visible := true
      end;
  end;
end;

procedure Tform17.hidefields;
begin
  spinedit2.Visible := false;
  label4.Visible := false;
  label5.Visible := false;
  combobox3.Visible := false;
  combobox4.Visible := false;
end;

procedure TForm17.FormCreate(Sender: TObject);
begin
  doublebuffered := true;
end;

procedure TForm17.Button1Click(Sender: TObject);
begin
  close;
end;

end.
