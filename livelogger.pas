unit livelogger;

interface

uses
  Classes, UDPlistenThread, main, typehelper;

type
  TLiveLog = class(TThread)
    FReciever : TUDPListener;
  private
    procedure LogEvents;
  protected
    procedure Execute; override;
  public
    constructor create(rcv : TUDPListener);
  end;

implementation

uses debugger;

{ Important: Methods and properties of objects in visual components can only be
  used in a method called using Synchronize, for example,

      Synchronize(UpdateCaption);

  and UpdateCaption could look like,

    procedure TLiveLog.UpdateCaption;
    begin
      Form1.Caption := 'Updated in a thread';
    end; }

{ TLiveLog }

procedure TLiveLog.Execute;
begin
  LogEvents;
end;

constructor TLiveLog.create(rcv : TUDPListener);
begin
  inherited create(false);
  Freciever := rcv;
end;

procedure TLiveLog.LogEvents;
var temp : string;
    oldBuff : TBuff;
    i : integer;
begin
  oldBuff := Freciever.Buffer;
  while true do begin
    if Freciever.Buffer <> oldBuff then begin
      oldBuff := Freciever.Buffer;
      temp := Bufftostr(FReciever.Buffer);
      form6.Memo1.Lines.Add(temp);
    end;
  end;
end;

end.
