object TrainControlpanel: TTrainControlpanel
  Left = 640
  Top = 223
  BorderStyle = bsToolWindow
  Caption = 'Lok'
  ClientHeight = 246
  ClientWidth = 303
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnDeactivate = FormDeactivate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 0
    Width = 303
    Height = 2
    Cursor = crVSplit
    Align = alTop
  end
  object Groupbox2: TGroupBox
    Left = 0
    Top = 2
    Width = 203
    Height = 244
    Align = alClient
    Caption = 'Steuerung'
    TabOrder = 1
    object GroupBox1: TGroupBox
      Left = 2
      Top = 15
      Width = 199
      Height = 97
      Align = alTop
      TabOrder = 0
      object Image1: TImage
        Left = 2
        Top = 15
        Width = 195
        Height = 80
        Align = alClient
        Proportional = True
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 112
      Width = 199
      Height = 130
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 0
        Top = 0
        Width = 199
        Height = 13
        Align = alTop
        Caption = 'Info'
      end
      object Gauge1: TGauge
        Left = 0
        Top = 13
        Width = 199
        Height = 84
        Align = alTop
        Kind = gkNeedle
        Progress = 0
      end
      object Panel2: TPanel
        Left = 166
        Top = 97
        Width = 33
        Height = 33
        Align = alRight
        TabOrder = 0
        object Image2: TImage
          Left = 1
          Top = 1
          Width = 31
          Height = 31
          Align = alClient
          Stretch = True
          OnMouseDown = Image2MouseDown
          OnMouseUp = Image2MouseUp
        end
      end
      object TrackBar1: TTrackBar
        Left = 0
        Top = 97
        Width = 166
        Height = 33
        Align = alClient
        Max = 100
        TabOrder = 1
        TickMarks = tmBoth
        TickStyle = tsManual
      end
    end
  end
  object Groupbox3: TGroupBox
    Left = 203
    Top = 2
    Width = 100
    Height = 244
    Align = alRight
    Caption = 'Funktionen'
    TabOrder = 0
  end
end
