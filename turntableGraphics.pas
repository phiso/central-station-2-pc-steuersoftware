unit turntableGraphics;

interface

uses Classes, Sysutils, Graphics, Extctrls, Controls, types, math, forms,
     typehelper, dialogs;

const Step = pi/180;

type TClickWayEvent = procedure(Sender : Tobject; deg,x,y : integer) of object;
type TCircleClickEvent = procedure(Sender : Tobject; x,y : integer; deg : double) of object;
type TCirclePiece = record
       StartAngle,EndAngle : real;
       Innerradius,OuterRadius : integer;
end;
type TTurntableGraph = class(Timage)
  private
    FWayClick : TClickWayEvent;
    FCircleClick, FCircleMove : TCircleClickEvent;
    FTablePos : Double;
    FOuterTableRadius, FinnerTableRadius,
    FAnimSpeed, FNullAngle : integer;
    FRotation : array[0..1] of real;
    FCenter, FMousePos : Tpoint;
    FCirclePiece : array[0..47] of TCirclePiece;

    procedure drawMainCircles;
    procedure Circle(center : Tpoint; r : integer);
    procedure DrawPositions;
    procedure drawWay(deg : real); //deg = 0..360 Grad
    procedure SetAnimSpeed(s : integer); // 0..100
    function TranslatePos(x,y : integer):double;
    procedure MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Clicking(Sender : TObject);
  protected
    procedure CircleClick(Sender : TObject; x,y : integer; deg : double);
    procedure CircleMove(Sender : TObject; x,y : integer; deg : double);
  public
    property AnimationSpeed : integer read FAnimspeed write SetAnimspeed;
    property Rotation : real read FRotation[1];
    property OnCircleClick : TCircleClickEvent read FCircleClick write FCircleClick;
    property OnCircleMove : TCircleClickEvent read FCircleMove write FCircleMove;

    constructor create(Owner : TComponent; animspeed : integer=100);

    procedure turn(add : real);
    procedure resetPosition;
    procedure TurnTo(rot : real; anim : boolean);
    procedure TurnToPos(rot : real; anim : boolean);
    procedure RailPosition(c : integer);
    procedure NumPosition(c : integer);
    procedure MarkPosition(c : integer);
end;

implementation

procedure TTurnTableGraph.CircleClick(Sender : TObject; x,y : integer; deg : double);
begin
  if assigned(FcircleCLick) then
    FCircleClick(Sender,x,y,deg);
end;

procedure TTurnTableGraph.CircleMove(Sender : TObject; x,y : integer; deg : double);
begin
  if assigned(FCircleMove) then
    FCircleMOve(Sender,x,y,deg);
end;

constructor TTurntableGraph.create(Owner : TComponent; animspeed : integer=100);
begin
  inherited create(Owner);
  self.Parent := (Owner as Twincontrol);
  self.Align := alClient;
  self.Visible := true;
  (Owner as TwinControl).DoubleBuffered := true;
  if self.Width-self.Height >= 0 then FOutertableRadius := (self.Width div 2)-50
  else FOutertableRadius := (self.Height div 2)-50;
  FInnerTableRadius := FouterTableRadius-20;
  Fcenter := point(self.Width div 2,self.Height div 2);
  FRotation[0] := 270;
  FRotation[1] := 270;
  FAnimSpeed := animspeed;
  drawMainCircles;
  drawPositions;
  self.Canvas.Pen.Mode := pmnotxor;
  self.OnMouseMove := MouseMove;
  self.OnClick := Clicking;
end;

procedure TTurntableGraph.SetAnimSpeed(s : integer);
begin
  FAnimSpeed := s;
end;

procedure TTurntableGraph.drawMainCircles;
begin
  circle(Fcenter,FOuterTableRadius);
  circle(Fcenter,FInnerTableRadius);
  drawway(FRotation[1]);
  self.Refresh;
end;

procedure TTurntableGraph.Circle(center : Tpoint; r : integer);
var Punkt : Tpoint;
    i : integer;
begin
  punkt.X := round(cos(0)*r)+center.X;
  punkt.Y := round(sin(0)*r)+center.Y;
  for i := 0 to 360 do begin
    with self.Canvas do begin
      pen.Width := 2;
      moveto(punkt.X,punkt.Y);
      punkt.X := round(cos(step*i)*r)+center.X;
      punkt.Y := round(sin(step*i)*r)+center.Y;
      lineto(punkt.X,punkt.Y);
      pen.Width := 1;
    end;
  end;
end;

procedure TTurntableGraph.DrawPositions;
var i : integer;
    p,w : real;
    up,down : Tpoint;
begin
  p := 3.75;
  for i := 0 to 47 do begin
    w := 270+p+i*7.5;
    FCirclePiece[i].StartAngle := w;
    FCirclePiece[i].EndAngle := w+7.5;
    FCirclePiece[i].Innerradius := FInnerTableRadius;
    FCirclePiece[i].OuterRadius := FOuterTableRadius;
    
    up.X := round(cos(step*w)*FOuterTableRadius)+Fcenter.X;
    up.Y := round(sin(step*w)*FouterTableRadius)+Fcenter.Y;
    down.X := round(cos(step*w)*FInnerTableRadius)+Fcenter.X;
    down.Y := round(sin(step*w)*FInnerTableRadius)+Fcenter.Y;
    with self.Canvas do begin
      pen.Width := 2;
      moveto(up.X,up.Y);
      lineto(down.X,down.Y);
      pen.Width := 1;
    end;
//    numPosition(i);
  end;
end;

procedure TTurntableGraph.drawWay(deg : real);
var pointUp1,pointDown1,pointUp2,pointDown2,
    pointleft,pointLeft2,pointLeft3 : Tpoint;
    invdeg,normdeg : real;
begin
  normdeg := deg+3.75;
  pointUp1.X := round(cos(step*normdeg)*(FInnerTableRadius-5))+Fcenter.X;
  pointUp1.Y := round(sin(step*normdeg)*(FinnerTableRadius-5))+Fcenter.Y;
  invdeg := normdeg+180;
  if normdeg > 180 then invdeg := normdeg-180;
  pointDown1.X := round(cos(step*invdeg)*(FInnerTableRadius-5))+Fcenter.X;
  pointDown1.Y := round(sin(step*invdeg)*(FinnerTableRadius-5))+Fcenter.Y;

  normdeg := deg-3.75;
  pointUp2.X := round(cos(step*normdeg)*(FInnerTableRadius-5))+Fcenter.X;
  pointUp2.Y := round(sin(step*normdeg)*(FinnerTableRadius-5))+Fcenter.Y;
  invdeg := normdeg+180;
  if normdeg > 180 then invdeg := normdeg-180;
  pointDown2.X := round(cos(step*invdeg)*(FInnerTableRadius-5))+Fcenter.X;
  pointDown2.Y := round(sin(step*invdeg)*(FinnerTableRadius-5))+Fcenter.Y;
  pointLeft.X := round(cos(step*(normdeg-8))*(FInnerTableRadius-5))+Fcenter.X;
  pointLeft.Y := round(sin(step*(normdeg-8))*(FinnerTableRadius-5))+Fcenter.Y;
  pointLeft2.X := round(cos(step*(normdeg-8))*(FInnerTableRadius-35))+Fcenter.X;
  pointLeft2.Y := round(sin(step*(normdeg-8))*(FinnerTableRadius-35))+Fcenter.Y;
  pointLeft3.X := round(cos(step*normdeg)*(FInnerTableRadius-35))+Fcenter.X;
  pointLeft3.Y := round(sin(step*normdeg)*(FinnerTableRadius-35))+Fcenter.Y;

  with self.Canvas do begin
    pen.Width := 2;
    moveto(pointup1.X,pointup1.Y);

    lineto(pointdown2.X,pointdown2.Y);
    Pixels[pointdown2.X,pointdown2.Y] := clblack;

    lineto(pointdown1.x,pointdown1.y);
    pixels[pointdown1.x,pointdown1.y] := clblack;

    lineto(pointup2.X,pointup2.Y);
    pixels[pointup2.X,pointup2.Y] := clblack;

    lineto(pointup1.X,pointup1.Y);
    pixels[pointup1.X,pointup1.Y] := clblack;

    moveto(pointup2.X,pointup2.Y);

    lineto(pointLeft.X,pointLeft.Y);
    pixels[pointLeft.X,pointLeft.Y] := clblack;

    lineto(pointLeft2.X,pointLeft2.Y);
    pixels[pointLeft2.X,pointLeft2.Y] := clblack;

    lineto(pointleft3.X,pointleft3.Y);
    pixels[pointleft3.X,pointleft3.Y] := clblack;
    pen.Width := 1;
  end;
end;

procedure TTurntableGraph.turn(add : real);
begin
  Frotation[1] := Frotation[0] + add;
  drawway(FRotation[0]);
  drawway(Frotation[1]);
  Frotation[0] := FRotation[1];
  self.Refresh;
end;

procedure TTurntableGraph.resetPosition;
begin
  drawway(FRotation[0]);
  FRotation[1] := 270;
  drawway(Frotation[1]);
  Frotation[0] := Frotation[1];
end;

procedure TTurntableGraph.TurnTo(rot : real; anim : boolean);
var i,max : integer;
begin
  max := floor(abs(rot));
  if anim then begin
    for i := 0 to max-1 do begin
      if rot < 0 then
        turn(-1)
      else turn(1);
      pause(268-FAnimspeed);
    end;
    if rot < 0 then turn(rot+max)
    else turn(rot-max);
  end else begin
    drawway(FRotation[0]);
    FRotation[1] := Frotation[0] + rot;
    drawway(FRotation[0]);
    drawway(Frotation[1]);
    FRotation[0] := FRotation[1];
  end;
end;

procedure TTurnTableGraph.TurnToPos(rot : real; anim : boolean);
var i,max : integer;
begin
  max := floor(abs(rot));
  if anim then begin
    for i := 0 to max-1 do begin
      if rot < 0 then
        turn(-1)
      else turn(1);
      pause(268-FAnimspeed);
    end;
  end else begin
    FRotation[1] := Frotation[0] + rot;
    drawway(FRotation[0]);
    drawway(Frotation[1]);
    FRotation[0] := FRotation[1];
  end;
end;

procedure TTurnTableGraph.NumPosition(c : integer);
var x,y : integer;
    p : TCirclePiece;
begin
  p := FCirclePiece[c];
  x := round(cos(step*(p.StartAngle+2.5))*(p.Outerradius-4))+FCenter.X;
  y := round(sin(step*(p.StartAngle+2.5))*(p.Outerradius-4))+Fcenter.Y;
  with self.Canvas do begin
    pen.Mode := pmBlack;
    TextOut(x,y,inttostr(c));
    pen.Mode := pmNotXor;
  end;
end;

procedure TTurntableGraph.RailPosition(c : integer);
var x1,x2,y1,y2 : integer;
    p : TCirclePiece;
begin
  p := FCirclePiece[c];
  x1 := round(cos(step*(p.StartAngle+2.5))*(p.Innerradius-2))+Fcenter.X;
  x2 := round(cos(step*(p.StartAngle+2.5))*(p.OuterRadius+15))+FCenter.X;
  y1 := round(sin(step*(p.StartAngle+2.5))*(p.Innerradius-2))+FCenter.Y;
  y2 := round(sin(step*(p.StartAngle+2.5))*(p.OuterRadius+15))+FCenter.Y;
  with self.Canvas do begin
    pen.Mode := pmBlack;
    moveto(x1,y1);
    lineto(x2,y2);
  end;

  x1 := round(cos(step*(p.EndAngle-2.5))*(p.Innerradius-2))+Fcenter.X;
  x2 := round(cos(step*(p.EndAngle-2.5))*(p.OuterRadius+15))+FCenter.X;
  y1 := round(sin(step*(p.EndAngle-2.5))*(p.Innerradius-2))+FCenter.Y;
  y2 := round(sin(step*(p.EndAngle-2.5))*(p.OuterRadius+15))+FCenter.Y;
  with self.Canvas do begin
    moveto(x1,y1);
    lineto(x2,y2);
    pen.Width := 1;
    pen.Mode := pmNotXor;
  end;
end;

procedure TTurntableGraph.MarkPosition(c : integer);
var x,y,i : integer;
    p : TCirclePiece;
begin
  p := FCirclePiece[c];
  self.Canvas.Pen.Color := clred;
  x := round(cos(step*(p.StartAngle+1.25))*(p.Innerradius+3))+Fcenter.X;
  y := round(sin(step*(p.StartAngle+1.25))*(p.Innerradius+3))+FCenter.Y;
  self.Canvas.MoveTo(x,y);
  for i := 1 to 5 do begin
    x := round(cos(step*((p.StartAngle+1.25)+i))*(p.Innerradius+3))+Fcenter.X;
    y := round(sin(step*((p.StartAngle+1.25)+i))*(p.Innerradius+3))+FCenter.Y;
    self.Canvas.LineTo(x,y);
  end;
  x := round(cos(step*(p.EndAngle-1.25))*(p.OuterRadius-3))+FCenter.X;
  y := round(sin(step*(p.endAngle-1.25))*(p.OuterRadius-3))+FCenter.Y;
  self.Canvas.LineTo(x,y);
  for i := 1 to 5 do begin
    x := round(cos(step*((p.endAngle-1.25)-i))*(p.OuterRadius-3))+Fcenter.X;
    y := round(sin(step*((p.endAngle-1.25)-i))*(p.OuterRadius-3))+Fcenter.Y;
    self.Canvas.LineTo(x,y);
  end;
  x := round(cos(step*(p.StartAngle+1.25))*(p.Innerradius+3))+Fcenter.X;
  y := round(sin(step*(p.StartAngle+1.25))*(p.Innerradius+3))+FCenter.Y;
  self.canvas.lineto(x,y);
  self.Canvas.Pen.Color := clblack;
end;

procedure TTurnTableGraph.MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
var temp : double;
begin
  FMousePos := point(x,y);
  temp := translatePos(x,y);
  if temp <> -1 then CircleMove(Sender,x,y,temp);
end;

procedure TTurnTablegraph.Clicking(Sender : TObject);
var deg : double;
begin
  deg := Translatepos(FMousePos.X,FMousePos.Y);
  if deg <> -1 then begin
    CircleClick(Sender,FMousePos.X,FmousePos.Y,deg);
  end;
end;

function TTurnTableGraph.TranslatePos(x,y : integer):double;
var d,rot : double;
begin
  rot := -1;
  //abstand zum Mittelpunkt berechnen
  d := sqrt(power((Fcenter.X-x),2)+power((Fcenter.Y-y),2));
  if (d > FInnerTableRadius) and (d < FOuterTableRadius) then begin
    //winkel berechnen
    rot := (180/pi)*arcsin((Fcenter.Y-y)/d);
    if (rot < 0) and (x > Fcenter.X) then rot := abs(rot)
    else if (rot < 0) and (x < Fcenter.X) then rot := 180-abs(rot)
    else if (rot > 0) and (x < Fcenter.X) then rot := 180+rot
    else rot := 360-rot;
  end;
  result := rot;
end;

end.
