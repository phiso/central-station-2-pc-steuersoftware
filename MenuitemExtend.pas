unit MenuitemExtend;

interface

uses Classes, Sysutils, menus;

type TClickEvent = procedure(Sender : Tobject; nr : integer) of object;
type TMenuItemEx = class(TMenuitem)
  private
    FNr : integer;
    FOnClick : TClickEvent;
    procedure clicking(Sender : Tobject);
  protected
    procedure newClick(Sender : Tobject; nr : integer);
  public
    property doClick : TclickEvent read FOnClick write FOnClick;
    constructor create(Owner : TMenuItem; nr : integer);
end;

implementation

constructor TMenuItemEx.create(Owner : TMenuItem; nr : integer);
begin
  inherited create(Owner);
  Owner.add(self);
  Fnr := nr;
  self.OnClick := clicking;
end;

procedure TMenuitemEx.newClick;
begin
  if Assigned(FOnClick) then
    FonClick(Self, Fnr);
end;

procedure TMenuItemEx.clicking(Sender : Tobject);
begin
  newClick(Sender,Fnr);
end;



end.
