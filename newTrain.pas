unit newTrain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Spin, ExtCtrls, ExtDlgs, jpg, typehelper, main, imageEx,
  Menus;

type
  TForm7 = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    LabeledEdit1: TLabeledEdit;
    Image1: TImage;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    SpinEdit1: TSpinEdit;
    Label2: TLabel;
    ComboBox1: TComboBox;
    Button2: TButton;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    SpinEdit2: TSpinEdit;
    SpinEdit3: TSpinEdit;
    SpinEdit4: TSpinEdit;
    SpinEdit5: TSpinEdit;
    SpinEdit6: TSpinEdit;
    Label8: TLabel;
    SpinEdit7: TSpinEdit;
    OpenPictureDialog1: TOpenPictureDialog;
    Button3: TButton;
    Button4: TButton;
    GroupBox2: TGroupBox;
    Image13: TImage;
    PopupMenu1: TPopupMenu;
    FunktionAktiv1: TMenuItem;
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    tempTrainData : TTrain;
    tempDecoder : TDecoder;
    tempCVValues : TBytes;
    funcimage : array[0..15] of TimageEx;
    funcImageState : array[0..15] of Boolean;
    
    ImageOnMouse : integer;

    procedure FuncImageClick(Sender : TObject; nr : integer);
    procedure FuncImageOver(Sender : TObject; nr : integer);
  public
    { Public declarations }
  end;

var
  Form7: TForm7;

implementation

{$R *.dfm}

procedure TForm7.Button1Click(Sender: TObject);
var w,h : integer;
begin
  if openpicturedialog1.Execute then begin
    loadpicture(openpicturedialog1.FileName,image1,w,h);
    image1.Visible := true;
  end;
end;

procedure TForm7.Button3Click(Sender: TObject);
begin
  with tempTRainData do begin
    Name := labelededit1.Text;
    Adress := spinedit1.Value;
    decoder := tempDecoder;
    pic := openpicturedialog1.FileName;
    anf := spinedit2.Value;
    brems := spinedit3.Value;
    vmin := spinedit5.Value;
    vmax := spinedit6.Value;
    loud := spinedit4.Value;
    tacho := spinedit7.Value;
  end;
  form1.newTrain(tempTrainData);
  close;
end;

procedure TForm7.FormCreate(Sender: TObject);
var i : integer;
begin
  tempDecoder := dcMM2Shift;
  setlength(tempCVValues,0); //sp�ter per CV Zugriff
  openpicturedialog1.InitialDir := extractfilepath(application.ExeName)+'icons';
  image1.Picture.LoadFromFile(extractfilepath(application.ExeName)+'icons\loks\keinbild.jpg');
  openpicturedialog1.FileName := extractfilepath(application.ExeName)+'icons\loks\keinbild.jpg';
  ImageOnMouse := -1;
  for i := 0 to 15 do begin
    funcimage[i] := TimageEX.create(Groupbox2,i);
    funcImageState[i] := false;
    funcimage[i].Parent := Groupbox2;
    funcImage[i].DoClick := FuncImageClick;
    funcimage[i].IsMouseOn := FuncImageOver;
    funcimage[i].PopupMenu := popupmenu1;
    funcimage[i].Width := 33;
    funcImage[i].Height := 33;
    funcimage[i].Stretch := true;
    funcimage[i].Top := ((i div 2)*40)+10;
    if ((i+1) mod 2) = 0 then funcimage[i].Left := 53
    else funcimage[i].Left := 10;
    funcimage[i].Visible := true;
    funcimage[i].Picture.Bitmap.LoadFromFile(form1.Default_SystemPath+'icons\func\std_off.bmp');
    funcimage[i].Picture.Bitmap.Canvas.TextRect(rect(36,24,64,64),40,30,inttostr(i));
  end;
end;

procedure TForm7.ComboBox1Change(Sender: TObject);
begin
  case combobox1.ItemIndex of
    0:tempdecoder := dcMM2Shift;
    1:tempdecoder  := dcMM2Prog;
    2:tempdecoder := dcDCC;
  end;
end;

procedure TForm7.Button4Click(Sender: TObject);
begin
  close;
end;

procedure TForm7.FuncImageOver(Sender : TObject; nr : integer);
begin
  ImageOnMouse := nr;
end;

procedure TForm7.FuncImageClick(Sender : TObject; nr : integer);
begin
  if funcImageState[nr] then begin
    funcImageState[nr] := false;
    funcimage[nr].Picture.Bitmap.LoadFromFile(form1.Default_SystemPath+'icons\func\std_off.bmp');
    funcimage[nr].Picture.Bitmap.Canvas.TextRect(rect(36,24,64,64),40,30,inttostr(nr));
  end else begin
    funcImageState[nr] := true;
    funcimage[nr].Picture.Bitmap.LoadFromFile(form1.Default_SystemPath+'icons\func\std_on.bmp');
    funcimage[nr].Picture.Bitmap.Canvas.TextRect(rect(36,24,64,64),40,30,inttostr(nr));
  end;
end;

end.
