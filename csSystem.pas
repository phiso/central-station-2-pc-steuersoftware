unit csSystem;

interface

uses typehelper, package, control, SysUtils, math, dialogs;

type TcsSystem = class(Tcontrol)
    FS88 : integer;
    FS88Response : TIntArray;
  private
    function Checkresponse(cmd : TBuffer):boolean;
    procedure SetS88(c : integer);
  public
    property S88 : integer read FS88 write SetS88;
    property S88Response : TIntArray read FS88Response;

    constructor create(s88:integer=0);

    procedure SystemOverload(ch : integer);
    procedure S88Event(nr,active : integer);

    function SystemStop:boolean;
    function SystemGo:boolean;
    function SystemHold:boolean;
    function SystemShiftTime(t : integer):boolean;
    function SystemStatus(channel : integer):integer;
    function SystemReset:boolean;//ACHTUNNG F�HRT KOMPLETTEN RESET DER CENTRAL STATION DURCH
end;

implementation

uses main;

constructor TcsSystem.create(s88:integer=0);
var i : integer;
begin
  inherited create(0,dcDCC);
  FBuffer.Adress := 0;
  Fbuffer.cmdbyte := 0;
  setlength(FS88Response,s88*16);
  for i := 0 to s88-1 do FS88Response[i] := 0;
end;

function TcsSystem.Checkresponse(cmd : TBuffer):boolean;
var resp : Tbuff;
begin
  resp := MainUDPListener.ResponseBuff;
  if ord(resp[1]) = cmd.cmdbyte+1 then result := true
    else result := false;
end;

function TcsSystem.SystemStop:boolean;
begin
  FBuffer.DLCbyte := 5;
  FBuffer.SingleVal := 0;
  Fpackage.Buildpackage(Fbuffer);
  Fpackage.SendPackage(false);
  sleep(0);
  result := Checkresponse(Fbuffer);
end;

function TcsSystem.SystemGo:boolean;
begin
  FBuffer.DLCbyte := 5;
  FBuffer.SingleVal := 1;
  Fpackage.Buildpackage(Fbuffer);
  Fpackage.SendPackage(false);
  sleep(0);
  result := Checkresponse(Fbuffer);
end;

function TcsSystem.SystemHold:boolean;
begin
  Fbuffer.DLCbyte := 5;
  FBuffer.SingleVal := 2;
  Fpackage.Buildpackage(Fbuffer);
  Fpackage.SendPackage(false);
  sleep(0);
  result := Checkresponse(Fbuffer);
end;

function TcsSystem.SystemShiftTime(t : integer):boolean;
begin
  FBuffer.DLCbyte := 7;
  Fbuffer.B3Val[0] := 6;
  Fbuffer.B3Val[1] := ord(shiftSpeedbytes(t)[0]);
  Fbuffer.B3Val[2] := ord(ShiftSpeedBytes(t)[1]);
  Fpackage.Buildpackage(Fbuffer);
  Fpackage.SendPackage(false);
  sleep(0);
  result := Checkresponse(Fbuffer);
end;

function TcsSystem.SystemStatus(channel : integer):integer;
var resp : TBuff;
    temp : TBytes;
begin
  FBuffer.DLCbyte := 6;
  FBuffer.B2Val[0] := 11;
  FBuffer.B2Val[1] := channel;
  Fpackage.Buildpackage(Fbuffer);
  Fpackage.SendPackage(false);
  sleep(0);
  result := 0;
  setlength(temp,2);
  resp := MainUDPListener.ResponseBuff;
  temp[0] := ord(resp[11]);
  temp[1] := ord(resp[12]);
  result := bytesToInt(temp);
end;

function TcsSystem.SystemReset:boolean;
begin
  FBuffer.DLCbyte := 6;
  //Unklar was genau gesendet werden soll
  {Funktion ist nicht Relevant, da ein reset sowieso nur an der Central Station
   durchgef�hrt werden sollte}
  Fpackage.Buildpackage(Fbuffer);
  Fpackage.SendPackage(false);
  sleep(0);
  result := Checkresponse(Fbuffer);
end;

procedure TcsSystem.SetS88(c : integer);
var i : integer;
begin
  setlength(FS88Response,c*16);
  for i := FS88*16 to (c*16)-1 do FS88Response[i] := 0;
  FS88 := c;
end;

procedure TcsSystem.SystemOverload(ch : integer);
begin
  Showmessage('System Meldet �berlastung auf Kanal '+inttostr(ch));
end;

procedure TcsSystem.S88Event(nr,active : integer);
begin
  If active = 0 then FS88Response[nr] := 0
    else FS88Response[nr] := 1;
end;

end.
