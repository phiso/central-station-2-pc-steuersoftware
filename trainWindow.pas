unit trainWindow;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Spin, ComCtrls, StdCtrls, typehelper, traincontroller;

type TControlEvent = procedure(Sender:TObject; adr : integer) of Object;
type TTrainWindow = class(Tform)
    Panel1: TPanel;
    Panel2: TPanel;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Panel3: TPanel;
    Image1: TImage;
    Panel4: TPanel;
    Splitter3: TSplitter;
    GroupBox1: TGroupBox;
    ProgressBar1: TProgressBar;
    SpinButton1: TSpinButton;
    Image2: TImage;
    ScrollBox1: TScrollBox;
  private
    //Controller : TTrainController;
    FControlEvent : TControlEvent;
    FTrain : TTrain;

    procedure Image1Click(Sender : Tobject);
  protected
    procedure OnControl(Sender : Tobject; adr : integer);
  public
    property ControlEvent : TControlEvent read FControlEvent write FcontrolEvent;
    constructor create(Owner : Tcomponent; Train : TTrain);
end;

var
  TrainW : TTrainWindow;

implementation

{$R *.dfm}

constructor TTrainWindow.create(Owner : Tcomponent; Train : TTrain);
begin
  inherited create(Owner);
  FTrain := Train;
  self.Show;
end;

procedure TTrainWindow.OnControl(Sender : Tobject; adr : integer);
begin
  if Assigned(FControlEvent) then begin
    FControlEvent(Sender,adr);
  end;
end;

procedure TTrainwindow.Image1Click(Sender : Tobject);
begin

end;

end.
