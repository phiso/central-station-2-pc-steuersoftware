unit traincontroller;

interface

uses typehelper, Sysutils, Classes, RuleHandler, Control;

const WAIT_FOR_RESPONSE = 0;

type TTrainController = class(TControl)
  private
    FDecType : TDecoder;
    function Checkresponse(cmd : TBuffer):boolean;
  public
    constructor create(adr : integer; dc : TDecoder);

    function ChangeDirection:boolean;
    function ForceForward:boolean;
    function ForceBackward:boolean;
    function CheckDirection:integer;

    function ActivateFunction(f : integer):boolean;
    function DeactivateFunction(f : integer):boolean;
    function CheckFunction(f : integer):boolean;

    function SetSpeed(s : integer):boolean;
    function CheckSpeed:integer;
end;

implementation

uses main;

constructor TTraincontroller.create(adr : integer; dc : TDecoder);
begin
  inherited create(adr,dc);
end;

function TTrainController.Checkresponse(cmd : TBuffer):boolean;
var resp : Tbuff;
begin
  resp := MainUDPListener.ResponseBuff;
  if ord(resp[1]) = cmd.cmdbyte+1 then result := true
    else result := false;
end;

function TTraincontroller.ChangeDirection:boolean;
var resp : TBuff;
begin
  FBuffer.cmdbyte := 10;
  FBuffer.Adress := Fadress;
  FBuffer.DLCbyte := 5;
  Fbuffer.SingleVal := 3;
  if (FDecoder = dcDCC) or (FDecoder = dcMFX) then Fpackage.Buildpackage(Fbuffer,1)
  else Fpackage.Buildpackage(Fbuffer);
  Fpackage.SendPackage(false);
  sleep(WAIT_FOR_RESPONSE);
  result :=  Checkresponse(FBuffer);
end;

function TTrainController.ForceForward:Boolean;
begin
  FBuffer.cmdbyte := 10;
  FBuffer.Adress := Fadress;
  FBuffer.DLCbyte := 5;
  Fbuffer.SingleVal := 1;
  if (FDecoder = dcDCC) or (FDecoder = dcMFX) then Fpackage.Buildpackage(Fbuffer,1)
  else Fpackage.Buildpackage(Fbuffer);
  Fpackage.SendPackage(false);
  sleep(WAIT_FOR_RESPONSE);
  result := Checkresponse(FBuffer);
end;

function TTrainController.ForceBackward:Boolean;
begin
  FBuffer.cmdbyte := 10;
  FBuffer.Adress := Fadress;
  FBuffer.DLCbyte := 5;
  Fbuffer.SingleVal := 2;
  if (FDecoder = dcDCC) or (FDecoder = dcMFX) then Fpackage.Buildpackage(Fbuffer,1)
  else Fpackage.Buildpackage(Fbuffer);
  Fpackage.SendPackage(false);
  sleep(WAIT_FOR_RESPONSE);
  result := Checkresponse(FBuffer);
end;

function TTrainController.CheckDirection:integer;
begin

end;

function TTrainController.ActivateFunction(f : integer):boolean;
begin
  FBuffer.cmdbyte := 12;
  FBuffer.Adress := Fadress;
  Fbuffer.DLCbyte := 6;
  FBuffer.B2Val[0] := f;
  FBuffer.B2Val[1] := 1;
  if (FDecoder = dcDCC) or (FDecoder = dcMFX) then Fpackage.Buildpackage(Fbuffer,1)
  else Fpackage.Buildpackage(Fbuffer);
  Fpackage.SendPackage(false);
  sleep(WAIT_FOR_RESPONSE);
  result := CHeckresponse(FBuffer);
end;

function TTrainController.DeactivateFunction(f : integer):boolean;
begin
  FBuffer.cmdbyte := 12;
  FBuffer.Adress := Fadress;
  Fbuffer.DLCbyte := 6;
  FBuffer.B2Val[0] := f;
  FBuffer.B2Val[1] := 0;
  if (FDecoder = dcDCC) or (FDecoder = dcMFX) then Fpackage.Buildpackage(Fbuffer,1)
  else Fpackage.Buildpackage(Fbuffer);
  Fpackage.SendPackage(false);
  sleep(WAIT_FOR_RESPONSE);
  result := CHeckresponse(FBuffer);
end;

function TTrainController.CheckFunction(f : integer):boolean;
var resp : TBuff;
begin
  FBuffer.cmdbyte := 12;
  FBuffer.Adress := Fadress;
  Fbuffer.DLCbyte := 5;
  FBuffer.SingleVal := f;
  if (FDecoder = dcDCC) or (FDecoder = dcMFX) then Fpackage.Buildpackage(Fbuffer,1)
  else Fpackage.Buildpackage(Fbuffer);
  Fpackage.SendPackage(false);
  sleep(WAIT_FOR_RESPONSE);
  resp := MainUDPListener.ResponseBuff;
  if ord(resp[4+6]) = 1 then result := true
    else result := false;
end;

function TTrainController.SetSpeed(s : integer):boolean;
begin
  FBuffer.cmdbyte := 8;
  FBuffer.Adress := FAdress;
  FBuffer.DLCbyte := 6;
  FBuffer.B2Val[0] := CalcSpeedBytes(s)[0];
  FBuffer.B2Val[1] := CalcSpeedBytes(s)[1];
  if (FDecoder = dcDCC) or (FDecoder = dcMFX) then Fpackage.Buildpackage(Fbuffer,1)
  else Fpackage.Buildpackage(Fbuffer);
  FPackage.SendPackage(false);
  sleep(WAIT_FOR_RESPONSE);
  result := checkresponse(FBuffer);
end;

function TTrainController.CheckSpeed:integer;
var resp : TBuff;
begin
  FBuffer.cmdbyte := 8;
  FBuffer.Adress := FAdress;
  FBuffer.DLCbyte := 4;
  if (FDecoder = dcDCC) or (FDecoder = dcMFX) then Fpackage.Buildpackage(Fbuffer,1)
  else Fpackage.Buildpackage(Fbuffer);
  Fpackage.SendPackage(false);
  sleep(WAIT_FOR_RESPONSE);
  resp := MainUDPListener.ResponseBuff;
  result := bytestospeed(ord(resp[9]),ord(resp[10]));
end;

end.
