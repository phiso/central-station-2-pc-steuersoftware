unit insertLayoutObject;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, typehelper, layoutobject, keyboard;

type
  TForm13 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Label1: TLabel;
    ComboBox1: TComboBox;
  private
    FArticles : TArticleArray;
  public
    procedure start(lobtype : string);
  end;

var
  Form13: TForm13;

implementation

{$R *.dfm}

procedure Tform13.start(lobtype : string);
var i : integer;
begin
//  setlength(Farticles,form3.GetArticleCount);
  for i := 0 to form3.GetArticleCount-1 do begin
    if form3.getArticle(i).Typ = lobtype then begin
      setlength(Farticles,length(Farticles)+1);
      Farticles[length(Farticles)-1] := form3.getArticle(i);
      combobox1.Items.Add(Farticles[length(Farticles)-1].name+','+inttostr(Farticles[length(Farticles)-1].adr))
    end;
  end;
  showmodal;
end;

end.
