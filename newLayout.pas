unit newLayout;
{Momentan ist die gr��e des fenster festgestellt, da die automatische
 Gr��enanpassung der Klass TGridImage nicht fertig implementiert ist}
 
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ExtCtrls, StdCtrls, gridimage, layoutobject, imageEx, inifiles,
  math, jpeg, ComCtrls, typehelper, ExtDlgs, GDIPOBJ, GDIPAPI, Buttons;


type
  TForm8 = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    MainMenu1: TMainMenu;
    Gleisbild1: TMenuItem;
    Neu1: TMenuItem;
    Export1: TMenuItem;
    Import1: TMenuItem;
    Beenden1: TMenuItem;
    ScrollBox1: TScrollBox;
    Image1: TImage;
    PopupMenu1: TPopupMenu;
    lschen1: TMenuItem;
    drehen1: TMenuItem;
    info1: TMenuItem;
    exteinfgen1: TMenuItem;
    StatusBar1: TStatusBar;
    RadioGroup1: TRadioGroup;
    Button1: TButton;
    Laden1: TMenuItem;
    Speichern1: TMenuItem;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    Button2: TButton;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Button1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Speichern1Click(Sender: TObject);
    procedure Laden1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    GridImage : TgridImage;
    LayoutObject : array of TLayoutObject;
    Panels : array of Tpanel;
    imgs : array of TImage;
    Mouseimage : Timage;
    objPath,SelectedObject : string;
    SelectedIndex : integer;
    moving : boolean;

    function ListFiles(var Box : TStringlist; LPfad : string) : Boolean;
    procedure LoadObjects(f : string);
    procedure panelMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure panelMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    //PanelEvents sind hier eigentlich f�r die Images gedacht und falsche benannt
    procedure ImageClick(Sender : Tobject);
    procedure OnGridOver(Sender:TObject; x,y : integer);
    procedure OnGridClick(Sender:Tobject; x,y : integer);
    function getLayoutIndex(l : string):integer;
  public
    procedure StartEmpty;
    procedure StartNew(path : string);
    procedure StartExisting(path : string);

    procedure save(path : string);
    procedure load(path : string);
  end;

var
  Form8: TForm8;

implementation

uses main;

{$R *.dfm}

procedure Tform8.StartEmpty;
begin
  show;
end;

procedure Tform8.StartNew;
begin
  objPath := form1.Default_SystemPath+'data\sys\Articles_std.ini';
  GridImage.ToggleGrid();
  loadobjects(objPath);
  show;
end;

procedure Tform8.StartExisting(path : string);
begin
  show;
  objPath := form1.Default_SystemPath+'data\sys\Articles_std.ini';
  loadobjects(objPath);
  gridimage.Load(path,true);
  Gridimage.ToggleGrid();
end;

procedure Tform8.LoadObjects(f : string);
var ini : Tinifile;
    secs : Tstringlist;
    i,j,turn,state,d,n : integer;
begin
  secs := Tstringlist.Create;
  ini := Tinifile.Create(f);
  ini.ReadSections(secs);
  setlength(LayoutObject,secs.Count);
  setlength(Panels,secs.Count);
  setlength(imgs,secs.Count);

  for i := 0 to secs.Count-1 do begin
    turn := ini.ReadInteger(secs[i],'Turn',0);
    state := ini.ReadInteger(Secs[i],'State',0);
    LayoutObject[i] := TLayoutobject.create(secs[i],point(0,0),state,turn);
    Panels[i] := Tpanel.Create(groupbox2);
    Panels[i].Parent := groupbox2;
    Panels[i].Width := 20;
    Panels[i].Height := 20;
    imgs[i] := Timage.Create(panels[i]);
    imgs[i].parent := panels[i];
    imgs[i].Align := alCLient;
    imgs[i].Stretch := true;
    imgs[i].Hint := LayoutObject[i].Typ;
    imgs[i].ShowHint := true;
    LayoutObject[i].putOnImage(imgs[i]);
    imgs[i].Refresh;
    imgs[i].OnMouseDown := panelmousedown;
    imgs[i].OnMouseUp := panelmouseup;
    imgs[i].OnClick := imageclick;
  end;

  d := floor(secs.Count/2);
  n := 0;
  for i := 0 to 1 do begin
    for j := 0 to d-1 do begin
      panels[n].Top := 20+j*(panels[n].Height*2);
      panels[n].Left := 10+(i*30);
      inc(n);
    end;
  end;
  if secs.Count > d then begin
    panels[n].Top := 20+d*(panels[n].Height*2);
    panels[n].Left := 10;
  end;
end;


function Tform8.ListFiles(var Box : TStringlist; LPfad : string) : Boolean;
var LStrList: TStringlist;
    LSearchRec: TSearchRec;
begin
  Box.Clear;
  LStrList := TStringlist.Create;
  if FindFirst(LPfad + '*.*', faAnyFile, LSearchRec) = 0 then
    begin
      repeat
        if LSearchRec.Attr and faDirectory = 0 then
        begin
          Box.Add(LSearchRec.Name);
        end;
      until FindNext(LSearchRec) <> 0;
      FindClose(LSearchRec);
    end;
  LStrList.Free;
end;

procedure Tform8.panelMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  ((Sender as TImage).Parent as TPanel).BevelInner := bvLowered;
end;

procedure Tform8.panelMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  ((Sender as TImage).Parent as TPanel).BevelInner := bvRaised;
end;

procedure Tform8.ImageClick(Sender : Tobject);
begin
  SelectedObject := (Sender as Timage).Hint;
  statusbar1.Panels[0].Text := SelectedObject;
  SelectedIndex := getLayoutIndex(SelectedObject);
  MouseImage.Visible := true;
  MouseImage.Picture.LoadFromFile(form1.Default_IconPath+'\Gleisbild\'+SelectedObject+'_0.bmp');
  MouseIMage.Refresh;
  radiogroup1.ItemIndex := -1;
  Gridimage.ResetAllLayoutBitmaps;
end;

procedure Tform8.OnGridOver(Sender:TObject; x,y : integer);
begin
  statusbar1.Panels[1].Text := pointtostr(point(x,y));
end;

procedure TForm8.FormCreate(Sender: TObject);
begin
  GridImage := TgridImage.create(image1);
  GridImage.OnGridMouseOver := OnGridOver;
  Gridimage.OnGridClick := onGridClick;
  SelectedIndex := -1;
  SelectedObject := '';
  MouseImage := Timage.Create(self);
  MouseImage.Parent := self;
  MouseImage.Height := 19;
  MouseImage.Width := 19;
end;

function Tform8.getLayoutIndex(l : string):integer;
var i : integer;
begin
  result := -1;
  for i := 0 to length(LayoutObject)-1 do begin
    if LayoutObject[i].Typ = l then begin
      result := i;
      exit;
    end;
  end;
end;

procedure TForm8.Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if Selectedindex <> -1 then begin
    MouseImage.Left := x+1;
    MouseImage.Top := y+1;
    MouseImage.Refresh;
  end;
end;

procedure Tform8.OnGridClick(Sender:Tobject; x,y : integer);
var tempstr : string;
begin
  case Radiogroup1.ItemIndex of
    -1:begin
         moving := false;
         if SelectedIndex <> -1 then begin
           GridImage.InsertLayoutObject(LayoutObject[SelectedIndex],x,y);
           image1.Refresh;
         end;
       end;
    0:begin//�ndern
        moving := false;
      end;
    1:begin
        Gridimage.TurnNextAtPos(x,y);//Drehen
        moving := false;
      end;
    2:begin
        GridImage.ClearPos(x,y);//L�schen
        moving := false;
      end;
    3:begin//text einf�gen
        tempstr := inputbox('Text einf�gen','Text einf�gen','');
        gridimage.InsertText(x,y,tempstr);
        moving := false;
      end;
    4:begin
        moving := true;
      end;
    else begin
    end;
  end;
end;

procedure TForm8.Button1Click(Sender: TObject);
begin
  GridImage.ClearAll;
end;

procedure TForm8.FormResize(Sender: TObject);
begin
  gridImage.AutoResize(image1.Width,image1.Height);
end;

procedure TForm8.Speichern1Click(Sender: TObject);
begin
  savedialog1.InitialDir := form1.Default_SystemPath+'data\save';
  if savedialog1.Execute then begin
    case savedialog1.FilterIndex of
      1:gridimage.Save(savedialog1.FileName);
//      1:saveascs2
      3:gridimage.Save(savedialog1.FileName);
    end;
  end;
end;

procedure TForm8.Laden1Click(Sender: TObject);
begin
  opendialog1.InitialDir := form1.Default_SystemPath+'data\save';
  if opendialog1.Execute then begin
    gridimage.Load(opendialog1.FileName,true);
  end;
end;

procedure TForm8.FormShow(Sender: TObject);
begin
  moving := false;
end;

procedure Tform8.save(path : string);
var ini : Tinifile;
begin
  ini := Tinifile.Create(path);
  try
  finally
    ini.Free;
  end;
end;

procedure Tform8.load(path : string);
var ini : Tinifile;
begin
  ini := Tinifile.Create(path);
  try
  finally
    ini.Free;
  end;
end;

end.
