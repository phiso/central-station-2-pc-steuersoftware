object Form15: TForm15
  Left = 1390
  Top = 200
  Width = 423
  Height = 439
  AutoSize = True
  Caption = 'Fahrstra'#223'en'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 185
    Height = 340
    Align = alLeft
    Caption = 'Ausl'#246'ser'
    TabOrder = 0
    object Label2: TLabel
      Left = 8
      Top = 96
      Width = 30
      Height = 13
      Caption = 'Aktion'
      Visible = False
    end
    object Label3: TLabel
      Left = 8
      Top = 48
      Width = 38
      Height = 13
      Caption = 'Adresse'
      Visible = False
    end
    object ComboBox1: TComboBox
      Left = 8
      Top = 24
      Width = 161
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      Text = 'Typ Ausw'#228'hlen'
      OnChange = ComboBox1Change
      Items.Strings = (
        'S88 - Kontakt'
        'Magnetartikel'
        'Zug'
        'Manuell')
    end
    object SpinEdit1: TSpinEdit
      Left = 8
      Top = 64
      Width = 73
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 1
      Value = 0
      Visible = False
    end
    object ComboBox3: TComboBox
      Left = 8
      Top = 112
      Width = 161
      Height = 21
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 2
      Text = 'Geschwindigkeit'
      Visible = False
      Items.Strings = (
        'Geschwindigkeit'
        'Richtungs'#228'nderung'
        'Funktion')
    end
    object LabeledEdit1: TLabeledEdit
      Left = 8
      Top = 208
      Width = 169
      Height = 21
      EditLabel.Width = 59
      EditLabel.Height = 13
      EditLabel.Caption = #220'bertragung'
      TabOrder = 3
      Visible = False
    end
    object ComboBox2: TComboBox
      Left = 16
      Top = 256
      Width = 145
      Height = 21
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 4
      Text = 'Aus, Rund, Rot, Rechts, HP0'
      Items.Strings = (
        'Aus, Rund, Rot, Rechts, HP0'
        'Ein, Gr'#252'n, Gerade, HP1'
        'Gelb, Links, HP2'
        'Weis, SH0')
    end
    object ComboBox4: TComboBox
      Left = 8
      Top = 136
      Width = 33
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ItemHeight = 13
      ItemIndex = 1
      ParentFont = False
      TabOrder = 5
      Text = '>'
      Visible = False
      Items.Strings = (
        '<'
        '>')
    end
    object SpinEdit2: TSpinEdit
      Left = 48
      Top = 136
      Width = 121
      Height = 22
      MaxValue = 1000
      MinValue = 0
      TabOrder = 6
      Value = 0
      Visible = False
    end
  end
  object GroupBox2: TGroupBox
    Left = 185
    Top = 0
    Width = 222
    Height = 340
    Align = alClient
    Caption = 'Aktionen'
    TabOrder = 1
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 218
      Height = 30
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Button1: TButton
        Left = 8
        Top = 3
        Width = 113
        Height = 25
        Caption = 'Neue Aktion...'
        TabOrder = 0
        OnClick = Button1Click
      end
    end
    object ListBox1: TListBox
      Left = 8
      Top = 48
      Width = 209
      Height = 329
      ItemHeight = 13
      MultiSelect = True
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 340
    Width = 407
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Button2: TButton
      Left = 0
      Top = 0
      Width = 185
      Height = 41
      Caption = 'Abbrechen'
      TabOrder = 0
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 201
      Top = 0
      Width = 200
      Height = 41
      Caption = 'Erstellen'
      TabOrder = 1
    end
  end
  object MainMenu1: TMainMenu
    Top = 352
    object Fahrstrae1: TMenuItem
      Caption = 'Fahrstra'#223'e'
    end
  end
end
