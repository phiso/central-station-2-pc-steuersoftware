object Form2: TForm2
  Left = 533
  Top = 476
  BorderStyle = bsToolWindow
  Caption = 'Befehle bearbeiten'
  ClientHeight = 317
  ClientWidth = 588
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 241
    Top = 0
    Height = 317
    ResizeStyle = rsUpdate
    OnMoved = Splitter1Moved
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 241
    Height = 317
    Align = alLeft
    Caption = 'Alle befehle'
    TabOrder = 0
    object ListBox1: TListBox
      Left = 2
      Top = 15
      Width = 237
      Height = 300
      Align = alClient
      ItemHeight = 13
      PopupMenu = PopupMenu1
      TabOrder = 0
      OnClick = ListBox1Click
    end
  end
  object GroupBox2: TGroupBox
    Left = 244
    Top = 0
    Width = 344
    Height = 317
    Align = alClient
    Caption = 'Bearbeiten'
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 144
      Width = 92
      Height = 13
      Caption = 'Bescheibung (Hilfe)'
    end
    object Button1: TButton
      Left = 232
      Top = 280
      Width = 89
      Height = 25
      Caption = 'OK'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 16
      Top = 280
      Width = 105
      Height = 25
      Caption = 'Hinzuf'#252'gen'
      TabOrder = 1
      OnClick = Button2Click
    end
    object LabeledEdit1: TLabeledEdit
      Left = 8
      Top = 32
      Width = 321
      Height = 21
      EditLabel.Width = 28
      EditLabel.Height = 13
      EditLabel.Caption = 'Name'
      TabOrder = 2
    end
    object LabeledEdit2: TLabeledEdit
      Left = 8
      Top = 80
      Width = 321
      Height = 21
      EditLabel.Width = 70
      EditLabel.Height = 13
      EditLabel.Caption = 'Buffer (13Byte)'
      TabOrder = 3
    end
    object Memo1: TMemo
      Left = 8
      Top = 160
      Width = 321
      Height = 113
      Lines.Strings = (
        'Memo1')
      TabOrder = 4
    end
    object CheckBox1: TCheckBox
      Left = 8
      Top = 104
      Width = 281
      Height = 17
      Caption = 'Dynamischer Befehl'
      TabOrder = 5
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 40
    Top = 80
    object EintragLschen1: TMenuItem
      Caption = 'Eintrag L'#246'schen'
      OnClick = EintragLschen1Click
    end
  end
end
